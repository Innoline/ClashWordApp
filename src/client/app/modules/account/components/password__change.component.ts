import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs';

import { AccountService } from '../../services/account.service';

@Component({
  moduleId: module.id,
  templateUrl: 'password__change.component.html'
})

export class PasswordChangeComponent implements OnInit, OnDestroy {
  model: string;
  key: string;
  loading = false;
  error = '';
  private subscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private accountService: AccountService) { }

  ngOnInit() {
    // reset login status
    this.accountService.logout();
    // subscribe to router event
    this.subscription = this.activatedRoute.queryParams.subscribe(
      (param: any) => {
        this.key = param['key'];
      });
  }

  ngOnDestroy() {
    // prevent memory leak by unsubscribing
    this.subscription.unsubscribe();
  }

  change() {
    this.loading = true;
    this.accountService.passwordChange(this.key, this.model)
      .subscribe(result => {
          this.router.navigate(['/login']);
        },
        err  => {
          if(err instanceof Response) {
            this.error = err.text();
          } else {
            this.error = err;
          }
          this.loading = false;
        }
      );
  }
}
