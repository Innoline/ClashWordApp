import { Component, OnInit } from '@angular/core';
import { ClashWordCartsService } from '../services/clashword__carts.service';
import {
  ClashWordCartAdditionalSpecialEnum,
  ClashWordCartTypeEnum, ClashWordUserCartFilter,
  ClashWordUserCartModelApi
} from '../models/clashword.model';
import { LoadingService } from '../shared/components/loading/loading.service';

@Component({
  moduleId: module.id,
  selector: 'clash-word-carts',
  templateUrl: 'clashword__carts.component.html'
})

export class ClashWordCartsComponent implements OnInit {
  filter: ClashWordUserCartFilter;
  cartTypeEnum = ClashWordCartTypeEnum;
  cartSpecialEnum = ClashWordCartAdditionalSpecialEnum;
  cefrList: string[];
  typeList: ClashWordCartTypeEnum[];
  letters: string[];
  userCarts: ClashWordUserCartModelApi[];
  constructor(private loadingService: LoadingService,
              private clashWordCartsService: ClashWordCartsService) {}

  ngOnInit(): void {
    this.filter = {
      letter: null,
      typeId: null,
      cefr: null
    };
    this.loadingService.toggleLoadingIndicator(true);
    this.clashWordCartsService.list().subscribe(
      obj => {
        if (obj!== null) {
          this.cefrList = obj.cefrList;
          this.letters = obj.letters;
          this.typeList = obj.typeList;
          this.userCarts = obj.carts;
        }
        this.loadingService.toggleLoadingIndicator(false);
      },
      error => console.log(error)
    );
  }

  update(model: ClashWordUserCartModelApi) {
    let index = this.userCarts.indexOf(model);
    this.loadingService.toggleLoadingIndicator(true);
    this.clashWordCartsService.update(model.key).subscribe(
      obj => {
        if (obj !== null) {
          this.userCarts[index] = obj;
        }
        this.loadingService.toggleLoadingIndicator(false);
      },
      error => console.log(error)
    );
  }

  onChangeFromCefr(newValue: string) {
    this.filter.cefr = newValue;
  }

  onChangeFromType(newValue: ClashWordCartTypeEnum) {
    this.filter.typeId = newValue;
  }

  onChangeFromLetter(newValue: string) {
    this.filter.letter = newValue;
  }
}
