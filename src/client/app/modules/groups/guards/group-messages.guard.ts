import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { RouterExtensions } from '../../core/index';

import { IAppState } from '../../ngrx/index';

import { IUserState, UserCacheModel } from '../../user/index';
import { AuthGuardProvider } from '../../shared/index';
import { Account } from '../../account/index';

@Injectable()
export class GroupMessagesGuard implements CanActivate {

  constructor(public router: RouterExtensions,
              private store: Store<IAppState>,
              private authGuardProvider: AuthGuardProvider) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const canActivate = this.authGuardProvider.canActivate();
    if (canActivate) {
      let model = this.getState(this.store);
      if (model) {
        if (model.clanId === null) {
          this.router.navigate(['/clan/create'], {
            transition: {
              duration: 10000,
              name: 'slideTop',
            }
          });
        }
        return model.clanId !== null;
      }
    } else {
      this.store.dispatch(new Account.LogoutInProgressAction());
      this.router.navigate(['/'], {
        transition: {
          duration: 10000,
          name: 'slideTop',
        }
      });
    }
    return false;
  }

  getState(store: Store<IAppState>): any {
    let state: IUserState = undefined;

    store.select(s => s.user).take(1).subscribe(s => {
      state = s;
    });
    return state.cache;
  }
}
