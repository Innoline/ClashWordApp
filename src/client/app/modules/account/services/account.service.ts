// angular
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

// libs
import { Observable } from 'rxjs/Observable';

// app
import { Config } from '../../core/index';
import { Analytics, AnalyticsService } from '../../analytics/index';
import { CookieService } from 'ngx-cookie';

// module
import { Account, ApiJson, AuthToken, AuthTokenModel, UserLoginModel } from '../index';
import { HttpHelper } from '../../services/index';

@Injectable()
export class AccountService extends Analytics {

  private config: any;
  constructor(
    public analytics: AnalyticsService,
    private http: Http,
    private httpHelper: HttpHelper,
    private cookieService: CookieService
  ) {
    super(analytics);
    this.category = Account.CATEGORY;
    this.config = Config.ENVIRONMENT();
  }

  auth(): Observable<ApiJson<boolean, string>> {
    let userToken: AuthTokenModel = <AuthTokenModel>this.cookieService.getObject(AuthToken);
    if (userToken && userToken.token.length > 0) {
      let expires: Date = new Date(userToken.expires);

      if (typeof (userToken.expires) !== 'undefined' || new Date() < expires) {
        return this.httpHelper.get(this.config.API + 'auth/', true);
      } else {
        this.cookieService.putObject(AuthToken, undefined);
      }
    }

    const out = <ApiJson<boolean, string>>{
      data: false,
      errors: undefined
    };

    return Observable.of(out);
  }

  login(model: UserLoginModel): Observable<ApiJson<AuthTokenModel, string>> {
    return this.http.post(this.config.baseAPI + 'token', JSON.stringify({
      email: model.username,
      password: model.password
    }), this.getRequestOptions())
      .map((res: Response) => res.json());
  }

  u_login(token: string): Observable<ApiJson<AuthTokenModel, string>> {
    return this.http.get(this.config.API + 'auth/ulogin/' + token)
      .map((res: Response) => res.json());
  }


  logout(): Observable<ApiJson<boolean, string>>  {
    return this.http.get(this.config.API + 'auth/signout/')
      .map((res: Response) => res.json());
  }

  /**
   * Returns the RequestOptions for the HTTP POST and HTTP PATCH Requests
   * @return {RequestOptions} The request options.
   */
  private getRequestOptions(): RequestOptions {
    const headers: Headers = new Headers({'Content-Type': 'application/json'});
    return new RequestOptions({headers: headers});
  }
}
