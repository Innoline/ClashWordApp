// angular
import { Injectable } from '@angular/core';

// libs
import { Observable } from 'rxjs/Observable';

// app
import { Config } from '../../core/index';
import { Analytics, AnalyticsService } from '../../analytics/index';
import { HttpHelper } from '../../services/http.helper';

// module
import { Dictionary, DictionaryWordModel, DictionaryList } from '../index';
import { DictionaryWordCompleter } from '../completers/dictionary__word.completer';

@Injectable()
export class DictionaryService extends Analytics {
  private mainAPI: string = Config.ENVIRONMENT().API + 'dictionary';

  constructor(
    public analytics: AnalyticsService,
    private httpHelper: HttpHelper
  ) {
    super(analytics);
    this.category = Dictionary.CATEGORY;
  }

  completerWord(wordModel: DictionaryWordModel) {
    return new DictionaryWordCompleter(this.httpHelper, wordModel, this.mainAPI + '/completer');
  }

  searchWord(wordModel: DictionaryWordModel): Observable<DictionaryList[]> {
    return this.httpHelper.post(this.mainAPI + '/search', wordModel, true);
  }
}
