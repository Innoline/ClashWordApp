// angular
import { Injectable } from '@angular/core';

// libs
import { Observable } from 'rxjs/Observable';

// app
import { Config } from '../../core/index';
import { Analytics, AnalyticsService } from '../../analytics/index';

// module
import { User, ApiJson, UserInfoModel, UserCacheModel } from '../index';
import { HttpHelper } from '../../services/index';

@Injectable()
export class UserService extends Analytics {
  private profileAPI: string = Config.ENVIRONMENT().API + 'userprofile';
  private cacheAPI: string = Config.ENVIRONMENT().API + 'usersettings';

  constructor(
    public analytics: AnalyticsService,
    private httpHelper: HttpHelper
  ) {
    super(analytics);
    this.category = User.CATEGORY;
  }

  getProfile(): Observable<ApiJson<UserInfoModel, string>> {
    return this.httpHelper.get(this.profileAPI, true);
  }

  saveProfile(model: UserInfoModel): Observable<ApiJson<UserInfoModel, string>> {
    return this.httpHelper.post(this.profileAPI, model, true);
  }

  resetCache(): Observable<ApiJson<UserCacheModel, string>> {
    return this.httpHelper.get(this.cacheAPI + '/reset', true);
  }

  getCache(): Observable<ApiJson<UserCacheModel, string>> {
    return this.httpHelper.get(this.cacheAPI, true);
  }

  saveCache(model: UserCacheModel): Observable<ApiJson<UserCacheModel, string>> {
    return this.httpHelper.post(this.cacheAPI, model, true);
  }
}
