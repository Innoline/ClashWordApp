// angular
import { Http, Headers, RequestOptions } from '@angular/http';

// libs
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ConfigLoader } from '@ngx-config/core';
import { AuthToken, AuthTokenModel, ApiJson } from './app/modules/shared/models/index';
import { CookieService } from 'ngx-cookie';

export class SettingsHttpLoader implements ConfigLoader {
  private readonly headers = new Headers({
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    'Accept': 'application/json'
  });
  private readonly getSettings = new RequestOptions({headers: this.headers});
  private readonly pathConfig: string = '/assets/config.json';
  constructor(private readonly http: Http,
              private readonly cookieService: CookieService,
              private readonly endpoint: string) {
  }

  get authToken(): string {
    let userToken = <AuthTokenModel>this.cookieService.getObject(AuthToken);
    if (userToken) {
      return userToken.token;
    } else {
      return null;
    }
  }

  loadSettings(): any {
    if (this.authToken === null || !this.endpoint) {
      return this.http.get(this.pathConfig)
        .map((res: any) => res.json())
        .toPromise()
        .then((settings: any) => settings)
        .catch(() => Promise.reject('Endpoint unreachable!'));
    } else {
      console.log(null);
      this.headers.delete('Authorization');
      this.headers.append('Authorization', `Bearer ${this.authToken}`);
      return this.http.get(this.endpoint, this.getSettings)
        .map((res: any) => res.json())
        .toPromise()
        .then((settings: ApiJson<any, string>) => settings.data)
        .catch(() => Promise.reject('Endpoint unreachable!'));
    }
  }
}
