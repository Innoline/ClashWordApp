import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IAppState, getClashWordLetterList, getClashWordCefrList,
  getClashWordTypeList, getClashWordCarts } from '../../ngrx/index';
import { SelectListItem, ClashWordCartTypeEnum, ClashWordUserCartModelApi } from '../index';


@Component({
  selector: 'cw-carts-page',
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'clashword_carts.page.html'
})

export class ClashWordCartsPageComponent {
  cefrList$: Observable<string[]>;
  typeList$: Observable<ClashWordCartTypeEnum[]>;
  letterList$: Observable<string[]>;
  carts$: Observable<ClashWordUserCartModelApi[]>;

  constructor(private store: Store<IAppState>) {
    this.cefrList$ = store.let(getClashWordCefrList);
    this.typeList$ = store.let(getClashWordTypeList);
    this.letterList$ = store.let(getClashWordLetterList);
    this.carts$ = store.let(getClashWordCarts);
  }
}
