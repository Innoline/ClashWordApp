import { Pipe, PipeTransform } from '@angular/core';
import { ClashWordUserCartFilter, ClashWordUserCartModelApi } from '../../models/clashword.model';

@Pipe({
  name: 'carts_filter',
  pure: false
})
export class ClashWordCartsPipe implements PipeTransform {
  transform(items: ClashWordUserCartModelApi[], filter: ClashWordUserCartFilter): ClashWordUserCartModelApi[] {
    if (!items || !filter) {
      return items;
    }
    return items.filter(
      cart => {
        let out = true;
        if (out === true && filter.cefr !== null) {
          out = cart.cefrKey === filter.cefr;
        }
        if (out === true && filter.typeId !== null) {
          out = cart.typeId === filter.typeId;
        }
        if (out === true && filter.letter !== null) {
          out = cart.letter === filter.letter;
        }
        return out;
      });
  }
}
