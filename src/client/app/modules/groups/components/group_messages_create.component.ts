import { Component, Input, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import { Subject } from 'rxjs/Subject';
import { IAppState, getGroupsErrors } from '../../ngrx/index';
import { Groups, GroupMessageListItem, GroupMessageTypeEnum, GroupMessageStatusEnum } from '../index';
import { UserCacheModel } from '../../user/index';

@Component({
  moduleId: module.id,
  selector: 'cw-group-messages-create',
  templateUrl: 'group_messages_create.component.html'
})

export class GroupMessagesCreateComponent implements OnInit, OnDestroy {
  @Input() settings: UserCacheModel;
  errors: string;
  groupMessageTypeEnum = GroupMessageTypeEnum;
  groupMessageStatusEnum = GroupMessageStatusEnum;

  public message: GroupMessageListItem;
  private ngUnSubscribeErrors: Subject<void> = new Subject<void>();
  private message_init = {
    id: undefined,
    message: undefined,
    groupId: undefined,
    statusId: this.groupMessageStatusEnum.Send,
    typeId: this.groupMessageTypeEnum.Message
  };

  constructor(private store: Store<IAppState>) {
    this.message = <GroupMessageListItem>(this.message_init);
  }

  ngOnInit() {
    this.store.let(getGroupsErrors)
      .takeUntil(this.ngUnSubscribeErrors)
      .subscribe((result: string) => {
        if (result) {
          this.errors = result;
        }
      });
  }

  create() {
    if (this.message) {
      if (this.message.message && this.message.message.length > 0) {
        console.log(this.message_init);
        let model = <GroupMessageListItem>{
          id: undefined,
          message: this.message.message,
          groupId: this.settings.clanId,
          statusId: this.groupMessageStatusEnum.Send,
          typeId: this.groupMessageTypeEnum.Message
        };
        this.store.dispatch(new Groups.MessagesAddInProgressAction(model));
        this.message.message = undefined;
      } else {
        this.errors = 'Заполните все поля';
      }
    }
  }

  ngOnDestroy(): void {
    this.ngUnSubscribeErrors.next();
    this.ngUnSubscribeErrors.complete();
  }
}
