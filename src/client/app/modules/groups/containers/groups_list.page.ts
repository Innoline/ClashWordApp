import 'rxjs/add/operator/let';
import { Component, ChangeDetectionStrategy, HostBinding } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IAppState, getUserCache, getGroupsList } from '../../ngrx/index';
import { GroupListItem } from '../models/index';
import { UserCacheModel } from '../../user/models/index';


@Component({
  selector: 'cw-groups-list-page',
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'groups_list.page.html'
})

export class GroupsListPageComponent {
  @HostBinding('class.o-container')
  @HostBinding('class.o-container--medium')
  @HostBinding('style.display') get getDisplay(){
    return 'block';
  }

  settings$: Observable<UserCacheModel>;
  groups$: Observable<GroupListItem[]>;

  constructor(private store: Store<IAppState>) {
    this.settings$ = store.let(getUserCache);
    this.groups$ = store.let(getGroupsList);
  }
}
