import { GroupsService } from './groups.service';
import { GroupMessagesService } from './group.messages.service';
import { GroupsHub } from './groups.hub';

export const GROUPS_SERVICES: any[] = [
  GroupsHub,
  GroupsService,
  GroupMessagesService
];

export * from './groups.service';
export * from './group.messages.service';
export * from './groups.hub';
