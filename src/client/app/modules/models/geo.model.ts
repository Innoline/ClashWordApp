export class GeoCountryModel {
  query: string;
}
export class GeoRegionModel {
  query: string;
  countryId?: number;
}
export class GeoCityModel {
  query: string;
  regionId?: number;
  countryId?: number;
}
