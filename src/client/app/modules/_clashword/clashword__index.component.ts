import { Component, OnInit } from '@angular/core';
import {
  ClashWordCartTypeEnum,
  ClashWordUserChestModelApi,
  ClashWordUserCartModelApi,
  ClashWordChestStatusEnum, ClashWordChestTypeEnum, ClashWordCartAdditionalSpecialEnum
} from '../models/clashword.model';
import { ClashWordChestsService } from '../services/clashword__chests.service';
import { CountDownStateEnum } from '../shared/components/countdown/countdown.model';
import { LoadingService } from '../shared/components/loading/loading.service';

@Component({
  moduleId: module.id,
  selector: 'clash-word',
  templateUrl: 'clashword__index.component.html'
})
export class ClashWordIndexComponent implements OnInit {
  cartTypeEnum = ClashWordCartTypeEnum;
  cartSpecialEnum = ClashWordCartAdditionalSpecialEnum;
  chestTypeEnum = ClashWordChestTypeEnum;
  chestStatusEnum = ClashWordChestStatusEnum;
  countDownStateEnum = CountDownStateEnum;

  userCarts: ClashWordUserCartModelApi[];

  crownChest: ClashWordUserChestModelApi;
  freeChest: ClashWordUserChestModelApi;
  freeChestsCount: number = 0;
  normalChests: ClashWordUserChestModelApi[] = [];
  gameState: CountDownStateEnum = CountDownStateEnum.Stop;
  crownChestUserCtn: number;
  crownChestTotalCtn: number;

  private specialChests: ClashWordUserChestModelApi[];
  private freeChests: ClashWordUserChestModelApi[] = [];
  private error: any;

  constructor(private loadingService: LoadingService,
              private clashWordGameChestService: ClashWordChestsService) {
  }

  ngOnInit() {
    this.ChestNormal();
    this.ChestSpecial();
  }

  openNormalChest(model: ClashWordUserChestModelApi) {
    if (model.statusId === this.chestStatusEnum.UnLocked) {
      this.loadingService.toggleLoadingIndicator(true);
      this.clashWordGameChestService.open(model).subscribe(
        obj => {
          if (obj !== null) {
            let index = this.normalChests.indexOf(model);
            this.normalChests[index] = obj.chest;

            this.userCarts = obj.carts;
          }
          this.loadingService.toggleLoadingIndicator(false);
        },
        error => console.log(error)
      );
    }
  }

  checkUnlock(item: ClashWordUserChestModelApi) {
    console.log(item.statusId);
    return item.statusId === this.chestStatusEnum.Timer;
  }

  unlockNormalChest(model: ClashWordUserChestModelApi) {
    let chestUnlock = 0;
    for (let i = 0; i < this.normalChests.length; i++) {
      if (this.normalChests[i] !== null) {
        if (this.normalChests[i].statusId === this.chestStatusEnum.Timer) {
          chestUnlock++;
        }
      }
    }

    if (chestUnlock === 0) {
      this.clashWordGameChestService.timer(model).subscribe(
        obj => {
          console.log(obj);
          if (obj !== null) {
            let index = this.normalChests.indexOf(model);
            this.normalChests[index] = obj;
          }
        },
        error => console.log(error)
      );
    }
  }

  openSpecialChest(model: ClashWordUserChestModelApi) {
    if (model.statusId === this.chestStatusEnum.UnLocked) {
      this.loadingService.toggleLoadingIndicator(true);
      this.clashWordGameChestService.open(model).subscribe(
        obj => {
          if (obj !== null) {
            let index = this.specialChests.indexOf(model);
            this.ChestSpecialOpen(index, obj.chest);

            console.log(obj.carts);
            this.userCarts = obj.carts;
          }
          this.loadingService.toggleLoadingIndicator(false);
        },
        error => console.log(error)
      );
    }
  }

  closeCarts() {
    console.info('closeCarts');
    this.userCarts = null;
  }

  onNormalDuration(model: ClashWordUserChestModelApi) {
    let index = this.normalChests.indexOf(model);
    model.statusId = this.chestStatusEnum.UnLocked;
    model.duration = 0;
    model.state = this.countDownStateEnum.Stop;
    this.normalChests[index] = model;
  }

  onSpecialDuration(model: ClashWordUserChestModelApi) {
    console.log(model);
  }

  private ChestNormal() {
    this.clashWordGameChestService.list().subscribe(
      data => {
        this.normalChests = data;
      },
      error => console.log(error)
    );
  }

  private ChestSpecial() {
    this.clashWordGameChestService.special().subscribe(
      data => {
        this.specialChests = data.chests;
        this.crownChestUserCtn = data.userCnt;
        this.crownChestTotalCtn = data.totalCnt;
        this.fillChests();
      },
      error => console.log(error)
    );
  }

  private ChestSpecialOpen(index: number, chest: ClashWordUserChestModelApi) {
    this.specialChests[index] = chest;
    switch (chest.typeId) {
      case this.chestTypeEnum.Free:
        this.freeChests[0] = this.freeChests[1];
        this.freeChests[1] = chest;
        this.freeChest = this.freeChests[0];
        var chestArr = this.freeChests.filter(function(chest) {
          return chest.duration > 0;
        });
        this.freeChestsCount = chestArr.length;
        break;
      case this.chestTypeEnum.Crown:
        this.crownChest = chest;
        break;
    }
  }

  private fillChests() {
    let self = this;

    self.freeChestsCount = 0;
    self.freeChests = [];
    self.crownChest = null;

    self.specialChests.forEach((item, index) => {
      switch (item.typeId) {
        case self.chestTypeEnum.Free:
          self.freeChests.push(item);
          self.freeChest = item;
          if (item.duration === 0) {
            self.freeChestsCount++;
          }
          break;
        case self.chestTypeEnum.Crown:
          self.crownChest = item;
          break;
      }
    });
  }
}
