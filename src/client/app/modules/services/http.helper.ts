import { Http, Headers, RequestOptions, ResponseOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { CookieService } from 'ngx-cookie';
import { AuthToken, AuthTokenModel } from '../shared/models/index';

@Injectable()
export class HttpHelper {
  private readonly headers = new Headers({
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    'Accept': 'application/json'
  });
  private readonly getSettings = new RequestOptions({headers: this.headers});
  private readonly postSettings = new ResponseOptions({headers: this.headers});

  constructor(private http: Http,
              private cookieService: CookieService) {
  }

  get(url: string, isAuth?: boolean) {
    if (isAuth) {
      this.addAuth();
    } else {
      this.delAuth();
    }
    return this.http.get(url, this.getSettings)
      .map(this.extractData)
      .catch(this.handleError);
  }

  post(url: string, body: any, isAuth?: boolean) {
    if (isAuth) {
      this.addAuth();
    } else {
      this.delAuth();
    }
    return this.http.post(url, this.stringJson(body), this.postSettings)
      .map(this.extractData)
      .catch(this.handleError);
  }

  put(url: string, body: any, isAuth?: boolean) {
    if (isAuth) {
      this.addAuth();
    } else {
      this.delAuth();
    }
    return this.http.put(url, this.stringJson(body), this.postSettings)
      .map(this.extractData)
      .catch(this.handleError);
  }

  remove(url: string, body: any, isAuth?: boolean) {
    if (isAuth) {
      this.addAuth();
    } else {
      this.delAuth();
    }
    return this.http.delete(url, this.getSettings)
      .map(this.extractData)
      .catch(this.handleError);
  }

  get authToken(): string {
    let userToken = <AuthTokenModel>this.cookieService.getObject(AuthToken);
    if (userToken) {
      return userToken.token;
    } else {
      return null;
    }
  }

  private extractData(res: Response) {
    if (res.text().length > 0) {
      let body = res.json();
      return body || {};
    } else return {};
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  private stringJson(body: any) {
    return JSON.stringify(body);
  }

  private addAuth() {
    this.delAuth();
    this.headers.append('Authorization', `Bearer ${this.authToken}`);
  }

  private delAuth() {
    this.headers.delete('Authorization');
  }
}
