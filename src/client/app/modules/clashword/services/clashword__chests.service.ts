import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {
  ClashWord, ClashWordUserChestModelApi, ClashWordUserChestSpecialModelApi,
  ClashWordUserOpenedChestModel
} from '../index';
// app
import { Config } from '../../core/index';
import { Analytics, AnalyticsService } from '../../analytics/index';

import { HttpHelper } from '../../services/http.helper';
import { ApiJson } from '../../shared/models/index';

@Injectable()
export class ClashWordChestsService extends Analytics {
  private mainAPI: string = Config.ENVIRONMENT().API + 'ClashWordGameChests';
  constructor(
    public analytics: AnalyticsService,
    private httpHelper: HttpHelper
  ) {
    super(analytics);
    this.category = ClashWord.CATEGORY;
  }

  list(): Observable<ApiJson<ClashWordUserChestModelApi[], string>> {
    return this.httpHelper.get(this.mainAPI, true);
  }

  special(): Observable<ApiJson<ClashWordUserChestSpecialModelApi, string>> {
    return this.httpHelper.get(this.mainAPI + '/special', true);
  }

  open(model: ClashWordUserChestModelApi): Observable<ApiJson<ClashWordUserOpenedChestModel, string>> {
    return this.httpHelper.post(this.mainAPI + '/open', model, true);
  }

  timer(model: ClashWordUserChestModelApi): Observable<ApiJson<ClashWordUserChestModelApi, string>> {
    return this.httpHelper.post(this.mainAPI + '/timer', model, true);
  }
}
