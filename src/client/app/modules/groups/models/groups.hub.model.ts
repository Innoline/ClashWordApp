///<reference path="../../../../../../node_modules/@types/signalr/index.d.ts"/>
/* SignalR related interfaces  */
/// <reference path="./../../../../../../node_modules/@types/signalr/index.d.ts" />

import { GroupMessageStatusEnum, GroupMessageTypeEnum } from './group.messages.model';
export interface GroupsSignalR extends SignalR {
  groupHub: GroupsProxy;
}

export interface GroupsProxy {
  client: GroupsClient;
  server: GroupsServer;
}

export interface GroupsClient {
  setConnectionId: (id: string) => void;
  setMessage: (obj: GroupMessageListItemHub) => void;
}

export interface GroupsServer {
  init(): void;
  start(id: string): void;
}

export enum SignalRConnectionStatus {
  Connected = 1,
  Disconnected = 2,
  Error = 3
}

export interface GroupMessageListItemHub {
  Id: string;
  Message: string;
  GroupId: string;
  StatusId: GroupMessageStatusEnum;
  TypeId: GroupMessageTypeEnum;
}

