import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IAppState, getEducationModeList, getEducationCefrList,
  getEducationPosList, getEducationLangList, getEducationTopicList, getEducationTask } from '../../ngrx/index';
import { SelectListItem, EducationWordTaskModel } from '../index';


@Component({
  selector: 'cw-education-main-page',
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'education_main.page.html'
})

export class EducationMainPageComponent {
  modeList$: Observable<SelectListItem[]>;
  cefrList$: Observable<SelectListItem[]>;
  posList$: Observable<SelectListItem[]>;
  langList$: Observable<SelectListItem[]>;
  topicList$: Observable<SelectListItem[]>;
  //task$: Observable<EducationWordTaskModel>;

  constructor(private store: Store<IAppState>) {
    this.modeList$ = store.let(getEducationModeList);
    this.cefrList$ = store.let(getEducationCefrList);
    this.posList$ = store.let(getEducationPosList);
    this.langList$ = store.let(getEducationLangList);
    this.topicList$ = store.let(getEducationTopicList);
    //this.task$ = store.let(getEducationTask);
  }
}
