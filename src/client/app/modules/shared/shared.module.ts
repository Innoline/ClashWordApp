import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// modules
import { MultilingualModule } from '../i18n/index';
import { SHARED_COMPONENTS } from './components/index';
import { SHARED_PIPES } from './pipes/index';
import { SHARED_DIRECTIVES } from './directives/index';
import { SHARED_PROVIDERS } from './providers/index';
import { TabsModule } from './ui/tabs/tabs.module';

const SHARED_MODULES: any[] = [
  CommonModule,
  HttpModule,
  FormsModule,
  RouterModule,
  TabsModule,
  MultilingualModule,
];

/**
 * SharedModule
 * Only for shared components, directives and pipes
 * Do not specify providers here
 * https://angular.io/docs/ts/latest/cookbook/ngmodule-faq.html#!#what-kinds-of-modules-should-i-have-and-how-should-i-use-them-
 */

@NgModule({
  imports: [
    ...SHARED_MODULES
  ],
  declarations: [
    ...SHARED_COMPONENTS,
    ...SHARED_PIPES,
    ...SHARED_DIRECTIVES
  ],
  exports: [
    ...SHARED_MODULES,
    ...SHARED_COMPONENTS,
    ...SHARED_PIPES,
    ...SHARED_DIRECTIVES
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        ...SHARED_PROVIDERS
      ]
    };
  }
}
