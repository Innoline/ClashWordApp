import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SharedServicesModule } from '../shared.services.module';
import { EducationComponent } from './education.component';
import { EducationService } from '../services/education.service';
import { EducationRoutingModule } from './education-routing.module';

@NgModule({
  imports: [SharedModule, SharedServicesModule, EducationRoutingModule],
  declarations: [EducationComponent],
  exports: [EducationComponent],
  providers: [EducationService]
})
export class EducationModule { }
