import { Input, Component, HostBinding } from '@angular/core';

import {
  Groups, GroupListItem, GroupMessageListItem, GroupMessageTypeEnum, GroupMessageStatusEnum, GroupJoinModel, GroupAccessEnum
} from '../index';
import { Store } from '@ngrx/store';
import { IAppState } from '../../ngrx/index';
import { UserCacheModel } from '../../user/index';


@Component({
  moduleId: module.id,
  selector: 'cw-groups-list',
  templateUrl: 'groups_list.component.html'
})

export class GroupsListComponent {
  @HostBinding('class.o-grid')

  @Input() groups: GroupListItem[];
  @Input() settings: UserCacheModel;

  groupMessageTypeEnum = GroupMessageTypeEnum;
  groupMessageStatusEnum = GroupMessageStatusEnum;
  groupAccessEnum = GroupAccessEnum;

  constructor(private store: Store<IAppState>) {
  }

  join(model: GroupJoinModel) {
    switch (model.accessId) {
      case this.groupAccessEnum.Open:
        this.store.dispatch(new Groups.GroupJoinInProgressAction(model.id));
        break;
      case this.groupAccessEnum.Invite:
        const message: GroupMessageListItem = {
          id: undefined,
          message: 'Хочу в ваш клан',
          groupId: model.id,
          statusId: this.groupMessageStatusEnum.Send,
          typeId: this.groupMessageTypeEnum.ClanJoiningRequest
        };
        this.store.dispatch(new Groups.MessagesAddInProgressAction(message));
        break;
    }
  }
}
