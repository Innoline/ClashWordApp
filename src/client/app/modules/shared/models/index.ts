export interface AuthTokenModel {
  token: string;
  expires: string;
}

export interface ApiJson<T,Z> {
  data: T;
  errors: Z;
}

export interface SelectListItem {
  key: string;
  text: string;
}

export const AuthToken: string = 'authToken';
