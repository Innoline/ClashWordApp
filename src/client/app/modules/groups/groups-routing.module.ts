import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/authentication.guard';
import { GroupsListPageComponent, GroupCreatePageComponent } from './containers/index';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'clans', component: GroupsListPageComponent, canActivate: [AuthGuard]},
      { path: 'clan/create', component: GroupCreatePageComponent, canActivate: [AuthGuard]}
    ])
  ],
  exports: [RouterModule]
})
export class GroupsRoutingModule { }
