import { IClashWordState, clashWordInitialState } from '../index';
import { ClashWord, ClashWordUserCartsApi, ClashWordUserCartModelApi,
  ClashWordUserChestSpecialModelApi, ClashWordChestTypeEnum, ClashWordUserOpenedChestModel, ClashWordUserChestModelApi } from '../index';

export function reducer(
  state: IClashWordState = clashWordInitialState,
  // could support multiple state actions via union type here
  // ie: NameList.Actions | Other.Actions
  // the seed's example just has one set of actions: NameList.Actions
  action: ClashWord.Actions
): IClashWordState {
  let chestTypeEnum = ClashWordChestTypeEnum;

  switch (action.type) {
    case ClashWord.ActionTypes.CARTS_FROM_CHEST_INIT:
      return (<any>Object).assign({}, state, {
        errors: undefined,
        cartsFromChest: clashWordInitialState.cartsFromChest
      });

    case ClashWord.ActionTypes.INIT_RECEIVED:
      const lists = <ClashWordUserCartsApi>action.payload;
      return (<any>Object).assign({}, state, {
        errors: undefined,
        letterList: lists.letters,
        cefrList: lists.cefrList,
        carts: lists.carts,
        typeList: lists.typeList
      });

    case ClashWord.ActionTypes.CART_UPDATE_RECEIVED:
      const cart = <ClashWordUserCartModelApi>action.payload;
      let index = state.carts.map(cart => cart.key).indexOf(cart.key);
      if (index === -1) {
        return (<any>Object).assign({}, state, {
          errors: undefined,
        });
      }
      return (<any>Object).assign({}, state, {
        errors: undefined,
        carts: [
          ...state.carts.slice(0, index),
          Object.assign({}, state.carts[index], action.payload),
          ...state.carts.slice(index + 1)
        ]
      });

    case ClashWord.ActionTypes.CHESTS_NORMAL_RECEIVED:
      return (<any>Object).assign({}, state, {
        errors: undefined,
        normalChests: action.payload
      });

    case ClashWord.ActionTypes.CHESTS_SPECIAL_RECEIVED:
      const special = <ClashWordUserChestSpecialModelApi>action.payload;
      let freeChestsCount = 0;
      let freeChests = [];
      let crownChest = null;
      let freeChest = null;

      special.chests.forEach((item, index) => {
        switch (item.typeId) {
          case chestTypeEnum.Free:
            freeChests.push(item);
            freeChest = item;
            if (item.duration === 0) {
              freeChestsCount++;
            }
            break;
          case chestTypeEnum.Crown:
            crownChest = item;
            break;
        }
      });

      return (<any>Object).assign({}, state, {
        errors: undefined,
        freeChest: freeChest,
        freeChests: freeChests,
        freeChestsCount: freeChestsCount,
        crownChest: crownChest,
        crownChestUserCtn: special.userCnt,
        crownChestTotalCtn: special.totalCnt,
      });

    case ClashWord.ActionTypes.CHEST_OPEN_RECEIVED:
      const openedChest = <ClashWordUserOpenedChestModel>action.payload;
      if (!openedChest.chest) {
        let indexChest = state.normalChests.map(chest => chest.chestId).indexOf(openedChest.chestId);
        if (indexChest === -1) {
          return (<any>Object).assign({}, state, {
            errors: undefined,
          });
        }

        let normalChests = [
          ...state.normalChests.slice(0, indexChest),
          openedChest.chest,
          ...state.normalChests.slice(indexChest + 1)
        ];

        console.log(normalChests);
        return (<any>Object).assign({}, state, {
          errors: undefined,
          normalChests: normalChests,
          cartsFromChest: openedChest.carts
        });
      } else {
        switch (openedChest.chest.typeId) {
          case chestTypeEnum.Free:
            let freeChests = [];
            freeChests[0] = state.freeChests[1];
            freeChests[1] = openedChest.chest;
            let chestArr = freeChests.filter(function (chest) {
              return chest.duration > 0;
            });

            return (<any>Object).assign({}, state, {
              errors: undefined,
              freeChest: state.freeChests[0],
              freeChests: freeChests,
              freeChestsCount: chestArr.length,
              cartsFromChest: openedChest.carts
            });
          case chestTypeEnum.Crown:
            return (<any>Object).assign({}, state, {
              errors: undefined,
              crownChest: openedChest.chest,
              cartsFromChest: openedChest.carts
            });
          default:
            return (<any>Object).assign({}, state, {
              errors: undefined,
            });
        }
      }

    case ClashWord.ActionTypes.CHEST_UNLOCK_RECEIVED:
      console.log(action.payload);
      const unlockChest = <ClashWordUserChestModelApi>action.payload;
      let indexUnlock = state.normalChests.map(chest => chest.chestId).indexOf(unlockChest.chestId);
      if (indexUnlock === -1) {
        return (<any>Object).assign({}, state, {
          errors: undefined,
        });
      }

      let unlockChests = [
        ...state.normalChests.slice(0, indexUnlock),
        action.payload,
        ...state.normalChests.slice(indexUnlock + 1)
      ];

      console.log(indexUnlock);
      return (<any>Object).assign({}, state, {
        errors: undefined,
        normalChests: unlockChests
      });

    case ClashWord.ActionTypes.CHEST_NORMAL_DURATION:
      let normalDurationChest = <ClashWordUserChestModelApi>action.payload;
      let indexNormalDuration = state.normalChests.map(chest => chest.chestId).indexOf(normalDurationChest.chestId);
      if (indexNormalDuration === -1) {
        return (<any>Object).assign({}, state, {
          errors: undefined,
        });
      }

      normalDurationChest.statusId = this.chestStatusEnum.UnLocked;
      normalDurationChest.duration = 0;
      normalDurationChest.state = this.countDownStateEnum.Stop;

      let normalChests = [
        ...state.normalChests.slice(0, indexNormalDuration),
        Object.assign({}, state.normalChests[indexNormalDuration], normalDurationChest),
        ...state.normalChests.slice(indexNormalDuration + 1)
      ];

      return (<any>Object).assign({}, state, {
        errors: undefined,
        normalChests: normalChests
      });

    case ClashWord.ActionTypes.INIT_FAILURE:
    case ClashWord.ActionTypes.CART_UPDATE_FAILURE:
    case ClashWord.ActionTypes.CHESTS_NORMAL_FAILURE:
    case ClashWord.ActionTypes.CHESTS_SPECIAL_FAILURE:
    case ClashWord.ActionTypes.CHEST_UNLOCK_FAILURE:
    case ClashWord.ActionTypes.CHEST_OPEN_FAILURE:
    case ClashWord.ActionTypes.CARTS_FROM_CHEST_FAILURE:
      return (<any>Object).assign({}, state, {
        errors: action.payload
      });

    default:
      return state;
  }
}
