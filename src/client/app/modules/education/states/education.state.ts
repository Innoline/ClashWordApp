import { Observable } from 'rxjs/Observable';
import { SelectListItem, EducationWordTaskModel } from '../index';

export interface IEducationState {
  errors: string;
  task: EducationWordTaskModel;
  modeList: Array<SelectListItem>;
  cefrList: Array<SelectListItem>;
  posList: Array<SelectListItem>;
  langList: Array<SelectListItem>;
  topicList: Array<SelectListItem>;
}

export const dictionaryInitialState: IEducationState = {
  errors: undefined,
  task: undefined,
  modeList: undefined,
  cefrList: undefined,
  posList: undefined,
  langList: undefined,
  topicList: undefined,
};

export function getEducationModeList(state$: Observable<IEducationState>) {
  return state$.select(state => state.modeList);
}

export function getEducationCefrList(state$: Observable<IEducationState>) {
  return state$.select(state => state.cefrList);
}

export function getEducationPosList(state$: Observable<IEducationState>) {
  return state$.select(state => state.posList);
}

export function getEducationLangList(state$: Observable<IEducationState>) {
  return state$.select(state => state.langList);
}

export function getEducationTopicList(state$: Observable<IEducationState>) {
  return state$.select(state => state.topicList);
}

export function getEducationTask(state$: Observable<IEducationState>) {
  return state$.select(state => state.task);
}
