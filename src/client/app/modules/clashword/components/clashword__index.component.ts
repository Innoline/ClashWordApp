import { ChangeDetectionStrategy, Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../ngrx/index';

import {
  ClashWord,
  ClashWordCartTypeEnum,
  ClashWordUserChestModelApi,
  ClashWordUserCartModelApi,
  ClashWordChestStatusEnum, ClashWordChestTypeEnum, ClashWordCartAdditionalSpecialEnum
} from '../index';
import { CountDownStateEnum } from '../../shared/components/countdown/countdown.model';

@Component({
  moduleId: module.id,
  selector: 'clash-word-index',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'clashword__index.component.html'
})
export class ClashWordIndexComponent implements OnInit {
  @Input() normalChests: ClashWordUserChestModelApi[];
  @Input() freeChest: ClashWordUserChestModelApi;
  @Input() freeChests: ClashWordUserChestModelApi[];
  @Input() freeChestsCount: number;
  @Input() crownChest: ClashWordUserChestModelApi;
  @Input() crownChestUserCtn: number;
  @Input() crownChestTotalCtn: number;
  @Input() userCarts: ClashWordUserCartModelApi[];

  cartTypeEnum = ClashWordCartTypeEnum;
  cartSpecialEnum = ClashWordCartAdditionalSpecialEnum;
  chestTypeEnum = ClashWordChestTypeEnum;
  chestStatusEnum = ClashWordChestStatusEnum;
  countDownStateEnum = CountDownStateEnum;

  gameState: CountDownStateEnum = CountDownStateEnum.Stop;

  constructor(private store: Store<IAppState>) {
  }

  ngOnInit() {
    if(!this.normalChests) {
      this.store.dispatch(new ClashWord.ChestsNormalInProgressAction());
    }
    if(!this.freeChests && !this.crownChest) {
      this.store.dispatch(new ClashWord.ChestsSpecialInProgressAction());
    }
  }

  openNormalChest(model: ClashWordUserChestModelApi) {
    if (model.statusId === this.chestStatusEnum.UnLocked) {
      this.store.dispatch(new ClashWord.ChestOpenInProgressAction(model));
    }
  }

  checkUnlock(item: ClashWordUserChestModelApi) {
    console.log(item.statusId);
    return item.statusId === this.chestStatusEnum.Timer;
  }

  unlockNormalChest(model: ClashWordUserChestModelApi) {
    let chestUnlock = 0;
    for (let i = 0; i < this.normalChests.length; i++) {
      if (this.normalChests[i] !== null) {
        if (this.normalChests[i].statusId === this.chestStatusEnum.Timer) {
          chestUnlock++;
        }
      }
    }

    if (chestUnlock === 0) {
      this.store.dispatch(new ClashWord.ChestUnlockInProgressAction(model));
    }
  }

  openSpecialChest(model: ClashWordUserChestModelApi) {
    if (model.statusId === this.chestStatusEnum.UnLocked) {
      this.store.dispatch(new ClashWord.ChestOpenInProgressAction(model));
    }
  }

  closeCarts() {
    this.store.dispatch(new ClashWord.CartsFromChestInitAction());
    console.info('closeCarts');
  }

  onNormalDuration(model: ClashWordUserChestModelApi) {
    this.store.dispatch(new ClashWord.ChestNormalDurationAction(model));
  }

  onSpecialDuration(model: ClashWordUserChestModelApi) {
    console.log(model);
  }
}
