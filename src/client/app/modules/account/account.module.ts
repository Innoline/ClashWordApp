﻿// angular
import { NgModule, Optional, SkipSelf, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

// libs
import { Ng2CompleterModule } from 'ng2-completer';

// module
import { SharedModule } from '../shared/index';
import { SharedServicesModule } from '../shared.services.module';
import { ACCOUNT_PROVIDERS } from './services/index';
import { ACCOUNT_COMPONENTS } from './components/index';
import { ACCOUNT_PAGES } from './containers/index';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [
    SharedModule,
    SharedServicesModule,
    Ng2CompleterModule
  ],
  declarations: [
    ...ACCOUNT_PAGES,
    ...ACCOUNT_COMPONENTS
  ],
  providers: [
    ...ACCOUNT_PROVIDERS
  ],
  exports: [
    ...ACCOUNT_PAGES,
    ...ACCOUNT_COMPONENTS
  ],
  schemas: [
    NO_ERRORS_SCHEMA,
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AccountModule {

  constructor(@Optional() @SkipSelf() parentModule: AccountModule) {
    if (parentModule) {
      throw new Error('AccountModule already loaded; Import in root module only.');
    }
  }
}
