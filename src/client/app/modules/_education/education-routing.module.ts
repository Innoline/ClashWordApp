import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/authentication.guard';
import { EducationComponent } from './education.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'game/education', component: EducationComponent, canActivate: [AuthGuard]}
    ])
  ],
  exports: [RouterModule]
})
export class EducationRoutingModule { }
