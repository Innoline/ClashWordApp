import { Component, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { LoadingService } from './loading.service';

@Component({
  selector: 'loading-indicator',
  moduleId: module.id,
  templateUrl: 'loading.component.html'
})

export class LoadingComponent implements OnInit, OnDestroy {
  isLoading = false;
  private subscription: any;

  //we probably want a reference to ElementRef here to do some DOM manipulations
  constructor(public el: ElementRef, public loadingService: LoadingService) {
  }

  showOrHideLoadingIndicator(loading: boolean) {
    if (this.isLoading !== loading)
      this.isLoading = !this.isLoading;

    if (this.isLoading)
      this.playLoadingAnimation();
    //else cancel the animation?
    console.log(this.isLoading);
  }

  playLoadingAnimation() {
    //this will be your implementation to start the loading animation
  }

  ngOnInit() {
    this.subscription = this.loadingService.loading$.subscribe(loading => {
        this.showOrHideLoadingIndicator(loading);
      }
    );
  }

  ngOnDestroy() {
    this.subscription.next();
    this.subscription.unsubscribe();
  }
}
