import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SharedServicesModule } from '../shared.services.module';
import { ClashWordIndexComponent } from './clashword__index.component';
import { ClashWordCartsComponent } from './clashword__carts.component';
import { ClashWordBattleComponent } from './clashword__battle.component';
import { ClashWordMainService } from '../services/clashword__main.service';
import { ClashWordChestsService } from '../services/clashword__chests.service';
import { ClashWordMessageService } from '../services/clashword__message.service';
import { ClashWordRoutingModule } from './clashword-routing.module';

@NgModule({
  imports: [SharedModule, SharedServicesModule, ClashWordRoutingModule],
  declarations: [ClashWordIndexComponent, ClashWordCartsComponent, ClashWordBattleComponent],
  exports: [ClashWordIndexComponent, ClashWordCartsComponent, ClashWordBattleComponent],
  providers: [ClashWordMainService, ClashWordChestsService, ClashWordMessageService]
})
export class ClashWordModule { }
