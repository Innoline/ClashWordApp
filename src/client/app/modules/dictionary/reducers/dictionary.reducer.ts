import { IDictionaryState, dictionaryInitialState } from '../index';
import { Dictionary } from '../index';

export function reducer(
  state: IDictionaryState = dictionaryInitialState,
  // could support multiple state actions via union type here
  // ie: NameList.Actions | Other.Actions
  // the seed's example just has one set of actions: NameList.Actions
  action: Dictionary.Actions
): IDictionaryState {
  switch (action.type) {
    case Dictionary.ActionTypes.INITIALIZED:
      return (<any>Object).assign({}, state, {
        names: action.payload
      });

    case Dictionary.ActionTypes.NAME_ADDED:
      return (<any>Object).assign({}, state, {
        names: [...state.names, action.payload]
      });

    default:
      return state;
  }
}
