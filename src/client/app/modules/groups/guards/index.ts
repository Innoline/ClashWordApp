import { GroupCreateGuard } from './group-create.guard';
import { GroupMessagesGuard } from './group-messages.guard';

export const GROUPS_GUARDS: any[] = [
  GroupCreateGuard,
  GroupMessagesGuard
];

export * from './group-create.guard';
export * from './group-messages.guard';
