// libs
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { CookieService } from 'ngx-cookie';

import { Subject } from 'rxjs/Subject';

// app
import { RouterExtensions, Config } from '../../core/index';

import { Account, UserLoginModel, AuthTokenModel, AuthToken } from '../index';
import { IAppState, getAccountToken, getAccountErrors } from '../../ngrx/index';
import { User } from '../../user/index';

declare const uLogin: any;

@Component({
  moduleId: module.id,
  templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {
  undef = undefined;
  public model: UserLoginModel;
  public loading = false;
  public error: string;
  public guestIsShowed: boolean = false;
  public authIsShowed: boolean = false;

  private ngUnSubscribeToken: Subject<void> = new Subject<void>();
  private ngUnSubscribeErrors: Subject<void> = new Subject<void>();
  constructor(private store: Store<IAppState>,
              public routerext: RouterExtensions,
              private cookieService: CookieService) {
  }

  ngOnInit() {
    this.model = {
      username: '',
      password: ''
    };

    uLogin.setStateListener('uLogin', 'receive', ($data: string) => {
      this.store.dispatch(new Account.ULoginAction($data));
    });

    this.store.let(getAccountToken)
      .takeUntil(this.ngUnSubscribeToken)
      .subscribe((result: AuthTokenModel) => {
        console.log(result);
        if (result) {
          this.cookieService.putObject(AuthToken, result);
          this.store.dispatch(new User.SettingsInProgressAction());
          this.routerext.navigate(['/'], {
            transition: {
              duration: 10000,
              name: 'slideTop',
            }
          });
        }
      });

    this.store.let(getAccountErrors)
      .takeUntil(this.ngUnSubscribeErrors)
      .subscribe((result: string) => {
        if (result) {
          this.error = result;
        }
      });
  }

  login() {
    this.loading = true;
    this.store.dispatch(new Account.LoginAction({username: this.model.username, password: this.model.password}));
    this.loading = false;
  }

  guestIsShowedAction() {
    this.guestIsShowed = !this.guestIsShowed;
    this.authIsShowed = false;
  }

  authIsShowedAction() {
    this.authIsShowed = !this.authIsShowed;
    this.guestIsShowed = false;
  }

  restorePassAction() {
    this.loading = true;
  }

  ngOnDestroy() {
    this.ngUnSubscribeToken.next();
    this.ngUnSubscribeToken.complete();
    this.ngUnSubscribeErrors.next();
    this.ngUnSubscribeErrors.complete();
  }
}
