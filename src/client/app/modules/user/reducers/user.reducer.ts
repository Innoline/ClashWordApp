import { IUserState, userInitialState } from '../states/index';
import { User } from '../actions/index';

export function reducer(
  state: IUserState = userInitialState,
  // could support multiple state actions via union type here
  // ie: NameList.Actions | Other.Actions
  // the seed's example just has one set of actions: NameList.Actions
  action: User.Actions
): IUserState {
  switch (action.type) {
    case User.ActionTypes.PROFILE_INIT:
    case User.ActionTypes.SETTINGS_INIT:
      return userInitialState;

    case User.ActionTypes.PROFILE_FAILURE:
    case User.ActionTypes.PROFILE_SAVE_FAILURE:
    case User.ActionTypes.SETTINGS_FAILURE:
    case User.ActionTypes.SETTINGS_SAVE_FAILURE:
      return (<any>Object).assign({}, state, {
        error: action.payload
      });

    case User.ActionTypes.PROFILE_IN_PROGRESS:
    case User.ActionTypes.PROFILE_SAVE_IN_PROGRESS:
    case User.ActionTypes.SETTINGS_IN_PROGRESS:
    case User.ActionTypes.SETTINGS_SAVE_IN_PROGRESS:
      return (<any>Object).assign({}, state, {
        loading: true
      });

    case User.ActionTypes.PROFILE_RECEIVED:
    case User.ActionTypes.PROFILE_SAVE_RECEIVED:
      return (<any>Object).assign({}, state, {
        profile: action.payload,
        loading: false
      });

    case User.ActionTypes.SETTINGS_RECEIVED:
    case User.ActionTypes.SETTINGS_SAVE_RECEIVED:
      return (<any>Object).assign({}, state, {
        cache: action.payload,
        loading: false,
        error: undefined
      });

    case User.ActionTypes.USER_CLAN_SET_KEY:
      let cache = (<any>Object).assign({}, state.cache, {
        clanId: action.payload
      });
      return (<any>Object).assign({}, state, {
        cache: cache,
        loading: false,
        error: undefined
      });

    default:
      return state;
  }
}
