///<reference path="../models/groups.model.ts"/>
import { Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';

import { GroupListItem, GroupAccessEnum, GroupJoinModel } from '../index';

@Component({
  moduleId: module.id,
  selector: 'cw-groups-list-item',
  templateUrl: 'groups_list_item.component.html'
})
export class GroupsListItemComponent {
  @HostBinding('class.c-card')
  @Input() group: GroupListItem;
  @Input() clanId: string;
  @Output() joinGroup: EventEmitter<GroupJoinModel> = new EventEmitter<GroupJoinModel>();

  groupAccessEnum = GroupAccessEnum;

  get id() {
    return this.group.id;
  }

  get title() {
    return this.group.title;
  }

  get description() {
    return this.group.description;
  }

  get access() {
    return this.groupAccessEnum[this.group.accessId];
  }

  get isViewJoinBtn() {
    return !this.clanId && this.group.accessId !== this.groupAccessEnum.Close;
  }

  join() {
    let init = <GroupJoinModel>{
      id: this.group.id,
      accessId: this.group.accessId
    };
    this.joinGroup.emit(init);
  }
}
