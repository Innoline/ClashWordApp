// angular
import { Injectable } from '@angular/core';

// libs
import { Store, Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

// module
import { Education, EducationService } from '../index';

@Injectable()
export class EducationEffects {

  @Effect() list_mode$: Observable<Action> = this.actions$
    .ofType(Education.ActionTypes.LIST_MODE_IN_PROGRESS)
    .switchMap(() => this.dictionaryService.resources('mode'))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.dictionaryService.track(Education.ActionTypes.LIST_MODE_RECEIVED, { label: obj.data.length.toString() });
        return new Education.ListModeReceivedAction(obj.data);
      } else {
        this.dictionaryService.track(Education.ActionTypes.LIST_MODE_FAILURE, { label: obj.errors });
        return new Education.ListModeFailureAction(obj.errors);
      }
    })
    // nothing reacting to failure at moment but you could if you want (here for example)
    .catch((error) => Observable.of(new Education.ListModeFailureAction(error)));

  @Effect() list_cefr$: Observable<Action> = this.actions$
    .ofType(Education.ActionTypes.LIST_CEFR_IN_PROGRESS)
    .switchMap(() => this.dictionaryService.resources('cefr'))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.dictionaryService.track(Education.ActionTypes.LIST_CEFR_RECEIVED, { label: obj.data.length.toString() });
        return new Education.ListCefrReceivedAction(obj.data);
      } else {
        this.dictionaryService.track(Education.ActionTypes.LIST_CEFR_FAILURE, { label: obj.errors });
        return new Education.ListCefrFailureAction(obj.errors);
      }
    })
    // nothing reacting to failure at moment but you could if you want (here for example)
    .catch((error) => Observable.of(new Education.ListCefrFailureAction(error)));

  @Effect() list_pos$: Observable<Action> = this.actions$
    .ofType(Education.ActionTypes.LIST_POS_IN_PROGRESS)
    .switchMap(() => this.dictionaryService.resources('pos'))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.dictionaryService.track(Education.ActionTypes.LIST_POS_RECEIVED, { label: obj.data.length.toString() });
        return new Education.ListPosReceivedAction(obj.data);
      } else {
        this.dictionaryService.track(Education.ActionTypes.LIST_POS_FAILURE, { label: obj.errors });
        return new Education.ListPosFailureAction(obj.errors);
      }
    })
    // nothing reacting to failure at moment but you could if you want (here for example)
    .catch((error) => Observable.of(new Education.ListPosFailureAction(error)));

  @Effect() list_lang$: Observable<Action> = this.actions$
    .ofType(Education.ActionTypes.LIST_LANG_IN_PROGRESS)
    .switchMap(() => this.dictionaryService.resources('lang'))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.dictionaryService.track(Education.ActionTypes.LIST_LANG_RECEIVED, { label: obj.data.length.toString() });
        return new Education.ListLangReceivedAction(obj.data);
      } else {
        this.dictionaryService.track(Education.ActionTypes.LIST_LANG_FAILURE, { label: obj.errors });
        return new Education.ListLangFailureAction(obj.errors);
      }
    })
    // nothing reacting to failure at moment but you could if you want (here for example)
    .catch((error) => Observable.of(new Education.ListLangFailureAction(error)));

  @Effect() list_topics$: Observable<Action> = this.actions$
    .ofType(Education.ActionTypes.LIST_TOPIC_IN_PROGRESS)
    .switchMap(() => this.dictionaryService.topics())
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.dictionaryService.track(Education.ActionTypes.LIST_TOPIC_RECEIVED, { label: obj.data.length.toString() });
        return new Education.ListTopicReceivedAction(obj.data);
      } else {
        this.dictionaryService.track(Education.ActionTypes.LIST_TOPIC_FAILURE, { label: obj.errors });
        return new Education.ListTopicFailureAction(obj.errors);
      }
    })
    // nothing reacting to failure at moment but you could if you want (here for example)
    .catch((error) => Observable.of(new Education.ListTopicFailureAction(error)));

  constructor(
    private store: Store<any>,
    private actions$: Actions,
    private dictionaryService: EducationService
  ) { }
}
