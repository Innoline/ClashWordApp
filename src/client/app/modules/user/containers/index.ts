import { UserInfoPageComponent } from './user_info.page';
import { UserSettingsPageComponent } from './user_settings.page';

export const USER_PAGES: any[] = [
  UserInfoPageComponent,
  UserSettingsPageComponent,
];

export * from './user_info.page';
export * from './user_settings.page';
