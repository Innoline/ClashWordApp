import { Component, Input, OnInit } from '@angular/core';
import {
  ClashWord,
  ClashWordCartAdditionalSpecialEnum,
  ClashWordCartTypeEnum, ClashWordUserCartFilter,
  ClashWordUserCartModelApi
} from '../index';
import { Store } from '@ngrx/store';
import { IAppState } from '../../ngrx/index';

@Component({
  moduleId: module.id,
  selector: 'cw-carts',
  templateUrl: 'clashword__carts.component.html'
})

export class ClashWordCartsComponent implements OnInit {
  @Input() cefrList: string[];
  @Input() typeList: ClashWordCartTypeEnum[];
  @Input() letterList: string[];
  @Input() userCarts: ClashWordUserCartModelApi[];

  filter: ClashWordUserCartFilter;
  cartTypeEnum = ClashWordCartTypeEnum;
  cartSpecialEnum = ClashWordCartAdditionalSpecialEnum;
  constructor(private store: Store<IAppState>) {}

  ngOnInit(): void {
    this.filter = {
      letter: null,
      typeId: null,
      cefr: null
    };
    this.store.dispatch(new ClashWord.InitInProgressAction());
  }

  update(model: ClashWordUserCartModelApi) {
    this.store.dispatch(new ClashWord.CartUpdateInProgressAction(model.key));
  }

  onChangeFromCefr(newValue: string) {
    this.filter.cefr = newValue;
  }

  onChangeFromType(newValue: ClashWordCartTypeEnum) {
    this.filter.typeId = newValue;
  }

  onChangeFromLetter(newValue: string) {
    this.filter.letter = newValue;
  }
}
