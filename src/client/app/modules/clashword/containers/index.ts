import { ClashWordDashboardPageComponent } from './clashword_index.page';
import { ClashWordCartsPageComponent } from './clashword_carts.page';
import { ClashWordBattlePageComponent } from './clashword_battle.page';

export const CLASH_WORD_PAGES: any[] = [
  ClashWordDashboardPageComponent,
  ClashWordCartsPageComponent,
  ClashWordBattlePageComponent
];

export * from './clashword_index.page';
export * from './clashword_carts.page';
export * from './clashword_battle.page';
