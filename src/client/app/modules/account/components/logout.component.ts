import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { CookieService } from 'ngx-cookie';

import { Store } from '@ngrx/store';
import { RouterExtensions, Config } from '../../core/index';
import { getAccountToken, IAppState } from '../../ngrx/index';
import { Account, AuthToken, AuthTokenModel } from '../index';

declare const uLogin: any;

@Component({
  moduleId: module.id,
  template: ''
})

export class LogoutComponent implements OnInit, OnDestroy {
  model: any = {};
  loading = false;
  error = '';

  private ngUnSubscribeToken: Subject<void> = new Subject<void>();
  constructor(private store: Store<IAppState>,
              public routerext: RouterExtensions,
              private cookieService: CookieService) {
  }

  ngOnInit() {
    this.cookieService.putObject(AuthToken, null);
    this.store.dispatch(new Account.InitAction());
    this.store.dispatch(new Account.LogoutInProgressAction());
    this.store.let(getAccountToken)
      .takeUntil(this.ngUnSubscribeToken)
      .subscribe((result: AuthTokenModel) => {
        console.log(result);
        this.cookieService.putObject(AuthToken, null);
        this.routerext.navigate(['/'], {
          transition: {
            duration: 10000,
            name: 'slideTop',
          }
        });
      });
  }


  ngOnDestroy() {
    this.ngUnSubscribeToken.next();
    this.ngUnSubscribeToken.complete();
  }
}
