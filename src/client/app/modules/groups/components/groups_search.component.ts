import 'rxjs/add/operator/let';
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';

import { IAppState } from '../../ngrx/index';
import { GroupsFilter } from '../models/index';
import { Groups } from '../actions/index';


@Component({
  selector: 'cw-groups-search',
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'groups_search.component.html'
})

export class GroupsSearchComponent {
  query: GroupsFilter;

  constructor(private store: Store<IAppState>) {
    const filter_init = {
      title: undefined
    };
    this.query = <GroupsFilter>(filter_init);
  }

  search() {
    if(this.query.title) {
      const filter = {
        title: this.query.title
      };
      this.store.dispatch(new Groups.SearchInProgressAction(filter));
    } else {
      this.store.dispatch(new Groups.SearchInitAction());
    }
  }
}
