import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import {
  ClashWordCartTypeEnum,
  ClashWordUserCartModelApi, ClashWordUserCartsApi, ClashWordUserCartsFromBody
} from '../models/clashword.model';
import { HttpHelper } from './http.helper';
import { Config } from '../core/index';

@Injectable()
export class ClashWordCartsService {
  private mainAPI: string = Config.ENVIRONMENT().API + 'ClashWordGameCarts';
  constructor(private http: HttpHelper) {
  }

  list(): Observable<ClashWordUserCartsApi> {
    return this.http.get(this.mainAPI, true);
  }

  update(model: string): Observable<ClashWordUserCartModelApi> {
    return this.http.put(this.mainAPI, model, true);
  }

  carts(typeKey: ClashWordCartTypeEnum, character: string): Observable<ClashWordUserCartModelApi[]>  {
    let model: ClashWordUserCartsFromBody = {
      TypeKey: typeKey,
      Character: character
    };
    return this.http.post(this.mainAPI + '/byLetter', model, true);
  }
}
