import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/share';

/**
 * Singleton service, injected at app level
 */
export class LoadingService {
  loading$: Observable<boolean>;
  private _observer: Observer<boolean>;

  constructor() {
    this.loading$ = new Observable((data: any) => {
      return this._observer = data;
    }).share();
  }

  toggleLoadingIndicator(loading: boolean) {
    if (this._observer) {
      this._observer.next(loading);
    }
  }
}


