import { UserService } from './user.service';

export const USER_PROVIDERS: any[] = [
  UserService
];

export * from './user.service';
