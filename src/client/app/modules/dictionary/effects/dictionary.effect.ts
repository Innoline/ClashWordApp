// angular
import { Injectable } from '@angular/core';

// libs
import { Store, Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

// module
import { Dictionary, DictionaryService } from '../index';

@Injectable()
export class DictionaryEffects {

  /**
   * This effect makes use of the `startWith` operator to trigger
   * the effect immediately on startup.
   */

  constructor(
    private store: Store<any>,
    private actions$: Actions,
    private dictionaryService: DictionaryService
  ) { }
}
