import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { CompleterItem } from 'ng2-completer';

import { AccountService } from '../../services/account.service';
import { GeoService } from '../../services/geo.service';
import { RegistrationModel } from '../../models/registration.model';

@Component({
  moduleId: module.id,
  templateUrl: 'registration.component.html'
})

export class RegistrationComponent implements OnInit {
  public model: RegistrationModel = new RegistrationModel();
  public loading: boolean = false;
  public error = '';

  public dataGeoCountryCompleter: any;
  public dataGeoRegionCompleter: any;
  public dataGeoCityCompleter: any;

  constructor(private router: Router,
              private accountService: AccountService,
              private geoService: GeoService) {
    this.dataGeoCountryCompleter = this.geoService.completerCountry();
    this.dataGeoRegionCompleter = this.geoService.completerRegion(null);
    this.dataGeoCityCompleter = this.geoService.completerCity(null, null);
  }

  ngOnInit() {
    // reset login status
    this.accountService.logout();
    this.model = new RegistrationModel();
  }

  registration() {
    this.loading = true;
    this.accountService.registration(this.model)
      .subscribe(result => {
          if (result === true) {
            // login successful
            this.router.navigate(['/']);
          } else {
            // login failed
            this.error = 'Username or password is incorrect';
            this.loading = false;
          }
        },
        err => {
          if (err instanceof Response) {
            this.error = err.text();
          } else {
            this.error = err;
          }
          this.loading = false;
        }
      );
  }

  onCountrySelected(selected: CompleterItem) {
    if (selected.originalObject !== null) {
      let countryId = selected.originalObject.id;
      if (this.model.countryId !== countryId) {
        this.model.regionId = null;
        this.model.cityId = null;
        this.dataGeoRegionCompleter = this.geoService.completerRegion(countryId);
        this.dataGeoCityCompleter = this.geoService.completerCity(null, countryId);
      }
      this.model.countryId = countryId;
    }
  }

  onRegionSelected(selected: CompleterItem) {
    if (selected.originalObject !== null) {
      let countryId = selected.originalObject.countryId;
      let regionId = selected.originalObject.id;

      if (this.model.countryId !== countryId) {
        this.model.countryId = countryId;
        this.model.country = selected.originalObject.countryName;
        this.dataGeoRegionCompleter = this.geoService.completerRegion(countryId);
        this.dataGeoCityCompleter = this.geoService.completerCity(regionId, countryId);
      }

      if (this.model.regionId !== regionId) {
        this.model.cityId = null;
        this.model.regionId = regionId;
        this.dataGeoCityCompleter = this.geoService.completerCity(regionId, countryId);
      }
    }
  }

  onCitySelected(selected: CompleterItem) {
    if (selected.originalObject !== null) {
      let countryId = selected.originalObject.countryId;
      let regionId = selected.originalObject.regionId;
      let cityId = selected.originalObject.id;

      if (this.model.countryId !== countryId) {
        this.model.countryId = countryId;
        this.model.country = selected.originalObject.countryName;
        this.dataGeoRegionCompleter = this.geoService.completerRegion(countryId);
      }
      if (this.model.regionId !== regionId) {
        this.model.regionId = regionId;
        this.model.region = selected.originalObject.regionName;
        this.dataGeoCityCompleter = this.geoService.completerCity(regionId, countryId);
      }
      if (this.model.cityId !== cityId) {
        this.model.cityId = cityId;
      }
    }
  }
}
