import { Component, HostBinding, Input } from '@angular/core';
import { UserCacheModel } from '../models/index';
import { Store } from '@ngrx/store';
import { IAppState } from '../../ngrx/index';
import { User } from '../actions/index';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'cw-user-info',
  templateUrl: 'user__info.component.html'
})

export class UserInfoComponent {
  @HostBinding('class.o-container')
  @HostBinding('class.o-container--medium')
  @HostBinding('style.display') get getDisplay(){
    return 'block';
  }
  @Input() info: UserCacheModel;

  loading: boolean = false;
  error: string;

  constructor(private store: Store<IAppState>) {}

  save() {
    this.loading = true;
    console.log(this.info);
    this.store.dispatch(new User.SettingsSaveInProgressAction(this.info));
  }

  reset() {
    this.store.dispatch(new User.SettingsResetAction());
  }
}
