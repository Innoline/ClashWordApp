// angular
import { Injectable } from '@angular/core';

// libs
import { Observable } from 'rxjs/Observable';

// app
import { Config } from '../../core/index';
import { Analytics, AnalyticsService } from '../../analytics/index';

// module
import { Education, SelectListItem, ApiJson, EducationFilter, EducationWordTaskModel, EducationAnswerModel } from '../index';
import { HttpHelper } from '../../services/http.helper';

@Injectable()
export class EducationService extends Analytics {
  private mainAPI: string = Config.ENVIRONMENT().API + 'education';

  constructor(
    public analytics: AnalyticsService,
    private httpHelper: HttpHelper
  ) {
    super(analytics);
    this.category = Education.CATEGORY;
  }

  start(filter: EducationFilter): Observable<ApiJson<EducationWordTaskModel, string>> {
    return this.httpHelper.post(this.mainAPI + '/start', filter, true);
  }

  next(filter: EducationFilter): Observable<ApiJson<EducationWordTaskModel, string>> {
    return this.httpHelper.post(this.mainAPI + '/next', filter, true);
  }

  answer(answerId: string, wordId: string, lang: string): Observable<ApiJson<EducationAnswerModel, string>> {
    let answer = {
      answerId: answerId,
      wordId: wordId,
      lang: lang
    };
    return this.httpHelper.post(this.mainAPI + '/answer', answer, true);
  }

  stop(filter: EducationFilter): Observable<ApiJson<EducationWordTaskModel, string>> {
    return this.httpHelper.post(this.mainAPI + '/stop', filter, true);
  }

  resources(resource: string): Observable<ApiJson<SelectListItem[], string>> {
    return this.httpHelper.get(this.mainAPI + 'resources/' + resource, true);
  }

  topics(): Observable<ApiJson<SelectListItem[], string>> {
    return this.httpHelper.get(this.mainAPI + 'topics/list', true);
  }

  match(fromId: string, destId: string): Observable<ApiJson<boolean, string>> {
    let answer = {
      fromId: fromId,
      destId: destId
    };
    return this.httpHelper.post(this.mainAPI + '/match', answer, true);
  }

  check(wordId: string, word: string, localization: string): Observable<ApiJson<boolean, string>> {
    let answer = {
      wordId: wordId,
      word: word,
      localization: localization
    };
    return this.httpHelper.post(this.mainAPI + '/check', answer, true);
  }

  help(wordId: string, index: number, localization: string): Observable<ApiJson<string, string>>  {
    let answer = {
      wordId: wordId,
      index: index,
      localization: localization
    };
    return this.httpHelper.post(this.mainAPI + '/help', answer, true);
  }}
