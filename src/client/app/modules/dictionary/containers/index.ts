import { DictionaryMainPageComponent } from './dictionary_main.page';

export const DICTIONARY_PAGES: any[] = [
  DictionaryMainPageComponent,
];

export * from './dictionary_main.page';
