import { IAccountState, accountInitialState } from '../states/index';
import { Account } from '../actions/index';

export function reducer(
  state: IAccountState = accountInitialState,
  // could support multiple state actions via union type here
  // ie: NameList.Actions | Other.Actions
  // the seed's example just has one set of actions: NameList.Actions
  action: Account.Actions
): IAccountState {
  switch (action.type) {
    case Account.ActionTypes.USER_IS_AUTHENTICATED_RECEIVED:
      return (<any>Object).assign({}, state,
        {
          isAuth: action.payload,
          error: undefined
        });

    case Account.ActionTypes.INIT:
    case Account.ActionTypes.LOGOUT_RECEIVED:
      return (<any>Object).assign({}, accountInitialState);

    case Account.ActionTypes.LOGIN_FAILURE:
    case Account.ActionTypes.SIGNUP_FAILURE:
    case Account.ActionTypes.LOGOUT_FAILURE:
    case Account.ActionTypes.USER_IS_AUTHENTICATED_FAILURE:
    case Account.ActionTypes.AUTH_TOKEN_EXPIRED:
      return (<any>Object).assign({}, state, {
        isAuth: false,
        error: action.payload,
        token: undefined
      });

    case Account.ActionTypes.LOGIN_IN_PROGRESS:
    case Account.ActionTypes.SIGNUP_IN_PROGRESS:
    case Account.ActionTypes.LOGOUT_IN_PROGRESS:
      return (<any>Object).assign({}, state, {
        loading: true
      });

    case Account.ActionTypes.SET_TOKEN:
      return (<any>Object).assign({}, state, {
        token: action.payload,
        loading: false
      });

    default:
      return state;
  }
}
