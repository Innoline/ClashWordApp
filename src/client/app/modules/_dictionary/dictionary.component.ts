import { Component, OnInit, ViewChild } from '@angular/core';

import { DictionaryList, DictionaryWordModel, DictionaryWordCompleterModel } from '../models/dictionary.model';
import { EducationService } from '../services/education.service';
import { DictionaryService } from '../services/dictionary.service';
import { CompleterData, CompleterItem } from 'ng2-completer';
import { TabsComponent } from '../shared/ui/tabs/tabs.component';
import { SelectListItem } from '../shared/models/index';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'edu-game',
  templateUrl: 'dictionary.component.html',
})

export class DictionaryComponent implements OnInit {
  @ViewChild(TabsComponent)

  public tabs: TabsComponent;

  public loading: boolean = false;
  public disabled: boolean = false;
  public langList: SelectListItem[] = [];
  public words: DictionaryList[];
  public wordService: CompleterData;
  public wordModel: DictionaryWordModel;

  constructor(private educationService: EducationService,
              private dictionaryService: DictionaryService) {
    this.wordModel = new DictionaryWordModel();
    this.wordModel.from = 'en';
    this.wordModel.dest = 'es';
    this.wordService = this.dictionaryService.completerWord(this.wordModel);
  }

  ngOnInit() {
    this.educationService.resources('lang').subscribe(result => {
      this.langList = result;
    });
  }

  searchWord() {
    this.dictionaryService.searchWord(this.wordModel).subscribe(data => {
      this.loading = false;
      this.wordModel.word = null;
      if (Object.keys(data).length > 0) {
        this.words = data;
        console.log(data);
      } else {
        this.words = null;
      }
    });
  }

  onChangeFromLang(newValue: string) {
    this.wordModel.from = newValue;
    this.wordService = this.dictionaryService.completerWord(this.wordModel);
  }

  onChangeDestLang(newValue: string) {
    this.wordModel.dest = newValue;
    this.wordService = this.dictionaryService.completerWord(this.wordModel);
  }

  onWordSelected(selected: CompleterItem) {
    if (selected !== null) {
      this.loading = true;
      let model: DictionaryWordCompleterModel = selected.originalObject;

      this.wordModel.word = model.word;
      if (model.localization === this.wordModel.dest) {
        this.wordModel.dest = this.wordModel.from;
        this.wordModel.from = model.localization;
      }

      this.searchWord();
    }
  }
}
