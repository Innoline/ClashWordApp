import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/authentication.guard';
import { DictionaryComponent } from './dictionary.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'dictionary', component: DictionaryComponent, canActivate: [AuthGuard]}
    ])
  ],
  exports: [RouterModule]
})
export class DictionaryRoutingModule { }
