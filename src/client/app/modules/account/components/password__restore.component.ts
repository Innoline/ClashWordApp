import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from '@angular/http';

import { AccountService } from '../../services/account.service';

@Component({
  moduleId: module.id,
  templateUrl: 'password__restore.component.html'
})

export class PasswordRestoreComponent implements OnInit {
  public model: string;
  public loading = false;
  public error = '';

  constructor(
    private router: Router,
    private accountService: AccountService) { }

  ngOnInit() {
    // reset login status
    this.accountService.logout();
  }

  restore() {
    this.loading = true;
    this.accountService.passwordRestore(this.model)
      .subscribe(result => {
          this.error = result;
          this.loading = false;
        },
        err  => {
          if(err instanceof Response) {
            this.error = err.text();
          } else {
            this.error = err;
          }
          this.loading = false;
        }
      );
  }
}
