// any operators needed throughout your application
import './operators';

// libs
import { Component } from '@angular/core';

// app
import { AnalyticsService } from '../modules/analytics/services/index';
import { LogService, AppService } from '../modules/core/services/index';
import { Config } from '../modules/core/utils/index';
import { Store } from '@ngrx/store';
import { User } from '../modules/user/index';

/**
 * This class represents the main application component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-app',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private store: Store<any>,
    public analytics: AnalyticsService,
    public log: LogService,
    private appService: AppService
  ) {
    log.debug(`Config env: ${Config.ENVIRONMENT().ENV}`);
    this.store.dispatch(new User.SettingsInProgressAction());
  }
}
