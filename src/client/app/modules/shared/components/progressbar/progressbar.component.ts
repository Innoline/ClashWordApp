import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-progress',
  moduleId: module.id,
  templateUrl: 'progressbar.component.html'
})

export class ProgressBarComponent {
  @Input() value: number = 0;
  @Input() max: number = 100;
  @Input() size: string = '';
  @Input() css: string = 'c-progress__bar--error';
  @Input() orientation: string = '';

  get width(): string {
    if (this.orientation === '') {
      return (100 * this.value / this.max).toFixed(2);
    } else {
      return '100';
    }
  }

  get height(): string {
    if (this.orientation !== '') {
      return (100 * this.value / this.max).toFixed(2);
    } else {
      return '100';
    }
  }
}
