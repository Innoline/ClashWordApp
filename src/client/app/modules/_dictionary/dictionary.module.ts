import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SharedServicesModule } from '../shared.services.module';
import { DictionaryComponent } from './dictionary.component';
import { EducationService } from '../services/education.service';
import { Ng2CompleterModule } from 'ng2-completer';
import { DictionaryRoutingModule } from './dictionary-routing.module';

@NgModule({
  imports: [SharedModule, SharedServicesModule, DictionaryRoutingModule, Ng2CompleterModule],
  declarations: [DictionaryComponent],
  exports: [DictionaryComponent],
  providers: [EducationService]
})
export class DictionaryModule { }
