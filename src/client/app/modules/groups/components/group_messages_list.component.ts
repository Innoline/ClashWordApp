import { Input, Component, HostBinding, OnInit } from '@angular/core';

import {
  Groups, GroupListItem, GroupMessageListItem, GroupMessageTypeEnum, GroupMessageStatusEnum
} from '../index';
import { Store } from '@ngrx/store';
import { IAppState } from '../../ngrx/index';


@Component({
  moduleId: module.id,
  selector: 'cw-group-messages-list',
  templateUrl: 'group_messages_list.component.html'
})

export class GroupMessagesListComponent implements OnInit {
  @HostBinding('class.o-grid')
  @HostBinding('class.page-content')

  @Input() messages: GroupMessageListItem[];

  groupMessageTypeEnum = GroupMessageTypeEnum;
  groupMessageStatusEnum = GroupMessageStatusEnum;

  constructor(private store: Store<IAppState>) {
  }

  ngOnInit(): void {
    this.store.dispatch(new Groups.MessagesInProgressAction());
  }
}
