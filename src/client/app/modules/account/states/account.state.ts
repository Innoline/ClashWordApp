import { Observable } from 'rxjs/Observable';
import { AuthTokenModel } from '../index';

export interface IAccountState {
  loading: boolean;
  isAuth: boolean;
  token: AuthTokenModel;
  error: string;
}

export const accountInitialState: IAccountState = {
  isAuth: false,
  loading: false,
  token: undefined,
  error: undefined,
};


export function getAuth(state$: Observable<IAccountState>) {
  return state$.select(state => state.isAuth);
}

export function getToken(state$: Observable<IAccountState>) {
  return state$.select(state => state.token);
}

export function getAccountErrors(state$: Observable<IAccountState>) {
  return state$.select(state => state.error);
}
