import { LoginComponent } from './login.component';
import { LogoutComponent } from './logout.component';
import { PasswordChangeComponent } from './password__change.component';
import { PasswordRestoreComponent } from './password__restore.component';
import { RegistrationComponent } from './registration.component';

export const ACCOUNT_COMPONENTS: any[] = [
  LoginComponent,
  LogoutComponent,
  PasswordChangeComponent,
  PasswordRestoreComponent,
  RegistrationComponent
];

export * from './login.component';
export * from './logout.component';
export * from './password__restore.component';
export * from './password__change.component';
export * from './registration.component';
