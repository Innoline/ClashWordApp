import { EnvConfig } from './env-config.interface';

const ProdConfig: EnvConfig = {
  ENV: 'PROD',
  baseAPI: 'http://socgot.ru/api/v1/',
  API: 'http://clashword.com/api/v1/'
};

export = ProdConfig;
