import { IEducationState, dictionaryInitialState } from '../index';
import { Education } from '../index';

export function reducer(
  state: IEducationState = dictionaryInitialState,
  // could support multiple state actions via union type here
  // ie: NameList.Actions | Other.Actions
  // the seed's example just has one set of actions: NameList.Actions
  action: Education.Actions
): IEducationState {
  switch (action.type) {
    case Education.ActionTypes.LIST_MODE_RECEIVED:
      return (<any>Object).assign({}, state, {
        modeList: action.payload
      });
    case Education.ActionTypes.LIST_CEFR_RECEIVED:
      return (<any>Object).assign({}, state, {
        cefrList: action.payload
      });
    case Education.ActionTypes.LIST_POS_RECEIVED:
      return (<any>Object).assign({}, state, {
        posList: action.payload
      });
    case Education.ActionTypes.LIST_LANG_RECEIVED:
      return (<any>Object).assign({}, state, {
        langList: action.payload
      });
    case Education.ActionTypes.LIST_TOPIC_RECEIVED:
      return (<any>Object).assign({}, state, {
        topicList: action.payload
      });

    default:
      return state;
  }
}
