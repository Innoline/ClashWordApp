export interface EducationWordModel {
  wordId: string;
  pos: string;
  original: string;
  translation: string;
  explanation: string;
  example: string;
  phrase: string;
  sound: string;
  image: string;
}

export class EducationFilter {
  modeId: string;
  cefrId?: string;
  posId?: string;
  topicId?: string;
  fromlang: string;
  destLang: string;
}

export class EducationStateModel {
  isViewWord: boolean;
  isViewAnswers: boolean;
}

export interface EducationAnswerModel {
  isCorrect: boolean;
  key: string;
}

export interface EducationWordLocalizationModel {
  key: string;
  value: string;
  css: string;
}

export interface EducationWordTaskModel {
  sWords: EducationWordLocalizationModel[];
  dWords: EducationWordLocalizationModel[];
  word: EducationWordModel;
  sLetters: string[];
  dLetters: string[];
}
