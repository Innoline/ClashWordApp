import { ClashWordIndexComponent } from './clashword__index.component';
import { ClashWordCartsComponent } from './clashword__carts.component';
import { ClashWordBattleComponent } from './clashword__battle.component';

export const CLASH_WORD_COMPONENTS: any[] = [
  ClashWordIndexComponent,
  ClashWordCartsComponent,
  ClashWordBattleComponent,
];

export * from './clashword__index.component';
export * from './clashword__carts.component';
export * from './clashword__battle.component';
