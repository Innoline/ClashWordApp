// angular
import { Injectable } from '@angular/core';

// libs
import { Store, Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

// module
import { Account, AccountService } from '../index';
import { User } from '../../user/index';

@Injectable()
export class AccountEffects {
  /**
   * This effect makes use of the `startWith` operator to trigger
   * the effect immediately on startup.
   */

  @Effect() auth$: Observable<Action> = this.actions$
    .ofType(Account.ActionTypes.USER_IS_AUTHENTICATED_INIT)
    .startWith(new Account.UserIsAuthenticatedInitAction)
    .switchMap(action  => this.accountService.auth())
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        // analytics
        this.accountService.track(Account.ActionTypes.USER_IS_AUTHENTICATED_RECEIVED, { label: obj.data.toString() });
        if (obj.data) {
          this.store.dispatch(new User.SettingsInitAction());
        }
        return new Account.UserIsAuthenticatedReceivedAction(obj.data);
      } else {
        // analytics
        this.accountService.track(Account.ActionTypes.USER_IS_AUTHENTICATED_FAILURE, { label: obj.errors });
        return new Account.UserIsAuthenticatedFailureAction(obj.errors);
      }
    });

  @Effect() login$: Observable<Action> = this.actions$
    .ofType(Account.ActionTypes.LOGIN)
    .switchMap(action  => this.accountService.login(action.payload))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        // analytics
        this.accountService.track(Account.ActionTypes.SET_TOKEN, { label: obj.data.token });
        return new Account.SetTokenAction(obj.data);
      } else {
        // analytics
        this.accountService.track(Account.ActionTypes.LOGIN_FAILURE, { label: obj.errors });
        this.store.dispatch(new Account.LoginInFailureAction(obj.errors));
        return new Account.SetTokenAction(undefined);
      }
    });

  @Effect() ulogin$: Observable<Action> = this.actions$
    .ofType(Account.ActionTypes.U_LOGIN)
    .switchMap(action  => this.accountService.u_login(action.payload))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        // analytics
        this.accountService.track(Account.ActionTypes.SET_TOKEN, { label: obj.data.token });
        return new Account.SetTokenAction(obj.data);
      } else {
        // analytics
        this.accountService.track(Account.ActionTypes.LOGIN_FAILURE, { label: obj.errors });
        this.store.dispatch(new Account.LoginInFailureAction(obj.errors));
        return new Account.SetTokenAction(undefined);
      }
    });

  @Effect() logout$: Observable<Action> = this.actions$
    .ofType(Account.ActionTypes.LOGOUT_IN_PROGRESS)
    .switchMap(action  => this.accountService.logout())
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        // analytics
        this.accountService.track(Account.ActionTypes.LOGOUT_RECEIVED, { label: 'user is logout' });
        this.store.dispatch(new User.SettingsInitAction());
        return new Account.LogoutReceivedAction(obj.data);
      } else {
        // analytics
        this.accountService.track(Account.ActionTypes.LOGIN_FAILURE, { label: obj.errors });
        return new Account.LoginInFailureAction(obj.errors);
      }
    });

  constructor(
    private store: Store<any>,
    private actions$: Actions,
    private accountService: AccountService
  ) { }
}
