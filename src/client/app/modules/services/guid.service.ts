import { GUID } from './../guid';

import { Injectable } from '@angular/core';

@Injectable()
export class GuidService {

  public generate(): string {
    return GUID.GUID();
  }
}
