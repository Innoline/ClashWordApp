import 'rxjs/add/operator/let';
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IAppState, getUserCache } from '../../ngrx/index';
import { UserCacheModel } from '../../user/models/index';

@Component({
  selector: 'cw-group-create-page',
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'group_create.page.html'
})

export class GroupCreatePageComponent {
  settings$: Observable<UserCacheModel>;

  constructor(private store: Store<IAppState>) {
    this.settings$ = store.let(getUserCache);
  }
}
