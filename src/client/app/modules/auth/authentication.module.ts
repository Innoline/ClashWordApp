import { NgModule, ModuleWithProviders } from '@angular/core';

import { AuthGuard } from './authentication.guard';
import { AccountService } from '../services/account.service';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */
@NgModule({ })
export class AuthenticationModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthenticationModule,
      providers: [
        AuthGuard, AccountService
      ]
    };
  }
}
