// angular
import { NgModule, Optional, SkipSelf, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

// app
import { SharedModule } from '../shared/index';
import { MultilingualModule } from '../i18n/multilingual.module';

import { EDUCATION_SERVICES } from './services/index';
//import { EDUCATION_GUARDS } from './guards/index';
import { EDUCATION_COMPONENTS } from './components/index';
import { EDUCATION_PAGES } from './containers/index';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [
    SharedModule,
    MultilingualModule,
  ],
  declarations: [
    ...EDUCATION_PAGES,
    ...EDUCATION_COMPONENTS
  ],
  providers: [
    ...EDUCATION_SERVICES
    //...EDUCATION_GUARDS
  ],
  schemas: [
    NO_ERRORS_SCHEMA,
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports: [
    ...EDUCATION_PAGES,
    ...EDUCATION_COMPONENTS
  ]
})
export class EducationModule {

  constructor(@Optional() @SkipSelf() parentModule: EducationModule) {
    if (parentModule) {
      throw new Error('SampleModule already loaded; Import in root module only.');
    }
  }
}
