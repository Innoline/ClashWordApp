export class User {
  email: string;
  password: string;
  firstName: string;
  secondName: string;
}

export class UserToken {
  token: string;
  expires: string;
}

export class UserInfo {
  lang: string;
  roleId: string;
  userId: string;
  name: string;
  email: string;
  avatar?: string;
  cover: string;
  alias: string;
  money: string;
  gold: string;
  timestamp: string;
}

export class UserInfoModel {
  dateYear: number;
  dateMonth: number;
  dateDay: number;
  dateBorn?: string;
  genderId?: string;
}

export class UserContactModel {
  street: string;
  home: string;
  flat: string;
  countryId: string;
  regionId: string;
  cityId: string;
  country: string;
  region: string;
  city: string;
  mobileList: ListItem[];
  emailList: ListItem[];
  siteList: ListItem[];
}

export class ListItem {
  key: number;
  value: string;
}

export interface UserSettingModel {
  languageInterface: string;
  languageNative: string;
  languageLearning: string;
}

export interface UserProfileModel {
  balanceGold: string;
  clashWordLevel: number;
  clashWordPoints: number;
  clashWordTrophies: number;
  clashWordZone: number;
}

