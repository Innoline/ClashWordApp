import { Component, Input, Output, EventEmitter, ViewChild, AfterViewInit, OnInit } from '@angular/core';

@Component({
  selector: 'audio-component',
  moduleId: module.id,
  templateUrl: 'audio.component.html',
})

export class AudioComponent implements AfterViewInit, OnInit {
  /**
   * @Input -> custom properties.
   *
   */

  /** Programmatically buttons. */
  @Input() playButton: boolean = false;
  @Input() pauseButton: boolean = false;
  @Input() selectableButton: boolean = false;
  @Input() muteButton: boolean = false;
  /** Array of audio tracks.*/
  @Input() file: string;
  /** Display or not the controls, default: true */
  @Input() controls: boolean = true;
  /** Set autoplay status, default false. */
  @Input() autoplay: boolean = false;
  /** Set loop status, default false. */
  @Input() loop: boolean = false;
  /** Set the volume, default: 1 (max). */
  @Input() volume: number = 1;
  /** Number in s, in order to start the transition, default: 5s */
  @Input() transition: number = 5;
  /** Interval in order to set the audio transition, in ms, default: 500ms. */
  @Input() intervalTransition = 500;
  /** Define if transition, default: false. */
  @Input() transitionEnd: boolean = true;
  /** Define the preload status, default metadata. */
  @Input() transitionStart: boolean = false;
  /** Define the preload status, default metadata. */
  @Input() preload: string = 'metadata';
  /** Define the mute status, default false. */
  @Input() muted: boolean = false;
  /**
   * Custom events who could be intercepted.
   * @type {EventEmitter}
   */
  /** Emit the playlist. */
  @Output() playlist = new EventEmitter();
  /** Emit informations on the current video. */
  @Output() current = new EventEmitter();
  /** Emit the progress status of audio dowloading. */
  @Output() progress = new EventEmitter();
  /** Emit downloading status of track. */
  @Output() downloading = new EventEmitter();

  @ViewChild('audioplayer') player: any;

  private timeout: any;
  private interval: any;
  private startTransition: any;

  ngOnInit() {
  }

  ngAfterViewInit() {

    if (this.transitionEnd) {
      this.player.nativeElement.addEventListener('play', () => {
        this.audioTransition(this.player.nativeElement.duration, this.player.nativeElement.currentTime);
      });
    }

    this.player.nativeElement.addEventListener('ended', () => {

      this.player.nativeElement.volume = this.volume;
      /** Set new src track */
      this.player.nativeElement.src = this.file;
      /** If onChangeTrack is set, then emit the new track. */

    });

    this.player.nativeElement.addEventListener('loadstart', () => {
      this.emitCurrentTrack();

      if (this.transitionStart)
        this.audioStartTransition(this.intervalTransition);
    });

    this.player.nativeElement.addEventListener('pause', () => {
      /** Reset Timeout && Interval. */
      window.clearTimeout(this.timeout);
      window.clearInterval(this.interval);
    });

    this.player.nativeElement.addEventListener('progress', (data: any) => this.downloading.emit(true));
  }
  /** Set programmatically audio controls. */
  play() {
    this.player.nativeElement.play();
  }

  pause() {
    this.player.nativeElement.pause();
  }

  muteVideo() {
    this.player.nativeElement.muted = !this.player.nativeElement.muted;
  }

  /** Audio Transitions */

  /** Set transition audio. */
  audioTransition(trackDuration: number, timeElapsed: number = 0) {
    /** Clear setInterval if defined. */
    window.clearInterval(this.interval);
    /** Check the currentTime elapsed, then set transition if defined. */
    this.timeout = this.setTimeoutDelay(trackDuration, timeElapsed);
  }

  audioStartTransition(interval: number) {
    /** Start the transition. */
    this.startTransition = this.setIncrementInterval(interval);
  }

  setTimeoutDelay(trackDuration: number, timeElapsed: number) {
    /** Timeout who correspond to the remaining time of audio player without the transition's time ( by default 5s before the end). */
    return setTimeout(() => {
      this.interval = this.setDecrementInterval(this.intervalTransition);
    }, (trackDuration - timeElapsed) * 1000 - (this.transition * 1000));
  }

  setIncrementInterval(interval: number) {
    return setInterval(() => {
      /** Define the new player's volume. Increment by step of 10%.*/
      this.player.nativeElement.volume  += (this.player.nativeElement.volume * 10) / 100;
      /** Security area in order to avoid error. If the player's volume is around 90%, then stop incrment, set the volume to 1. */
      if (this.player.nativeElement.volume >= 0.9) {
        this.player.nativeElement.volume = 1;
        window.clearInterval(this.startTransition);
      }
    }, interval);
  }

  setDecrementInterval(interval: number) {
    return setInterval(() => {
      /** Decrement the volume by step of 10%. */
      this.player.nativeElement.volume  -= (this.player.nativeElement.volume * 10) / 100;
    }, interval);
  }

  /**
   * Emitters
   */
  emitPlayList() {
    this.playlist.emit(this.file);
  }

  emitCurrentTrack(): any {
    /**
     * Return an object who will contain: Url of the track, duration, textTrack, volume)
     */
    this.current.emit({
      src: this.player.nativeElement.currentSrc,
      textTracks: this.player.nativeElement.textTracks,
      volume: this.player.nativeElement.volume
    });
  }
}
