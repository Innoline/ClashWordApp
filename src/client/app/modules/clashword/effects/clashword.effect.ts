// angular
import { Injectable } from '@angular/core';

// libs
import { Store, Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

// module
import { ClashWord } from '../index';
import { ClashWordCartsService, ClashWordChestsService } from '../services/index';

@Injectable()
export class ClashWordEffects {

  @Effect() init_mode$: Observable<Action> = this.actions$
    .ofType(ClashWord.ActionTypes.INIT_IN_PROGRESS)
    .switchMap(() => this.clashWordCartsService.list())
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.clashWordCartsService.track(ClashWord.ActionTypes.INIT_RECEIVED, { label: '' });
        return new ClashWord.InitReceivedAction(obj.data);
      } else {
        this.clashWordCartsService.track(ClashWord.ActionTypes.INIT_FAILURE, { label: obj.errors });
        return new ClashWord.InitFailureAction(obj.errors);
      }
    })
    // nothing reacting to failure at moment but you could if you want (here for example)
    .catch((error) => Observable.of(new ClashWord.InitFailureAction(error)));

  @Effect() cart_update$: Observable<Action> = this.actions$
    .ofType(ClashWord.ActionTypes.CART_UPDATE_IN_PROGRESS)
    .switchMap(action => this.clashWordCartsService.update(action.payload))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.clashWordCartsService.track(ClashWord.ActionTypes.CART_UPDATE_RECEIVED, { label: obj.data.word });
        return new ClashWord.CartUpdateReceivedAction(obj.data);
      } else {
        this.clashWordCartsService.track(ClashWord.ActionTypes.CART_UPDATE_FAILURE, { label: obj.errors });
        return new ClashWord.CartUpdateFailureAction(obj.errors);
      }
    })
    // nothing reacting to failure at moment but you could if you want (here for example)
    .catch((error) => Observable.of(new ClashWord.CartUpdateFailureAction(error)));

  @Effect() chests_normal$: Observable<Action> = this.actions$
    .ofType(ClashWord.ActionTypes.CHESTS_NORMAL_IN_PROGRESS)
    .switchMap(() => this.clashWordChestsService.list())
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.clashWordCartsService.track(ClashWord.ActionTypes.CHESTS_NORMAL_RECEIVED, { label: '' });
        return new ClashWord.ChestsNormalReceivedAction(obj.data);
      } else {
        this.clashWordCartsService.track(ClashWord.ActionTypes.CHESTS_NORMAL_FAILURE, { label: obj.errors });
        return new ClashWord.ChestsNormalFailureAction(obj.errors);
      }
    })
    // nothing reacting to failure at moment but you could if you want (here for example)
    .catch((error) => Observable.of(new ClashWord.ChestsNormalFailureAction(error)));

  @Effect() chests_special$: Observable<Action> = this.actions$
    .ofType(ClashWord.ActionTypes.CHESTS_SPECIAL_IN_PROGRESS)
    .switchMap(() => this.clashWordChestsService.special())
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.clashWordCartsService.track(ClashWord.ActionTypes.CHESTS_SPECIAL_RECEIVED, { label: '' });
        return new ClashWord.ChestsSpecialReceivedAction(obj.data);
      } else {
        this.clashWordCartsService.track(ClashWord.ActionTypes.CHESTS_SPECIAL_FAILURE, { label: obj.errors });
        return new ClashWord.ChestsSpecialFailureAction(obj.errors);
      }
    })
    // nothing reacting to failure at moment but you could if you want (here for example)
    .catch((error) => Observable.of(new ClashWord.ChestsSpecialFailureAction(error)));

  @Effect() chest_unlock$: Observable<Action> = this.actions$
    .ofType(ClashWord.ActionTypes.CHEST_UNLOCK_IN_PROGRESS)
    .switchMap(action => this.clashWordChestsService.timer(action.payload))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.clashWordCartsService.track(ClashWord.ActionTypes.CHEST_UNLOCK_RECEIVED, { label: '' });
        return new ClashWord.ChestUnlockReceivedAction(obj.data);
      } else {
        this.clashWordCartsService.track(ClashWord.ActionTypes.CHEST_UNLOCK_FAILURE, { label: obj.errors });
        return new ClashWord.ChestUnlockFailureAction(obj.errors);
      }
    })
    // nothing reacting to failure at moment but you could if you want (here for example)
    .catch((error) => Observable.of(new ClashWord.ChestUnlockFailureAction(error)));

  @Effect() chest_open$: Observable<Action> = this.actions$
    .ofType(ClashWord.ActionTypes.CHEST_OPEN_IN_PROGRESS)
    .switchMap(action => this.clashWordChestsService.open(action.payload))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.clashWordCartsService.track(ClashWord.ActionTypes.CHEST_OPEN_RECEIVED, { label: '' });
        return new ClashWord.ChestOpenReceivedAction(obj.data);
      } else {
        this.clashWordCartsService.track(ClashWord.ActionTypes.CHEST_OPEN_FAILURE, { label: obj.errors });
        return new ClashWord.ChestOpenFailureAction(obj.errors);
      }
    })
    // nothing reacting to failure at moment but you could if you want (here for example)
    .catch((error) => Observable.of(new ClashWord.ChestOpenFailureAction(error)));

  constructor(
    private store: Store<any>,
    private actions$: Actions,
    private clashWordChestsService: ClashWordChestsService,
    private clashWordCartsService: ClashWordCartsService
  ) { }
}
