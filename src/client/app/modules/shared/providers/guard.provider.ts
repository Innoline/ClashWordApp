import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';
import { AuthTokenModel, AuthToken } from '../models/index';

@Injectable()
export class AuthGuardProvider {

  constructor(private cookieService: CookieService) {
  }

  canActivate(): boolean {
    let authToken: AuthTokenModel = <AuthTokenModel>this.cookieService.getObject(AuthToken);
    //console.log(userToken);
    if (authToken && authToken.token.length > 0) {
      let newExpires: Date = new Date(authToken.expires);

      if (typeof (authToken.expires) === 'undefined' || new Date() > newExpires) {
        this.cookieService.putObject(AuthToken, null);
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }
}
