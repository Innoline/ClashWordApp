import { Component, OnInit, Input } from '@angular/core';

import { Store } from '@ngrx/store';
import { IAppState } from '../../ngrx/index';

import { EducationService } from '../services/education.service';
import {
  Education, EducationFilter, EducationWordTaskModel, EducationStateModel,
  EducationWordLocalizationModel, SelectListItem
} from '../index';
import { LoadingService } from '../../shared/components/loading/loading.service';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'cw-education-main',
  templateUrl: 'education.component.html',
})

export class EducationComponent implements OnInit {
  @Input() modeList: SelectListItem[];
  @Input() cefrList: SelectListItem[];
  @Input() posList: SelectListItem[];
  @Input() langList: SelectListItem[];
  @Input() topicList: SelectListItem[];

  public task: EducationWordTaskModel;
  public score: number = 0;
  public words: number = 0;
  public cssButton: string = 'c-button education-button';
  public disabled: boolean = false;

  public duration: number = 3*60/6;
  public isStart: boolean = false;
  public isCorrect: boolean = false;
  public isChecked: boolean = false;
  public isViewHelp: boolean = true;
  public lastWordId: string;

  public filter: EducationFilter = new EducationFilter();
  public state: EducationStateModel = new EducationStateModel();
  public cdState: string = 'stop';

  private sources: number[] = [];

  constructor(private loadingService: LoadingService,
              private educationService: EducationService,
              private store: Store<IAppState>) {
    this.filter.fromlang = 'en';
    this.filter.destLang = 'ru';
    this.onChangeMode('Vocabulary');
  }

  ngOnInit(): void {
    if (!this.modeList) {
      console.log('dispatch: modeList');
      this.store.dispatch(new Education.ListModeInProgressAction());
    }
    if (!this.cefrList) {
      console.log('dispatch: cefrList');
      this.store.dispatch(new Education.ListCefrInProgressAction());
    }
    if (!this.posList) {
      console.log('dispatch: posList');
      this.store.dispatch(new Education.ListPosInProgressAction());
    }
    if (!this.langList) {
      console.log('dispatch: langList');
      this.store.dispatch(new Education.ListLangInProgressAction());
    }
    if (!this.topicList) {
      console.log('dispatch: topicList');
      this.store.dispatch(new Education.ListTopicInProgressAction());
    }
  }


  onFinish(event: any) {
    console.log('isObFinish: ' + event);
  }

  get enabledStart() {
    return this.filter.fromlang !== undefined
      && this.filter.destLang !== undefined
      && this.filter.fromlang !== this.filter.destLang;
  }

  get isViewNext() {
    console.info(this.isCorrect);
    return this.filter.modeId === 'Cart'
      || this.isCorrect === true;
  }

  get isCheck() {
    return (this.filter.modeId === 'Write' || this.filter.modeId === 'Listen') && this.task.sLetters.indexOf('*') === -1;
  }

  start() {
    this.loadingService.toggleLoadingIndicator(true);
    this.educationService.start(this.filter).subscribe(obj => {
      this.isStart = true;
      this.cdState = 'start';
      this.next();
    });
  }

  stop() {
    this.isStart = false;
    this.task = undefined;
    this.cdState = 'stop';
  }

  answer(answer: EducationWordLocalizationModel) {
    if (this.disabled === false) {
      this.words += 1;
      this.loadingService.toggleLoadingIndicator(true);
      this.disabled = true;
      this.educationService.answer(answer.key, this.task.word.wordId, this.filter.destLang).subscribe(obj => {
        this.isCorrect = !obj.data.isCorrect;
        if (obj.data.isCorrect === true) {
          this.score = this.score + 1;
          this.next();
        } else {
          for (let key in this.task.dWords) {
            if (this.task.dWords[key].key === obj.data.key) {
              this.task.dWords[key].css = 'c-button--success';
            } else {
              this.task.dWords[key].css = 'c-button--error';
            }
          }
        }
      },
        error => console.log(error),
        () => this.loadingService.toggleLoadingIndicator(false)
      );
    }
  }

  next() {
    this.educationService.next(this.filter).subscribe(obj => {
      this.disabled = false;
      this.isCorrect = false;

      this.task = obj.data;
      console.info(this.task);

      if (this.filter.modeId === 'Vocabulary' || this.filter.modeId === 'Cart' || this.filter.modeId === 'Audio') {
        for (let key in this.task.dWords) {
          this.task.dWords[key].css = 'c-button--info';
        }
      }

      if (this.filter.modeId === 'Match') {
        for (let key in this.task.sWords) {
          this.task.sWords[key].css = 'c-button--ghost-brand';
        }
        for (let key in this.task.dWords) {
          this.task.dWords[key].css = 'c-button--ghost-brand';
        }
      }

      if (this.filter.modeId === 'Write' || this.filter.modeId === 'Listen') {
        this.task.sLetters.forEach((item, index) => {
          if (item === '*') {
            this.sources.push(index);
          }
        });
      }
    },
      error => console.log(error),
      () => this.loadingService.toggleLoadingIndicator(false)
    );
  }

  letter(letter: string) {
    let at = this.task.sLetters.indexOf('*');
    if (at !== -1) {
      this.task.sLetters[at] = letter;
    }
  }

  check() {
    if (this.disabled === false) {
      this.loadingService.toggleLoadingIndicator(true);
      this.disabled = true;

      this.educationService.check(this.task.word.wordId, this.task.sLetters.join(''), this.filter.destLang).subscribe(obj => {
        this.disabled = false;
        if (obj.data === true) {
          this.score = this.score + 1;
          this.next();
        }
      },
        error => console.log(error),
        () => this.loadingService.toggleLoadingIndicator(false)
      );
    }
  }

  reset() {
    this.sources.forEach((item) => {
      this.task.sLetters[item] = '*';
    });
  }

  help() {
    if (this.disabled === false) {
      this.loadingService.toggleLoadingIndicator(true);
      this.disabled = true;
      let index = this.task.sLetters.indexOf('*');
      this.educationService.help(this.task.word.wordId, index, this.filter.destLang).subscribe(obj => {
        this.disabled = false;
        if (obj.data.length > 0) {
          this.task.sLetters[index] = obj.data;
          let i = this.sources.indexOf(index);
          if(i !== -1) {
            this.sources.splice(i, 1);
          }
        }
      },
        error => console.log(error),
        () => this.loadingService.toggleLoadingIndicator(false)
      );
    }
  }

  sourceMatch(word: EducationWordLocalizationModel) {
    if (word.css !== 'c-button--ghost-info') {
      this.isChecked = true;
      word.css = 'c-button--ghost-info';
      this.lastWordId = word.key;
    }
  }

  answerMatch(word: EducationWordLocalizationModel) {
    if (word.css !== 'c-button--ghost-success') {
      this.educationService.match(this.lastWordId, word.key).subscribe(obj => {
        if (obj.data === true) {
          word.css = 'c-button--ghost-success';
          for (let key in this.task.dWords) {
            if (this.task.dWords[key].css === 'c-button--error') {
              this.task.dWords[key].css = 'c-button--ghost-brand';
            }
          }
          this.isChecked = false;
          let success = this.task.dWords.filter(function(w) {
            return w.css === 'c-button--ghost-success';
          });

          if (success.length === this.task.dWords.length) {
            this.isCorrect = true;
          }
        } else {
          word.css = 'c-button--error';
        }
      },
        error => console.log(error),
        () => this.loadingService.toggleLoadingIndicator(false)
      );
    }
  }

  viewHelp () {
    console.log('isViewHelp: ' +this.isViewHelp);
    this.isViewHelp = !this.isViewHelp;
    console.log('isViewHelp: ' +this.isViewHelp);
  }

  onChangeMode(newValue: string) {
    this.filter.modeId = newValue;
  }

  onChangeCefr(newValue: string) {
    this.filter.cefrId = newValue;
  }

  onChangeTopic(newValue: string) {
    this.filter.topicId = newValue;
  }

  onChangePos(newValue: string) {
    this.filter.posId = newValue;
  }

  onChangeFromLang(newValue: string) {
    this.filter.fromlang = newValue;
  }

  onChangeDestLang(newValue: string) {
    this.filter.destLang = newValue;
  }
}
