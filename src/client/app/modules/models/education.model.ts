export class EducationWord {
  wordId: string;
  pos: string;
  original: string;
  translation: string;
  explanation: string;
  example: string;
  phrase: string;
  sound: string;
  image: string;
}

export class EducationFilter {
  modeId: string;
  cefrId?: string;
  posId?: string;
  topicId?: string;
  fromlang: string;
  destLang: string;
}

export class EducationState {
  isViewWord: boolean;
  isViewAnswers: boolean;
}

export class EducationWordLocalization {
  key: string;
  value: string;
  css: string;
}

export class EducationWordTask {
  sWords: EducationWordLocalization[];
  dWords: EducationWordLocalization[];
  word: EducationWord;
  sLetters: string[];
  dLetters: string[];
}
