import { Action } from '@ngrx/store';
import { type } from '../../core/utils/index';
import { SelectListItem } from '../index';

/**
 * Each action should be namespaced
 * this allows the interior to have similar typed names as other actions
 * however still allow index exports
 */
export namespace Education {
  // Category to uniquely identify the actions
  export const CATEGORY: string = 'Education';

  /**
   * For each action type in an action group, make a simple
   * enum object for all of this group's action types.
   *
   * The 'type' utility function coerces strings into string
   * literal types and runs a simple check to guarantee all
   * action types in the application are unique.
   */
  export interface IEducationActions {
    LIST_MODE_IN_PROGRESS: string;
    LIST_MODE_RECEIVED: string;
    LIST_MODE_FAILURE: string;

    LIST_CEFR_IN_PROGRESS: string;
    LIST_CEFR_RECEIVED: string;
    LIST_CEFR_FAILURE: string;

    LIST_POS_IN_PROGRESS: string;
    LIST_POS_RECEIVED: string;
    LIST_POS_FAILURE: string;

    LIST_LANG_IN_PROGRESS: string;
    LIST_LANG_RECEIVED: string;
    LIST_LANG_FAILURE: string;

    LIST_TOPIC_IN_PROGRESS: string;
    LIST_TOPIC_RECEIVED: string;
    LIST_TOPIC_FAILURE: string;
  }

  export const ActionTypes: IEducationActions = {
    LIST_MODE_IN_PROGRESS: type(`${CATEGORY} List of  In Progress`),
    LIST_MODE_RECEIVED: type(`${CATEGORY} List of  Received`),
    LIST_MODE_FAILURE: type(`${CATEGORY} List of  Failure`),

    LIST_CEFR_IN_PROGRESS: type(`${CATEGORY} List of Cefr In Progress`),
    LIST_CEFR_RECEIVED: type(`${CATEGORY} List of Cefr Received`),
    LIST_CEFR_FAILURE: type(`${CATEGORY} List of Cefr Failure`),

    LIST_POS_IN_PROGRESS: type(`${CATEGORY} List of Pos In Progress`),
    LIST_POS_RECEIVED: type(`${CATEGORY} List of Pos Received`),
    LIST_POS_FAILURE: type(`${CATEGORY} List of Pos Failure`),

    LIST_LANG_IN_PROGRESS: type(`${CATEGORY} List of Lang In Progress`),
    LIST_LANG_RECEIVED: type(`${CATEGORY} List of Lang Received`),
    LIST_LANG_FAILURE: type(`${CATEGORY} List of Lang Failure`),

    LIST_TOPIC_IN_PROGRESS: type(`${CATEGORY} List of Topic In Progress`),
    LIST_TOPIC_RECEIVED: type(`${CATEGORY} List of Topic Received`),
    LIST_TOPIC_FAILURE: type(`${CATEGORY} List of Topic Failure`),
  };

  /**
   * Every action is comprised of at least a type and an optional
   * payload. Expressing actions as classes enables powerful
   * type checking in reducer functions.
   *
   * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
   */

  export class ListModeInProgressAction implements Action {
    type = ActionTypes.LIST_MODE_IN_PROGRESS;

    payload: undefined;
  }

  export class ListModeReceivedAction implements Action {
    type = ActionTypes.LIST_MODE_RECEIVED;

    constructor(public payload: SelectListItem[]) { }
  }

  export class ListModeFailureAction implements Action {
    type = ActionTypes.LIST_MODE_FAILURE;

    constructor(public payload: string) { }
  }

  export class ListCefrInProgressAction implements Action {
    type = ActionTypes.LIST_CEFR_IN_PROGRESS;

    payload: undefined;
  }

  export class ListCefrReceivedAction implements Action {
    type = ActionTypes.LIST_CEFR_RECEIVED;

    constructor(public payload: SelectListItem[]) { }
  }

  export class ListCefrFailureAction implements Action {
    type = ActionTypes.LIST_CEFR_FAILURE;

    constructor(public payload: string) { }
  }

  export class ListPosInProgressAction implements Action {
    type = ActionTypes.LIST_POS_IN_PROGRESS;

    payload: undefined;
  }

  export class ListPosReceivedAction implements Action {
    type = ActionTypes.LIST_POS_RECEIVED;

    constructor(public payload: SelectListItem[]) { }
  }

  export class ListPosFailureAction implements Action {
    type = ActionTypes.LIST_POS_FAILURE;

    constructor(public payload: string) { }
  }

  export class ListLangInProgressAction implements Action {
    type = ActionTypes.LIST_LANG_IN_PROGRESS;

    payload: undefined;
  }

  export class ListLangReceivedAction implements Action {
    type = ActionTypes.LIST_LANG_RECEIVED;

    constructor(public payload: SelectListItem[]) { }
  }

  export class ListLangFailureAction implements Action {
    type = ActionTypes.LIST_LANG_FAILURE;

    constructor(public payload: string) { }
  }

  export class ListTopicInProgressAction implements Action {
    type = ActionTypes.LIST_TOPIC_IN_PROGRESS;

    payload: undefined;
  }

  export class ListTopicReceivedAction implements Action {
    type = ActionTypes.LIST_TOPIC_RECEIVED;

    constructor(public payload: SelectListItem[]) { }
  }

  export class ListTopicFailureAction implements Action {
    type = ActionTypes.LIST_TOPIC_FAILURE;

    constructor(public payload: string) { }
  }

  /**
   * Export a type alias of all actions in this action group
   * so that reducers can easily compose action types
   */
  export type Actions
    = ListModeInProgressAction
    | ListModeReceivedAction
    | ListModeFailureAction
    | ListCefrInProgressAction
    | ListCefrReceivedAction
    | ListCefrFailureAction
    | ListPosInProgressAction
    | ListPosReceivedAction
    | ListPosFailureAction
    | ListLangInProgressAction
    | ListLangReceivedAction
    | ListLangFailureAction
    | ListTopicInProgressAction
    | ListTopicReceivedAction
    | ListTopicFailureAction
    ;
}
