import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/authentication.guard';
import { ClashWordIndexComponent } from './clashword__index.component';
import { ClashWordCartsComponent } from './clashword__carts.component';
import { ClashWordBattleComponent } from './clashword__battle.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'cw/index', component: ClashWordIndexComponent, canActivate: [AuthGuard] },
      { path: 'cw/battle', component: ClashWordBattleComponent, canActivate: [AuthGuard] },
      { path: 'cw/carts', component: ClashWordCartsComponent, canActivate: [AuthGuard] },
    ])
  ],
  exports: [RouterModule]
})
export class ClashWordRoutingModule { }
