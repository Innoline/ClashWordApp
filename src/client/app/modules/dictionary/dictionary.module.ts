// angular
import { NgModule, Optional, SkipSelf, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

// app
import { SharedModule } from '../shared/index';
import { MultilingualModule } from '../i18n/multilingual.module';
import { Ng2CompleterModule } from 'ng2-completer';

import { DICTIONARY_SERVICES } from './services/index';
//import { DICTIONARY_GUARDS } from './guards/index';
import { DICTIONARY_COMPONENTS } from './components/index';
import { DICTIONARY_PAGES } from './containers/index';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [
    SharedModule,
    MultilingualModule,
    Ng2CompleterModule,
  ],
  declarations: [
    ...DICTIONARY_PAGES,
    ...DICTIONARY_COMPONENTS
  ],
  providers: [
    ...DICTIONARY_SERVICES
    //...DICTIONARY_GUARDS
  ],
  schemas: [
    NO_ERRORS_SCHEMA,
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports: [
    ...DICTIONARY_PAGES,
    ...DICTIONARY_COMPONENTS
  ]
})
export class DictionaryModule {

  constructor(@Optional() @SkipSelf() parentModule: DictionaryModule) {
    if (parentModule) {
      throw new Error('SampleModule already loaded; Import in root module only.');
    }
  }
}
