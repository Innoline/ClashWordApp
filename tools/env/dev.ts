import { EnvConfig } from './env-config.interface';

const DevConfig: EnvConfig = {
  ENV: 'DEV',
  // Sample API url
  baseAPI: 'http://socgot.ru/api/v1/', //2045
  API: 'http://localhost:5001/api/v1/' //или 5001
};

export = DevConfig;

