import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { UserHelper, AccountService } from '../services/index';
import { CookieService } from 'ngx-cookie';
import { UserToken } from '../models/index';
import { AuthToken } from '../shared/models/index';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router,
              private userHelper: UserHelper,
              private accountService: AccountService,
              private cookieService: CookieService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      let userToken: UserToken = <UserToken>this.cookieService.getObject(AuthToken);
      //console.log(userToken);
      if (userToken && userToken.token.length > 0) {
        let newExpires: Date = new Date(userToken.expires);

        if (typeof (userToken.expires) === 'undefined' || new Date() > newExpires) {
          this.accountService.logout();
          this.router.navigate(['/login']);

          return false;
        } else {
          let userAlias = route.params['alias'];
          if (userAlias === undefined) {
            this.userHelper.isView = false;
            this.userHelper.reset();
          } else {
            if (this.userHelper.userAlias !== userAlias) {
              this.userHelper.isView = true;
              this.userHelper.reset();
              this.userHelper.userAlias = userAlias;
            }
          }
          this.userHelper.urls = route.url;

          return true;
        }
      } else {
        // not logged in so redirect to login page
        this.router.navigate(['/login']);

        return false;
      }
  }
}
