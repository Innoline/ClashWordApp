import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ClashWordMessageModel } from '../models/clashword.model';
import { HttpHelper } from './http.helper';
import { Config } from '../core/index';

@Injectable()
export class ClashWordMessageService {
  private messages: ClashWordMessageModel[];
  private mainAPI: string = Config.ENVIRONMENT().API + 'ClashWordGameChat';

  constructor(private http: HttpHelper) {
    this.messages = [
      {
        key: 'goodLuck',
        isImage: false,
        text: 'Удачи!'
      },
      {
        key: 'wellPlayed',
        isImage: false,
        text: 'Отличная игра!'
      },
      {
        key: 'wow',
        isImage: false,
        text: 'Ух ты'
      },
      {
        key: 'thanks',
        isImage: false,
        text: 'Спасибо!'
      },
      {
        key: 'goodGame',
        isImage: false,
        text: 'Хорошая Игра'
      },
      {
        key: 'oops',
        isImage: false,
        text: 'Опаньки'
      }
    ];
  }

  list() {
    return this.messages;
  }

  get(key: string): ClashWordMessageModel {
    let index = this.find(this.messages, key);
    if (index > -1) {
      return this.messages[index];
    } else {
      return null;
    }
  }

  chat(chatKey: string): Observable<string> {
    return this.http.post(this.mainAPI, chatKey, true);
  }

  private find(array: ClashWordMessageModel[], value: string) {
    for (var i = 0; i < array.length; i++) {
      if (array[i].key === value) return i;
    }

    return -1;
  }
}

