import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IAppState, getClashWordLetterList, getClashWordCefrList,
  getClashWordTypeList, getClashWordCarts } from '../../ngrx/index';
import { SelectListItem, ClashWordCartTypeEnum, ClashWordUserCartModelApi } from '../index';


@Component({
  selector: 'cw-dashboard-page',
  moduleId: module.id,
  //changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'clashword_battle.page.html'
})

export class ClashWordBattlePageComponent {

  constructor(private store: Store<IAppState>) {
  }
}
