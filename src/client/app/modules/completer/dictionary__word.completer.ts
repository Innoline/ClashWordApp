import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { CompleterData, CompleterItem } from 'ng2-completer';
import { DictionaryWordModel, DictionaryWordCompleterModel } from '../models/dictionary.model';
import { HttpHelper } from '../services/http.helper';

export class DictionaryWordCompleter extends Subject<CompleterItem[]> implements CompleterData {
  private remoteSearch: Subscription;

  constructor(private http: HttpHelper,
              private wordModel: DictionaryWordModel,
              private Url: string) {
    super();
  }

  public search(term: string): void {
    this.cancel();
    this.wordModel.word = term;
    this.remoteSearch = this.http.post(this.Url, this.wordModel, true)
      .map((data: DictionaryWordCompleterModel[]) => {
        // Convert the result to CompleterItem[]
        let matches: CompleterItem[] = data.map((item: DictionaryWordCompleterModel) => {
          return {
            title: item.word,
            description: item.localization + ' (' + item.pos + ')',
            originalObject: item
          };
        });
        this.next(matches);
      })
      .subscribe(data => {
        console.info(data);
      });
  }

  public cancel() {
    if (this.remoteSearch) {
      this.remoteSearch.unsubscribe();
    }
  }
}
