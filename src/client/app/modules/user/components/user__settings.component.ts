import { Component, Input } from '@angular/core';
import { UserCacheModel } from '../models/index';
import { Store } from '@ngrx/store';
import { IAppState } from '../../ngrx/index';
import { User } from '../actions/index';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'cw-user-settings',
  templateUrl: 'user__settings.component.html'
})

export class UserSettingsComponent {
  @Input() settings: UserCacheModel;

  loading: boolean = false;
  error: string;

  constructor(private store: Store<IAppState>) {}

  save() {
    this.loading = true;
    console.log(this.settings);
    this.store.dispatch(new User.SettingsSaveInProgressAction(this.settings));
  }
}
