import { CookieService } from 'ngx-cookie';

import { AuthenticationModule } from './auth/authentication.module';
import {
  HttpHelper, UserHelper, AccountService, GeoService, EducationService,
  DictionaryService, ClashWordMainService, ClashWordChestsService,
  ClashWordMessageService, ClashWordCartsService
} from './services/index';
import { LoadingService } from './shared/components/loading/loading.service';
import { ModuleWithProviders, NgModule } from '@angular/core';
/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [AuthenticationModule.forRoot()]
})

export class SharedServicesModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedServicesModule,
      providers: [
        HttpHelper,
        UserHelper,

        LoadingService,
        CookieService,

        AccountService,
        GeoService,
        EducationService,
        DictionaryService,
        ClashWordMainService,
        ClashWordChestsService,
        ClashWordMessageService,
        ClashWordCartsService
      ]
    };
  }
}
