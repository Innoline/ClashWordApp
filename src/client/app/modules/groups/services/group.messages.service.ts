// angular
import { Injectable } from '@angular/core';

// libs
import { Observable } from 'rxjs/Observable';

// app
import { Config } from '../../core/index';
import { Analytics, AnalyticsService } from '../../analytics/index';

// module
import { Groups } from '../actions/index';
import { ApiJson, GroupMessageListItem } from '../models/index';
import { HttpHelper } from '../../services/http.helper';

@Injectable()
export class GroupMessagesService extends Analytics {
  private mainAPI: string = Config.ENVIRONMENT().API + 'groupmessages';

  constructor(
    public analytics: AnalyticsService,
    private httpHelper: HttpHelper
  ) {
    super(analytics);
    this.category = Groups.CATEGORY;
  }

  list(): Observable<ApiJson<GroupMessageListItem[], string>> {
    return this.httpHelper.get(this.mainAPI, true);
  }

  add(model: GroupMessageListItem): Observable<ApiJson<GroupMessageListItem, string>>  {
    return this.httpHelper.post(this.mainAPI, model, true);
  }
}
