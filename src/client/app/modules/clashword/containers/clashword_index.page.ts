import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IAppState, getClashWordChestNormal,
  getClashWordFreeChest, getClashWordFreeChests, getClashWordFreeChestsCount,
  getClashWordCrownChest, getClashWordCrownChestUserCtn, getClashWordCrownChestTotalCtn,
  getClashWordCartsFromChest } from '../../ngrx/index';
import { SelectListItem, ClashWordUserChestModelApi } from '../index';


@Component({
  selector: 'cw-dashboard-page',
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'clashword_index.page.html'
})

export class ClashWordDashboardPageComponent {
  normalChests$: Observable<ClashWordUserChestModelApi[]>;
  freeChest$: Observable<ClashWordUserChestModelApi>;
  freeChests$: Observable<ClashWordUserChestModelApi[]>;
  freeChestsCount$: Observable<number>;
  crownChest$: Observable<ClashWordUserChestModelApi>;
  crownChestUserCtn$: Observable<number>;
  crownChestTotalCtn$: Observable<number>;
  userCarts$: Observable<ClashWordUserChestModelApi>;

  constructor(private store: Store<IAppState>) {
    this.normalChests$ = store.let(getClashWordChestNormal);
    this.freeChest$ = store.let(getClashWordFreeChest);
    this.freeChests$ = store.let(getClashWordFreeChests);
    this.freeChestsCount$ = store.let(getClashWordFreeChestsCount);
    this.crownChest$ = store.let(getClashWordCrownChest);
    this.crownChestUserCtn$ = store.let(getClashWordCrownChestUserCtn);
    this.crownChestTotalCtn$ = store.let(getClashWordCrownChestTotalCtn);
    this.userCarts$ = store.let(getClashWordCartsFromChest);
  }
}
