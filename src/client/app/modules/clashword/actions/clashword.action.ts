import { Action } from '@ngrx/store';
import { type } from '../../core/utils/index';
import { ClashWordUserCartsApi, ClashWordUserCartModelApi, ClashWordUserChestModelApi,
  ClashWordUserChestSpecialModelApi, ClashWordUserOpenedChestModel } from '../index';

/**
 * Each action should be namespaced
 * this allows the interior to have similar typed names as other actions
 * however still allow index exports
 */
export namespace ClashWord {
  // Category to uniquely identify the actions
  export const CATEGORY: string = 'ClashWord';

  /**
   * For each action type in an action group, make a simple
   * enum object for all of this group's action types.
   *
   * The 'type' utility function coerces strings into string
   * literal types and runs a simple check to guarantee all
   * action types in the application are unique.
   */
  export interface IClashWordActions {
    INIT_IN_PROGRESS: string;
    INIT_RECEIVED: string;
    INIT_FAILURE: string;

    CART_UPDATE_IN_PROGRESS: string;
    CART_UPDATE_RECEIVED: string;
    CART_UPDATE_FAILURE: string;

    CHESTS_NORMAL_IN_PROGRESS: string;
    CHESTS_NORMAL_RECEIVED: string;
    CHESTS_NORMAL_FAILURE: string;

    CHESTS_SPECIAL_IN_PROGRESS: string;
    CHESTS_SPECIAL_RECEIVED: string;
    CHESTS_SPECIAL_FAILURE: string;

    CHEST_UNLOCK_IN_PROGRESS: string;
    CHEST_UNLOCK_RECEIVED: string;
    CHEST_UNLOCK_FAILURE: string;

    CHEST_OPEN_IN_PROGRESS: string;
    CHEST_OPEN_RECEIVED: string;
    CHEST_OPEN_FAILURE: string;

    CHEST_NORMAL_DURATION: string;

    CARTS_FROM_CHEST_INIT: string;
    CARTS_FROM_CHEST_IN_PROGRESS: string;
    CARTS_FROM_CHEST_RECEIVED: string;
    CARTS_FROM_CHEST_FAILURE: string;
  }

  export const ActionTypes: IClashWordActions = {
    INIT_IN_PROGRESS: type(`${CATEGORY} Init In Progress`),
    INIT_RECEIVED: type(`${CATEGORY} Init Received`),
    INIT_FAILURE: type(`${CATEGORY} Init Failure`),

    CART_UPDATE_IN_PROGRESS: type(`${CATEGORY} Cart Update In Progress`),
    CART_UPDATE_RECEIVED: type(`${CATEGORY} Cart Update Received`),
    CART_UPDATE_FAILURE: type(`${CATEGORY} Cart Update Failure`),

    CHESTS_NORMAL_IN_PROGRESS: type(`${CATEGORY} Chests Normal In Progress`),
    CHESTS_NORMAL_RECEIVED: type(`${CATEGORY} Chests Normal Received`),
    CHESTS_NORMAL_FAILURE: type(`${CATEGORY} Chests Normal Failure`),

    CHESTS_SPECIAL_IN_PROGRESS: type(`${CATEGORY} Chests Special In Progress`),
    CHESTS_SPECIAL_RECEIVED: type(`${CATEGORY} Chests Special Received`),
    CHESTS_SPECIAL_FAILURE: type(`${CATEGORY} Chests Special Failure`),

    CHEST_UNLOCK_IN_PROGRESS: type(`${CATEGORY} Chest Unlock In Progress`),
    CHEST_UNLOCK_RECEIVED: type(`${CATEGORY} Chest Unlock Received`),
    CHEST_UNLOCK_FAILURE: type(`${CATEGORY} Chest Unlock Failure`),

    CHEST_OPEN_IN_PROGRESS: type(`${CATEGORY} Chest Open In Progress`),
    CHEST_OPEN_RECEIVED: type(`${CATEGORY} Chest Open Received`),
    CHEST_OPEN_FAILURE: type(`${CATEGORY} Chest Open Failure`),

    CHEST_NORMAL_DURATION: type(`${CATEGORY} Chest Normal Duration`),

    CARTS_FROM_CHEST_INIT: type(`${CATEGORY} Carts From Chests  Init`),
    CARTS_FROM_CHEST_IN_PROGRESS: type(`${CATEGORY} Carts From Chests In Progress`),
    CARTS_FROM_CHEST_RECEIVED: type(`${CATEGORY} Carts From Chests Received`),
    CARTS_FROM_CHEST_FAILURE: type(`${CATEGORY} Carts From Chests Failure`),
  };

  /**
   * Every action is comprised of at least a type and an optional
   * payload. Expressing actions as classes enables powerful
   * type checking in reducer functions.
   *
   * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
   */

  export class InitInProgressAction implements Action {
    type = ActionTypes.INIT_IN_PROGRESS;

    payload: undefined;
  }

  export class InitReceivedAction implements Action {
    type = ActionTypes.INIT_RECEIVED;

    constructor(public payload: ClashWordUserCartsApi) { }
  }

  export class InitFailureAction implements Action {
    type = ActionTypes.INIT_FAILURE;

    constructor(public payload: string) { }
  }

  export class CartUpdateInProgressAction implements Action {
    type = ActionTypes.CART_UPDATE_IN_PROGRESS;

    constructor(public payload: string) { }
  }

  export class CartUpdateReceivedAction implements Action {
    type = ActionTypes.CART_UPDATE_RECEIVED;

    constructor(public payload: ClashWordUserCartModelApi) { }
  }

  export class CartUpdateFailureAction implements Action {
    type = ActionTypes.CART_UPDATE_FAILURE;

    constructor(public payload: string) { }
  }

  export class ChestsNormalInProgressAction implements Action {
    type = ActionTypes.CHESTS_NORMAL_IN_PROGRESS;

    payload: undefined;
  }

  export class ChestsNormalReceivedAction implements Action {
    type = ActionTypes.CHESTS_NORMAL_RECEIVED;

    constructor(public payload: ClashWordUserChestModelApi[]) { }
  }

  export class ChestsNormalFailureAction implements Action {
    type = ActionTypes.CHESTS_NORMAL_FAILURE;

    constructor(public payload: string) { }
  }

  export class ChestsSpecialInProgressAction implements Action {
    type = ActionTypes.CHESTS_SPECIAL_IN_PROGRESS;

    payload: undefined;
  }

  export class ChestsSpecialReceivedAction implements Action {
    type = ActionTypes.CHESTS_SPECIAL_RECEIVED;

    constructor(public payload: ClashWordUserChestSpecialModelApi) { }
  }

  export class ChestsSpecialFailureAction implements Action {
    type = ActionTypes.CHESTS_SPECIAL_FAILURE;

    constructor(public payload: string) { }
  }

  export class ChestUnlockInProgressAction implements Action {
    type = ActionTypes.CHEST_UNLOCK_IN_PROGRESS;

    constructor(public payload: ClashWordUserChestModelApi) { }
  }

  export class ChestUnlockReceivedAction implements Action {
    type = ActionTypes.CHEST_UNLOCK_RECEIVED;

    constructor(public payload: ClashWordUserChestModelApi) { }
  }

  export class ChestUnlockFailureAction implements Action {
    type = ActionTypes.CHEST_UNLOCK_FAILURE;

    constructor(public payload: string) { }
  }

  export class ChestOpenInProgressAction implements Action {
    type = ActionTypes.CHEST_OPEN_IN_PROGRESS;

    constructor(public payload: ClashWordUserChestModelApi) { }
  }

  export class ChestOpenReceivedAction implements Action {
    type = ActionTypes.CHEST_OPEN_RECEIVED;

    constructor(public payload: ClashWordUserOpenedChestModel) { }
  }

  export class ChestOpenFailureAction implements Action {
    type = ActionTypes.CHEST_OPEN_FAILURE;

    constructor(public payload: string) { }
  }

  export class ChestNormalDurationAction implements Action {
    type = ActionTypes.CHEST_NORMAL_DURATION;

    constructor(public payload: ClashWordUserChestModelApi) { }
  }

  export class CartsFromChestInitAction implements Action {
    type = ActionTypes.CARTS_FROM_CHEST_INIT;

    payload: undefined;
  }

  export class CartsFromChestInProgressAction implements Action {
    type = ActionTypes.CARTS_FROM_CHEST_IN_PROGRESS;

    payload: undefined;
  }

  export class CartsFromChestReceivedAction implements Action {
    type = ActionTypes.CARTS_FROM_CHEST_RECEIVED;

    constructor(public payload: ClashWordUserCartModelApi[]) { }
  }

  export class CartsFromChestFailureAction implements Action {
    type = ActionTypes.CARTS_FROM_CHEST_FAILURE;

    constructor(public payload: string) { }
  }

  /**
   * Export a type alias of all actions in this action group
   * so that reducers can easily compose action types
   */
  export type Actions
    = InitInProgressAction
    | InitReceivedAction
    | InitFailureAction
    | CartUpdateInProgressAction
    | CartUpdateReceivedAction
    | CartUpdateFailureAction
    | ChestsNormalInProgressAction
    | ChestsNormalReceivedAction
    | ChestsNormalFailureAction
    | ChestsSpecialInProgressAction
    | ChestsSpecialReceivedAction
    | ChestsSpecialFailureAction
    | ChestUnlockInProgressAction
    | ChestUnlockReceivedAction
    | ChestUnlockFailureAction
    | ChestOpenInProgressAction
    | ChestOpenReceivedAction
    | ChestOpenFailureAction
    | ChestNormalDurationAction
    | CartsFromChestInitAction
    | CartsFromChestInProgressAction
    | CartsFromChestReceivedAction
    | CartsFromChestFailureAction
    ;
}
