import { Component, OnInit } from '@angular/core';
import {
  SignalRConnectionStatus, ClashWordWordModel, ClashWordModel, ClashWordCartTypeEnum,
  ClashWordWordAnswerModel, ClashWordUserModel, ClashWordUserChestModelApi, ClashWordMessageModel,
  ClashWordUserCartModelApi,
  ClashWordChestStatusEnum, ClashWordChestTypeEnum, ClashWordCartAdditionalSpecialEnum
} from '../models/clashword.model';
import { ClashWordMainService } from '../services/clashword__main.service';
import { ClashWordCartsService } from '../services/clashword__carts.service';
import { ClashWordMessageService } from '../services/clashword__message.service';
import { AccountService } from '../services/account.service';
import { CountDownStateEnum } from '../shared/components/countdown/countdown.model';
import { LoadingService } from '../shared/components/loading/loading.service';

@Component({
  moduleId: module.id,
  selector: 'clash-word',
  templateUrl: 'clashword__battle.component.html'
})
export class ClashWordBattleComponent implements OnInit {
  cartTypeEnum = ClashWordCartTypeEnum;
  cartSpecialEnum = ClashWordCartAdditionalSpecialEnum;
  chestTypeEnum = ClashWordChestTypeEnum;
  chestStatusEnum = ClashWordChestStatusEnum;
  chestStateEnum = CountDownStateEnum;

  isViewAnswer: boolean = false;
  isViewChat: boolean = false;
  isViewCarts: boolean = false;
  listChatMessages: ClashWordMessageModel[];
  connectionId: string;
  currentUserId: string;
  currentGame: ClashWordModel;
  currentWord: ClashWordWordModel;
  currentAnswer: ClashWordWordAnswerModel;
  userInfo: ClashWordUserModel;
  userOpponent: ClashWordUserModel;
  userCarts: ClashWordUserCartModelApi[];

  crownChest: ClashWordUserChestModelApi;
  freeChest: ClashWordUserChestModelApi;
  freeChestsCount: number = 0;
  normalChests: ClashWordUserChestModelApi[] = [];
  gameState: CountDownStateEnum = CountDownStateEnum.Stop;
  crownChestUserCtn: number;
  crownChestTotalCtn: number;

  private error: any;

  constructor(private loadingService: LoadingService,
              private accountService: AccountService,
              private clashWordMainService: ClashWordMainService,
              private clashWordCartsService: ClashWordCartsService,
              private clashWordMessageService: ClashWordMessageService) {
  }

  ngOnInit() {
    this.listChatMessages = this.clashWordMessageService.list();
    this.accountService.getUserInfo().subscribe(
      user => this.currentUserId = user.userId,
      error => console.log(error)
    );

    this.listenConnectionState();
    this.listenConnectionId();
    this.listenAnswer();
    this.listenWord();
    this.listenGame();

    this.loadingService.toggleLoadingIndicator(true);
    if (this.clashWordMainService.currentState === SignalRConnectionStatus.Disconnected) {
      this.clashWordMainService.start().subscribe(
        data => this.startGame(),
        error => console.log(error)
      );
    } else {
      this.startGame();
    }
  }

  get isCurrentUser(): boolean {
    return this.currentUserId === this.currentGame.CurrentUserId;
  }

  startGame(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.stopGame();

    this.clashWordMainService.newGame().subscribe(
      data => {
        console.log(data);
        this.isViewAnswer = false;
        this.gameState = this.chestStateEnum.Work;
      },
      error => console.log(error)
    );
  }

  stopGame(): void {
    this.currentGame = null;
    this.currentWord = null;
    this.currentAnswer = null;
  }

  nextStep() {
    this.loadingService.toggleLoadingIndicator(true);
    this.clashWordMainService.nextStep(this.currentGame.GameKey).subscribe(
      data => {
        this.isViewAnswer = false;
      },
      error => console.log(error),
      () => this.loadingService.toggleLoadingIndicator(false)
    );
  }

  answer(model: ClashWordWordAnswerModel) {
    let self = this;
    if (self.isViewAnswer === false && self.isCurrentUser === true) {
      self.isViewAnswer = true;
      self.currentAnswer = model;
      self.clashWordMainService.answer(this.currentGame.GameKey, model.Key, false).subscribe(
        data => {
          setTimeout(function() {
            self.isViewAnswer = false;
          }, 1000);
        },
        error => console.log(error),
        () => this.loadingService.toggleLoadingIndicator(false)
      );
    }
  }

  useCart(model: ClashWordUserCartModelApi) {
    let self = this;
    if (self.isViewAnswer === false && self.isCurrentUser === true) {
      self.isViewAnswer = true;
      self.clashWordMainService.answer(this.currentGame.GameKey, model.key, true).subscribe(
        data => {
          self.userCarts = null;
          self.isViewCarts = !self.isViewCarts;

          setTimeout(function() {
            self.isViewAnswer = false;
          }, 1000);
        },
        error => console.log(error),
        () => this.loadingService.toggleLoadingIndicator(false)
      );
    }
  }

  setClassAnswer(isRight: boolean) {
    let classes = {
      'c-card__item--success': isRight,
      'c-card__item--error': !isRight
    };
    return classes;
  }

  showCarts(type: ClashWordCartTypeEnum) {
    this.isViewCarts = !this.isViewCarts;
    let self = this;
    if (this.isViewCarts) {
      this.clashWordCartsService.carts(type, this.currentWord.Character).subscribe(
        data => {
          self.userCarts = data;
        },
        error => console.log(error)
      );
    }
  }

  toggleChat() {
    this.isViewChat = !this.isViewChat;
  }

  addChatMessage(model: ClashWordMessageModel) {
    this.toggleChat();
    this.clashWordMessageService.chat(model.key).subscribe(
      null,
      error => console.log(error)
    );
  }

  onFinish(event: any) {
    console.log('isObFinish: ' + event);
  }

  listenConnectionState(): void {
    this.clashWordMainService.connectionState
      .subscribe(
        connectionState => {
          console.log('connectionState! ' + connectionState);
          if (connectionState === SignalRConnectionStatus.Connected) {
            console.log('Connected!');
            this.clashWordMainService.init();
          } else {
            console.log(connectionState.toString());
          }
        },
        error => {
          this.error = error;
          console.log(error);
        });
  }

  listenConnectionId(): void {
    // Listen for connected / disconnected events
    this.clashWordMainService.setConnectionId.subscribe(
      id => {
        this.connectionId = id;
      },
      error => {
        this.error = error;
        console.log(error);
      });
  }

  listenWord(): void {
    this.clashWordMainService.setGameWord.subscribe(
      word => {
        this.currentWord = word;
        this.loadingService.toggleLoadingIndicator(false);
      },
      error => {
        this.error = error;
        console.log(error);
      });
  }

  listenGame(): void {
    let self = this;
    self.clashWordMainService.setGame.subscribe(
      game => {
        if (self.currentUserId === game.User.UserId) {
          self.userInfo = game.User;
          self.userOpponent = game.Owner;
        } else {
          self.userInfo = game.Owner;
          self.userOpponent = game.User;
        }

        if (this.crownChestTotalCtn > 0 && !game.IsPlay && game.WinnerUserId && game.WinnerUserId === this.currentUserId) {
          this.crownChestUserCtn++;
          if (this.crownChestUserCtn >= this.crownChestTotalCtn) {
            this.crownChest.statusId = this.chestStatusEnum.UnLocked;
            this.crownChestTotalCtn = 0;
          }
        }

        self.currentGame = game;

        if (game.User.Message !== null) {
          self.currentGame.User.MessageModel = self.clashWordMessageService.get(game.User.Message);
        }

        if (game.Owner.Message !== null) {
          self.currentGame.Owner.MessageModel = self.clashWordMessageService.get(game.Owner.Message);
        }
      },
      error => {
        this.error = error;
        console.log(error);
      });
  }

  listenAnswer(): void {
    this.clashWordMainService.setLastAnswer.subscribe(
      answer => {
        this.currentAnswer = answer;
      },
      error => {
        this.error = error;
        console.log(error);
      });
  }
}
