import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IndexComponent } from './index.component';
import { AuthGuard } from '../../modules/auth/authentication.guard';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: IndexComponent, canActivate: [AuthGuard] }
    ])
  ],
  exports: [RouterModule]
})
export class IndexRoutingModule { }
