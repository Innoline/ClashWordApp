import { Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';

import { GroupMessageListItem, GroupMessageTypeEnum, GroupMessageStatusEnum } from '../index';

@Component({
  moduleId: module.id,
  selector: 'cw-group-messages-list-item',
  templateUrl: 'group_messages_list_item.component.html'
})
export class GroupMessagesListItemComponent {
  @HostBinding('class.c-card')
  @Input() message: GroupMessageListItem;

  groupMessageTypeEnum = GroupMessageTypeEnum;
  groupMessageStatusEnum = GroupMessageStatusEnum;

  get id() {
    return this.message.id;
  }

  get content() {
    return this.message.message;
  }

  get type() {
    return this.groupMessageTypeEnum[this.message.typeId];
  }

  get status() {
    return this.groupMessageStatusEnum[this.message.statusId];
  }
}
