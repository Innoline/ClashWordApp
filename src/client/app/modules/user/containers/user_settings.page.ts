import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IAppState, getUserCache } from '../../ngrx/index';
import { UserCacheModel } from '../models/index';


@Component({
  selector: 'cw-user-settings-page',
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'user_settings.page.html'
})

export class UserSettingsPageComponent {
  settings$: Observable<UserCacheModel>;

  constructor(private store: Store<IAppState>) {
    this.settings$ = store.let(getUserCache);
  }
}
