import { Http, Response, RequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { CompleterData, CompleterItem } from 'ng2-completer';
import { GeoCountryModel } from '../models/geo.model';

export class GeoCountryCompleter extends Subject<CompleterItem[]> implements CompleterData {
  private remoteSearch: Subscription;

  constructor(private http: Http,
              private geoCountryModel: GeoCountryModel,
              private requestOptions: RequestOptions,
              private Url: string) {
    super();
  }

  public search(term: string): void {
    this.cancel();
    this.geoCountryModel.query = term;

    this.remoteSearch = this.http.post(this.Url, JSON.stringify(this.geoCountryModel), this.requestOptions)
      .map((res: Response) => {
        // Convert the result to CompleterItem[]
        let data = res.json();
        let matches: CompleterItem[] = data.map((item: any) => {
          return {
            title: item.title,
            //image: item.image,
            originalObject: item
          };
        });
        this.next(matches);
      })
      .subscribe();
  }

  public cancel() {
    if (this.remoteSearch) {
      this.remoteSearch.unsubscribe();
    }
  }
}
