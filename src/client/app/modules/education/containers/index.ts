import { EducationMainPageComponent } from './education_main.page';

export const EDUCATION_PAGES: any[] = [
  EducationMainPageComponent,
];

export * from './education_main.page';
