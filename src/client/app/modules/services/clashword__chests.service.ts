import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {
  ClashWordUserChestModelApi, ClashWordUserChestSpecialModelApi,
  ClashWordUserOpenedChestModel
} from '../models/clashword.model';
import { HttpHelper } from './http.helper';
import { Config } from '../core/index';

@Injectable()
export class ClashWordChestsService {
  private mainAPI: string = Config.ENVIRONMENT().API + 'ClashWordGameChests';
  constructor(private http: HttpHelper) {
  }

  list(): Observable<ClashWordUserChestModelApi[]> {
    return this.http.get(this.mainAPI, true);
  }

  special(): Observable<ClashWordUserChestSpecialModelApi> {
    return this.http.get(this.mainAPI + '/special', true);
  }

  open(model: ClashWordUserChestModelApi): Observable<ClashWordUserOpenedChestModel> {
    return this.http.post(this.mainAPI + '/open', model, true);
  }

  timer(model: ClashWordUserChestModelApi): Observable<ClashWordUserChestModelApi> {
    return this.http.post(this.mainAPI + '/timer', model, true);
  }

  unlock(model: ClashWordUserChestModelApi): Observable<ClashWordUserChestModelApi> {
    return this.http.post(this.mainAPI + '/unlock', model, true);
  }
}
