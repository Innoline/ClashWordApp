// libs
import { Observable } from 'rxjs/Observable';
// import { combineLatest } from 'rxjs/observable/combineLatest';
import { ActionReducer } from '@ngrx/store';
import '@ngrx/core/add/operator/select';

/**
 * The compose function is one of our most handy tools. In basic terms, you give
 * it any number of functions and it returns a function. This new function
 * takes a value and chains it through every composed function, returning
 * the output.
 *
 * More: https://drboolean.gitbooks.io/mostly-adequate-guide/content/ch5.html
 */
import { compose } from '@ngrx/core/compose';

/**
 * storeFreeze prevents state from being mutated. When mutation occurs, an
 * exception will be thrown. This is useful during development mode to
 * ensure that none of the reducers accidentally mutates the state.
 */
import { storeFreeze } from 'ngrx-store-freeze';

/**
 * combineReducers is another useful metareducer that takes a map of reducer
 * functions and creates a new reducer that stores the gathers the values
 * of each reducer and stores them using the reducer's key. Think of it
 * almost like a database, where every reducer is a table in the db.
 *
 * More: https://egghead.io/lessons/javascript-redux-implementing-combinereducers-from-scratch
 */
import { combineReducers } from '@ngrx/store';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */
import * as fromMultilingual from '../i18n/index';
import * as fromSample from '../sample/index';
import * as fromAccount from '../account/index';
import * as fromUser from '../user/index';
import * as fromClashWord from '../clashword/index';
import * as fromGroups from '../groups/index';
import * as fromEducation from '../education/index';
import * as fromDictionary from '../dictionary/index';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface IAppState {
  i18n: fromMultilingual.IMultilingualState;
  sample: fromSample.ISampleState;
  account: fromAccount.IAccountState;
  user: fromUser.IUserState;
  clashWord: fromClashWord.IClashWordState;
  groups: fromGroups.IGroupsState;
  education: fromEducation.IEducationState;
  dictionary: fromDictionary.IDictionaryState;
}

/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */
const reducers = {
  i18n: fromMultilingual.reducer,
  sample: fromSample.reducer,
  account: fromAccount.reducer,
  user: fromUser.reducer,
  clashWord: fromClashWord.reducer,
  groups: fromGroups.reducer,
  education: fromEducation.reducer,
  dictionary: fromDictionary.reducer,
};

// ensure state is frozen as extra level of security when developing
// helps maintain immutability
const developmentReducer: ActionReducer<IAppState> = compose(storeFreeze, combineReducers)(reducers);
// for production, dev has already been cleared so no need
const productionReducer: ActionReducer<IAppState> = combineReducers(reducers);

export function AppReducer(state: any, action: any) {
  if (String('<%= BUILD_TYPE %>') === 'dev') {
    return developmentReducer(state, action);
  } else {
    return productionReducer(state, action);
  }
}

export function getMultilingualState(state$: Observable<IAppState>): Observable<fromMultilingual.IMultilingualState> {
  return state$.select(s => s.i18n);
}
export function getNameListState(state$: Observable<IAppState>): Observable<fromSample.ISampleState> {
  return state$.select(s => s.sample);
}
export function getAccountState(state$: Observable<IAppState>): Observable<fromAccount.IAccountState> {
  return state$.select(s => s.account);
}
export function getUserState(state$: Observable<IAppState>): Observable<fromUser.IUserState> {
  return state$.select(s => s.user);
}
export function getClashWordState(state$: Observable<IAppState>): Observable<fromClashWord.IClashWordState> {
  return state$.select(s => s.clashWord);
}
export function getGroupsState(state$: Observable<IAppState>): Observable<fromGroups.IGroupsState> {
  return state$.select(s => s.groups);
}
export function getEducationState(state$: Observable<IAppState>): Observable<fromEducation.IEducationState> {
  return state$.select(s => s.education);
}
export function getDictionaryState(state$: Observable<IAppState>): Observable<fromDictionary.IDictionaryState> {
  return state$.select(s => s.dictionary);
}

export const getAuth: any = compose(fromAccount.getAuth, getAccountState);

export const getLang: any = compose(fromMultilingual.getLang, getMultilingualState);
export const getNames: any = compose(fromSample.getNames, getNameListState);

export const getAccountToken: any = compose(fromAccount.getToken, getAccountState);
export const getAccountErrors: any = compose(fromAccount.getAccountErrors, getAccountState);

export const getUserProfile: any = compose(fromUser.getProfile, getUserState);
export const getUserCache: any = compose(fromUser.getCache, getUserState);

export const getClashWordLetterList: any = compose(fromClashWord.getClashWordLetterList, getClashWordState);
export const getClashWordCefrList: any = compose(fromClashWord.getClashWordCefrList, getClashWordState);
export const getClashWordTypeList: any = compose(fromClashWord.getClashWordTypeList, getClashWordState);
export const getClashWordCarts: any = compose(fromClashWord.getClashWordCarts, getClashWordState);
export const getClashWordChestNormal: any = compose(fromClashWord.getClashWordChestNormal, getClashWordState);
export const getClashWordFreeChest: any = compose(fromClashWord.getClashWordFreeChest, getClashWordState);
export const getClashWordFreeChests: any = compose(fromClashWord.getClashWordFreeChests, getClashWordState);
export const getClashWordFreeChestsCount: any = compose(fromClashWord.getClashWordFreeChestsCount, getClashWordState);
export const getClashWordCrownChest: any = compose(fromClashWord.getClashWordCrownChest, getClashWordState);
export const getClashWordCrownChestUserCtn: any = compose(fromClashWord.getClashWordCrownChestUserCtn, getClashWordState);
export const getClashWordCrownChestTotalCtn: any = compose(fromClashWord.getClashWordCrownChestTotalCtn, getClashWordState);
export const getClashWordCartsFromChest: any = compose(fromClashWord.getClashWordCartsFromChest, getClashWordState);

export const getGroupUser: any = compose(fromGroups.getGroupUser, getGroupsState);
export const getGroupCurrent: any = compose(fromGroups.getGroupCurrent, getGroupsState);
export const getGroupsList: any = compose(fromGroups.getGroupsList, getGroupsState);
export const getGroupMessagesList: any = compose(fromGroups.getGroupMessagesList, getGroupsState);
export const getGroupsErrors: any = compose(fromGroups.getGroupsErrors, getGroupsState);

export const getEducationModeList: any = compose(fromEducation.getEducationModeList, getEducationState);
export const getEducationCefrList: any = compose(fromEducation.getEducationCefrList, getEducationState);
export const getEducationPosList: any = compose(fromEducation.getEducationPosList, getEducationState);
export const getEducationLangList: any = compose(fromEducation.getEducationLangList, getEducationState);
export const getEducationTopicList: any = compose(fromEducation.getEducationTopicList, getEducationState);
export const getEducationTask: any = compose(fromEducation.getEducationTask, getEducationState);
