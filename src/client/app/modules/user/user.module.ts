﻿// angular
import { NgModule, Optional, SkipSelf, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

// libs

// module
import { SharedModule } from '../shared/index';
import { USER_PROVIDERS } from './services/index';
import { USER_COMPONENTS } from './components/index';
import { USER_PAGES } from './containers/index';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    ...USER_PAGES,
    ...USER_COMPONENTS
  ],
  providers: [
    ...USER_PROVIDERS
  ],
  exports: [
    ...USER_PAGES,
    ...USER_COMPONENTS
  ],
  schemas: [
    NO_ERRORS_SCHEMA,
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class UserModule {

  constructor(@Optional() @SkipSelf() parentModule: UserModule) {
    if (parentModule) {
      throw new Error('UserModule already loaded; Import in root module only.');
    }
  }
}
