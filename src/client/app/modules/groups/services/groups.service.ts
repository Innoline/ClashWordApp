// angular
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

// libs
import { Observable } from 'rxjs/Observable';

// app
import { Config } from '../../core/index';
import { Analytics, AnalyticsService } from '../../analytics/index';

// module
import { Groups } from '../actions/index';
import { ApiJson, GroupListItem, GroupsFilter, GroupInfoModel, GroupUserModel } from '../models/index';
import { HttpHelper } from '../../services/http.helper';

@Injectable()
export class GroupsService extends Analytics {
  private mainAPI: string = Config.ENVIRONMENT().API + 'clans';

  constructor(
    public analytics: AnalyticsService,
    private httpHelper: HttpHelper
  ) {
    super(analytics);
    this.category = Groups.CATEGORY;
  }

  get(): Observable<ApiJson<GroupUserModel, string>> {
    return this.httpHelper.get(this.mainAPI, true);
  }

  info(id: string): Observable<ApiJson<GroupInfoModel, string>> {
    return this.httpHelper.get(this.mainAPI + '/' + id, true);
  }

  search(model: GroupsFilter): Observable<ApiJson<GroupListItem[], string>> {
    return this.httpHelper.post(this.mainAPI + '/search', model, true);
  }

  add(model: GroupListItem): Observable<ApiJson<GroupListItem, string>>  {
    return this.httpHelper.post(this.mainAPI, model, true);
  }

  join(id: string): Observable<ApiJson<string, string>> {
    return this.httpHelper.post(this.mainAPI + '/join', id, true);
  }

  leave(id: string): Observable<ApiJson<boolean, string>> {
    return this.httpHelper.get(this.mainAPI + '/leave/' + id, true);
  }
}
