import 'rxjs/add/operator/let';
import { Component, ChangeDetectionStrategy, HostBinding, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { IAppState, getUserCache, getGroupMessagesList, getGroupUser } from '../../ngrx/index';
import { Groups, GroupListItem, GroupMessageListItem, SignalRConnectionStatus } from '../index';
import { UserCacheModel } from '../../user/index';
import { GroupsHub } from '../services/index';

@Component({
  selector: 'cw-group-messages-page',
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'group_messages.page.html'
})

export class GroupMessagePageComponent implements OnInit, OnDestroy {
  @HostBinding('class.page-main')

  group$: Observable<GroupListItem>;
  settings$: Observable<UserCacheModel>;
  messages$: Observable<GroupMessageListItem[]>;

  status = SignalRConnectionStatus;
  subConnectionState: Subscription;
  subConnectionId: Subscription;
  subMessage: Subscription;
  private connectionId: string;

  constructor(
    private store: Store<IAppState>,
    private groupHub: GroupsHub
  ) {
    this.settings$ = store.let(getUserCache);
    this.messages$ = store.let(getGroupMessagesList);
    this.group$ = store.let(getGroupUser);
  }

  ngOnInit(): void {
    this.listenConnectionState();
    this.listenConnectionId();
    this.listenMessage();
  }

  ngOnDestroy(): void {
    this.subConnectionState.unsubscribe();
    this.subConnectionId.unsubscribe();
    this.subMessage.unsubscribe();
  }

  leave(id: string) {
    this.store.dispatch(new Groups.GroupLeaveInProgressAction(id));
  }

  listenConnectionState(): void {
    this.subConnectionState = this.groupHub.connectionState
      .subscribe(
        connectionState => {
          console.log('connectionState! ' + connectionState);
          if (connectionState === this.status.Connected) {
            console.log('Connected!');
            this.groupHub.init();
          } else {
            console.log(connectionState.toString());
          }
        },
        error => {
          console.log(error);
        });
  }

  listenConnectionId(): void {
    // Listen for connected / disconnected events
    this.subConnectionId = this.groupHub.setConnectionId.subscribe(
      id => {
        this.connectionId = id;
      },
      error => {
        console.log(error);
      });
  }

  listenMessage(): void {
    // Listen for connected / disconnected events
    this.subMessage = this.groupHub.setMessage.subscribe(
      obj => {
        console.log(obj);
      },
      error => {
        console.log(error);
      });
  }
}
