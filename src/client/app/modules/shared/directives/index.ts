import { OnlyNumberDirective } from './onlynumber.directive';

export const SHARED_DIRECTIVES: any[] = [
  OnlyNumberDirective
];

export * from './onlynumber.directive';
