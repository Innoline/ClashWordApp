import { ClashWordCartsPipe } from './clashword__carts.pipe';

export const SHARED_PIPES: any[] = [
  ClashWordCartsPipe
];

export * from './clashword__carts.pipe';
