import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

// app
import { Config } from '../../core/index';
import { Analytics, AnalyticsService } from '../../analytics/index';

import { HttpHelper } from '../../services/http.helper';
import {
  ClashWord, ApiJson,
  ClashWordCartTypeEnum,
  ClashWordUserCartModelApi, ClashWordUserCartsApi, ClashWordUserCartsFromBody
} from '../index';

@Injectable()
export class ClashWordCartsService extends Analytics {
  private mainAPI: string = Config.ENVIRONMENT().API + 'ClashWordGameCarts';
  constructor(
    public analytics: AnalyticsService,
    private httpHelper: HttpHelper
  ) {
    super(analytics);
    this.category = ClashWord.CATEGORY;
  }

  list(): Observable<ApiJson<ClashWordUserCartsApi, string>> {
    return this.httpHelper.get(this.mainAPI, true);
  }

  update(model: string): Observable<ApiJson<ClashWordUserCartModelApi, string>> {
    return this.httpHelper.put(this.mainAPI, model, true);
  }

  carts(typeKey: ClashWordCartTypeEnum, character: string): Observable<ApiJson<ClashWordUserCartModelApi[], string>>  {
    let model: ClashWordUserCartsFromBody = {
      TypeKey: typeKey,
      Character: character
    };
    return this.httpHelper.post(this.mainAPI + '/byLetter', model, true);
  }
}
