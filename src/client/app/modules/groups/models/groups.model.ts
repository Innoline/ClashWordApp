export interface GroupListItem {
  id: string;
  title: string;
  description: string;
  visibilityId: GroupVisibilityEnum;
  accessId: GroupAccessEnum;
  typeId: string;
  trophies: number;
}

export interface GroupUserModel {
  id: string;
  title: string;
  description: string;
  trophies: number;
}

export interface GroupInfoModel {
  id: string;
  title: string;
  description: string;
  visibilityId: GroupVisibilityEnum;
  accessId: GroupAccessEnum;
  typeId: string;
  trophies: number;
}

export interface GroupsFilter {
  title: string;
}

export interface GroupJoinModel {
  id: string;
  accessId: GroupAccessEnum;
}

export enum GroupVisibilityEnum {
  Error = 0,
  /// <summary>
  /// Доступно всем
  /// </summary>
  Public = 1,
    /// <summary>
    /// Закрытые группы
    /// </summary>
  Internal = 2,
    /// <summary>
    /// Секретные группы
    /// </summary>
  Private = 3
}

export enum GroupAccessEnum {
  /// <summary>
  /// открытая
  /// </summary>
  Open = 1,
    /// <summary>
    /// по приглашению
    /// </summary>
  Invite = 2,
    /// <summary>
    /// закрытый
    /// </summary>
  Close = 3
}
