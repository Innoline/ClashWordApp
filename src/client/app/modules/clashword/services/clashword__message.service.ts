import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ClashWord, ClashWordMessageModel, ApiJson } from '../index';
// app
import { Config } from '../../core/index';
import { Analytics, AnalyticsService } from '../../analytics/index';

import { HttpHelper } from '../../services/http.helper';

@Injectable()
export class ClashWordMessageService extends Analytics {
  private messages: ClashWordMessageModel[];
  private mainAPI: string = Config.ENVIRONMENT().API + 'ClashWordGameChat';

  constructor(
    public analytics: AnalyticsService,
    private httpHelper: HttpHelper
  ) {
    super(analytics);
    this.category = ClashWord.CATEGORY;
    this.messages = [
      {
        key: 'goodLuck',
        isImage: false,
        text: 'Удачи!'
      },
      {
        key: 'wellPlayed',
        isImage: false,
        text: 'Отличная игра!'
      },
      {
        key: 'wow',
        isImage: false,
        text: 'Ух ты'
      },
      {
        key: 'thanks',
        isImage: false,
        text: 'Спасибо!'
      },
      {
        key: 'goodGame',
        isImage: false,
        text: 'Хорошая Игра'
      },
      {
        key: 'oops',
        isImage: false,
        text: 'Опаньки'
      }
    ];
  }

  list() {
    return this.messages;
  }

  get(key: string): ClashWordMessageModel {
    let index = this.find(this.messages, key);
    if (index > -1) {
      return this.messages[index];
    } else {
      return null;
    }
  }

  chat(chatKey: string): Observable<ApiJson<string, string>> {
    return this.httpHelper.post(this.mainAPI, chatKey, true);
  }

  private find(array: ClashWordMessageModel[], value: string) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].key === value) return i;
    }

    return -1;
  }
}

