import { NavbarComponent } from './navbar/navbar.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AudioComponent } from './audio/audio.component';
import { CountDownComponent } from './countdown/countdown.component';
import { ProgressBarComponent } from './progressbar/progressbar.component';
import { LoadingComponent } from './loading/loading.component';
import { OverlayComponent } from './overlay__loading.component';

export const SHARED_COMPONENTS: any[] = [
  NavbarComponent,
  ToolbarComponent,
  AudioComponent,
  CountDownComponent,
  ProgressBarComponent,
  LoadingComponent,
  OverlayComponent
];

export * from './navbar/navbar.component';
export * from './toolbar/toolbar.component';
export * from './overlay__loading.component';
export * from './countdown/countdown.component';
export * from './audio/audio.component';
export * from './progressbar/progressbar.component';
export * from './loading/loading.component';

