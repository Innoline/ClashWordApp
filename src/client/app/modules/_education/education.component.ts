import { Component, OnInit } from '@angular/core';

import { SelectListItem } from '../shared/models/index';
import { EducationService } from '../services/education.service';
import {
  EducationFilter, EducationWordTask, EducationState,
  EducationWordLocalization
} from '../models/education.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'edu-game',
  templateUrl: 'education.component.html',
})

export class EducationComponent implements OnInit {
  public score: number = 0;
  public words: number = 0;
  public modes: SelectListItem[] = [];
  public topics: SelectListItem[] = [];
  public poses: SelectListItem[] = [];
  public cefrs: SelectListItem[] = [];
  public langs: SelectListItem[] = [];
  public task: EducationWordTask;
  public cssButton: string = 'c-button education-button';
  public loading: boolean = false;
  public disabled: boolean = false;

  public duration: number = 3*60/6;
  public isStart: boolean = false;
  public isCorrect: boolean = false;
  public isChecked: boolean = false;
  public isViewHelp: boolean = true;
  public lastWordId: string;

  public filter: EducationFilter = new EducationFilter();
  public state: EducationState = new EducationState();
  public cdState: string = 'stop';

  private sources: number[] = [];

  constructor(private educationService: EducationService) {
    this.filter.fromlang = 'en';
    this.filter.destLang = 'ru';
    this.onChangeMode('Vocabulary');
  }

  ngOnInit() {
    Observable.forkJoin(
      [
        this.educationService.resources('mode'),
        this.educationService.resources('cefr'),
        this.educationService.resources('pos'),
        this.educationService.resources('lang'),
        this.educationService.topics()
      ]
    ).subscribe(data => {
      this.modes = data[0];
      this.cefrs = data[1];
      this.poses = data[2];
      this.langs = data[3];
      this.topics = data[4];
    });
  }

  onFinish(event: any) {
    console.log('isObFinish: ' + event);
  }

  get enabledStart() {
    return this.filter.fromlang !== undefined
      && this.filter.destLang !== undefined
      && this.filter.fromlang !== this.filter.destLang;
  }

  get isViewNext() {
    console.info(this.isCorrect);
    return this.filter.modeId === 'Cart'
      || this.isCorrect === true;
  }

  get isCheck() {
    return (this.filter.modeId === 'Write' || this.filter.modeId === 'Listen') && this.task.sLetters.indexOf('*') === -1;
  }

  start() {
    this.loading = true;
    this.educationService.start(this.filter).subscribe(data => {
      this.isStart = true;
      this.cdState = 'start';
      this.next();
    });
  }

  stop() {
    this.isStart = false;
    this.task = undefined;
    this.cdState = 'stop';
  }

  answer(answer: EducationWordLocalization) {
    if (this.disabled === false) {
      this.words += 1;
      this.loading = true;
      this.disabled = true;
      this.educationService.answer(answer.key, this.task.word.wordId, this.filter.destLang).subscribe(data => {
        this.isCorrect = !data.isCorrect;
        if (data.isCorrect === true) {
          this.score = this.score + 1;
          this.next();
        } else {
          this.loading = false;
          for (let key in this.task.dWords) {
            if (this.task.dWords[key].key === data.key) {
              this.task.dWords[key].css = 'c-button--success';
            } else {
              this.task.dWords[key].css = 'c-button--error';
            }
          }
        }
      });
    }
  }

  next() {
    this.educationService.next(this.filter).subscribe(data => {
      this.loading = false;
      this.disabled = false;
      this.isCorrect = false;

      this.task = data;
      console.info(this.task);

      if (this.filter.modeId === 'Vocabulary' || this.filter.modeId === 'Cart' || this.filter.modeId === 'Audio') {
        for (let key in this.task.dWords) {
          this.task.dWords[key].css = 'c-button--info';
        }
      }

      if (this.filter.modeId === 'Match') {
        for (let key in this.task.sWords) {
          this.task.sWords[key].css = 'c-button--ghost-brand';
        }
        for (let key in this.task.dWords) {
          this.task.dWords[key].css = 'c-button--ghost-brand';
        }
      }

      if (this.filter.modeId === 'Write' || this.filter.modeId === 'Listen') {
        this.task.sLetters.forEach((item, index) => {
          if (item === '*') {
            this.sources.push(index);
          }
        });
      }
    });
  }

  letter(letter: string) {
    let at = this.task.sLetters.indexOf('*');
    if (at !== -1) {
      this.task.sLetters[at] = letter;
    }
  }

  check() {
    if (this.disabled === false) {
      this.loading = true;
      this.disabled = true;

      this.educationService.check(this.task.word.wordId, this.task.sLetters.join(''), this.filter.destLang).subscribe(data => {
        this.disabled = false;
        if (data === true) {
          this.score = this.score + 1;
          this.next();
        } else {
          this.loading = false;
        }
      });
    }
  }

  reset() {
    this.sources.forEach((item) => {
      this.task.sLetters[item] = '*';
    });
  }

  help() {
    if (this.disabled === false) {
      this.loading = true;
      this.disabled = true;
      let index = this.task.sLetters.indexOf('*');
      this.educationService.help(this.task.word.wordId, index, this.filter.destLang).subscribe(data => {
        this.disabled = false;
        this.loading = false;
        if (data.length > 0) {
          this.task.sLetters[index] = data;
          let i = this.sources.indexOf(index);
          if(i !== -1) {
            this.sources.splice(i, 1);
          }
        }
      });
    }
  }

  sourceMatch(word: EducationWordLocalization) {
    if (word.css !== 'c-button--ghost-info') {
      this.isChecked = true;
      word.css = 'c-button--ghost-info';
      this.lastWordId = word.key;
    }
  }

  answerMatch(word: EducationWordLocalization) {
    if (word.css !== 'c-button--ghost-success') {
      this.educationService.match(this.lastWordId, word.key).subscribe(data => {
        if (data === true) {
          word.css = 'c-button--ghost-success';
          for (let key in this.task.dWords) {
            if (this.task.dWords[key].css === 'c-button--error') {
              this.task.dWords[key].css = 'c-button--ghost-brand';
            }
          }
          this.isChecked = false;
          let success = this.task.dWords.filter(function(w) {
            return w.css === 'c-button--ghost-success';
          });

          if (success.length === this.task.dWords.length) {
            this.isCorrect = true;
          }
        } else {
          word.css = 'c-button--error';
        }
      });
    }
  }

  viewHelp () {
    console.log('isViewHelp: ' +this.isViewHelp);
    this.isViewHelp = !this.isViewHelp;
    console.log('isViewHelp: ' +this.isViewHelp);
  }

  onChangeMode(newValue: string) {
    this.filter.modeId = newValue;
  }

  onChangeCefr(newValue: string) {
    this.filter.cefrId = newValue;
  }

  onChangeTopic(newValue: string) {
    this.filter.topicId = newValue;
  }

  onChangePos(newValue: string) {
    this.filter.posId = newValue;
  }

  onChangeFromLang(newValue: string) {
    this.filter.fromlang = newValue;
  }

  onChangeDestLang(newValue: string) {
    this.filter.destLang = newValue;
  }
}
