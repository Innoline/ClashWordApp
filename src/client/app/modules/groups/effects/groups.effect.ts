// angular
import { Injectable } from '@angular/core';

// libs
import { Store, Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

// module
import { Groups } from '../index';
import { User } from '../../user/index';
import { RouterExtensions } from '../../core/index';
import { GroupsService, GroupMessagesService } from '../services/index';

@Injectable()
export class GroupsEffects {

  /**
   * This effect makes use of the `startWith` operator to trigger
   * the effect immediately on startup.
   */
  @Effect() search$: Observable<Action> = this.actions$
    .ofType(Groups.ActionTypes.SEARCH_IN_PROGRESS)
    .switchMap(action => this.groupsService.search(action.payload))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.groupsService.track(Groups.ActionTypes.SEARCH_RECEIVED, { label: obj.data.length.toString() });
        return new Groups.SearchReceivedAction(obj.data);
      } else {
        this.groupsService.track(Groups.ActionTypes.SEARCH_FAILURE, { label: obj.errors });
        return new Groups.SearchFailureAction(obj.errors);
      }
    })
    .catch(error => Observable.of(new Groups.SearchFailureAction(error)));

  @Effect() add$: Observable<Action> = this.actions$
    .ofType(Groups.ActionTypes.ADD_IN_PROGRESS)
    .switchMap(action => this.groupsService.add(action.payload))
    .map(payload => {
      let obj = payload;
      // analytics
      if (obj.data) {
        this.groupsService.track(Groups.ActionTypes.ADD_RECEIVED, { label: obj.data.title });
        this.store.dispatch(new User.UserClanSetKeyAction(obj.data.id));
        return new Groups.AddReceivedAction(obj.data);
      } else {
        this.groupsService.track(Groups.ActionTypes.ADD_FAILURE, { label: obj.errors });
        return new Groups.AddFailureAction(obj.errors);
      }
    })
    .catch(error => Observable.of(new Groups.AddFailureAction(error)));

  @Effect({ dispatch: false }) add_received$: Observable<Action> = this.actions$
    .ofType(Groups.ActionTypes.ADD_RECEIVED)
    .do(() => {
      this.router.navigate(['/clans']);
    });

  @Effect() messages$: Observable<Action> = this.actions$
    .ofType(Groups.ActionTypes.MESSAGES_IN_PROGRESS)
    .switchMap(action => this.groupMessagesService.list())
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.groupsService.track(Groups.ActionTypes.MESSAGES_RECEIVED, { label: obj.data.length.toString() });
        return new Groups.MessagesReceivedAction(obj.data);
      } else {
        this.groupsService.track(Groups.ActionTypes.MESSAGES_FAILURE, { label: obj.errors });
        return new Groups.SearchFailureAction(obj.errors);
      }
    })
    .catch(error => Observable.of(new Groups.SearchFailureAction(error)));

  @Effect() messages_add$: Observable<Action> = this.actions$
    .ofType(Groups.ActionTypes.MESSAGES_ADD_IN_PROGRESS)
    .switchMap(action => this.groupMessagesService.add(action.payload))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.groupsService.track(Groups.ActionTypes.MESSAGES_ADD_RECEIVED, { label: obj.data.message });
        return new Groups.MessagesAddReceivedAction(obj.data);
      } else {
        this.groupsService.track(Groups.ActionTypes.MESSAGES_ADD_FAILURE, { label: obj.errors });
        return new Groups.MessagesAddFailureAction(obj.errors);
      }
    })
    .catch(error => Observable.of(new Groups.MessagesAddFailureAction(error)));

  @Effect() user_group$: Observable<Action> = this.actions$
    .ofType(Groups.ActionTypes.GROUP_USER_IN_PROGRESS)
    .switchMap(action => this.groupsService.get())
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.groupsService.track(Groups.ActionTypes.GROUP_USER_RECEIVED, { label: obj.data.title });
        return new Groups.GroupUserReceivedAction(obj.data);
      } else {
        this.groupsService.track(Groups.ActionTypes.GROUP_USER_FAILURE, { label: obj.errors });
        return new Groups.GroupUserFailureAction(obj.errors);
      }
    })
    .catch(error => Observable.of(new Groups.GroupUserFailureAction(error)));

  @Effect() group_info$: Observable<Action> = this.actions$
    .ofType(Groups.ActionTypes.GROUP_CURRENT_IN_PROGRESS)
    .switchMap(action => this.groupsService.info(action.payload))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.groupsService.track(Groups.ActionTypes.GROUP_CURRENT_RECEIVED, { label: obj.data.title });
        return new Groups.GroupCurrentReceivedAction(obj.data);
      } else {
        this.groupsService.track(Groups.ActionTypes.GROUP_CURRENT_FAILURE, { label: obj.errors });
        return new Groups.GroupCurrentFailureAction(obj.errors);
      }
    })
    .catch(error => Observable.of(new Groups.GroupCurrentFailureAction(error)));

  @Effect() user_join$: Observable<Action> = this.actions$
    .ofType(Groups.ActionTypes.GROUP_JOIN_IN_PROGRESS)
    .switchMap(action => this.groupsService.join(action.payload))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.groupsService.track(Groups.ActionTypes.GROUP_JOIN_RECEIVED, { label: obj.data });
        this.store.dispatch(new User.UserClanSetKeyAction(obj.data));
        return new Groups.GroupJoinReceivedAction();
      } else {
        this.groupsService.track(Groups.ActionTypes.GROUP_JOIN_FAILURE, { label: obj.errors });
        return new Groups.GroupJoinFailureAction(obj.errors);
      }
    })
    .catch(error => Observable.of(new Groups.GroupJoinFailureAction(error)));

  @Effect({ dispatch: false }) join_received$: Observable<Action> = this.actions$
    .ofType(Groups.ActionTypes.GROUP_JOIN_RECEIVED)
    .do(() => {
      this.router.navigate(['/clans'], {
        transition: {
          duration: 10000,
          name: 'slideTop',
        }
      });
    });

  @Effect() user_leave$: Observable<Action> = this.actions$
    .ofType(Groups.ActionTypes.GROUP_LEAVE_IN_PROGRESS)
    .switchMap(action => this.groupsService.leave(action.payload))
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        this.groupsService.track(Groups.ActionTypes.GROUP_LEAVE_RECEIVED, { label: 'Выход из группы' });
        this.store.dispatch(new User.UserClanSetKeyAction(null));
        return new Groups.GroupLeaveReceivedAction();
      } else {
        this.groupsService.track(Groups.ActionTypes.GROUP_LEAVE_FAILURE, { label: obj.errors });
        return new Groups.GroupLeaveFailureAction(obj.errors);
      }
    })
    .catch(error => Observable.of(new Groups.GroupLeaveFailureAction(error)));

  @Effect({ dispatch: false }) leave_received$: Observable<Action> = this.actions$
    .ofType(Groups.ActionTypes.GROUP_LEAVE_RECEIVED)
    .do(() => {
      this.router.navigate(['/clans'], {
        transition: {
          duration: 10000,
          name: 'slideTop',
        }
      });
    });

  constructor(
    private store: Store<any>,
    private actions$: Actions,
    private groupsService: GroupsService,
    private groupMessagesService: GroupMessagesService,
    private router: RouterExtensions
  ) { }
}
