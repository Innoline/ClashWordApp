import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Config } from '../core/index';
import { HttpHelper } from './http.helper';
import { DictionaryList, DictionaryWordModel } from '../models/dictionary.model';
import { DictionaryWordCompleter } from '../completer/index';

@Injectable()
export class DictionaryService {
  private mainAPI: string = Config.ENVIRONMENT().API + 'dictionary';

  constructor(private http: HttpHelper) {
  }

  completerWord(wordModel: DictionaryWordModel) {
    return new DictionaryWordCompleter(this.http, wordModel, this.mainAPI + '/completer');
  }

  searchWord(wordModel: DictionaryWordModel): Observable<DictionaryList[]> {
    return this.http.post(this.mainAPI + '/search', wordModel, true);
  }
}
