/// <reference path="./../../../../../node_modules/@types/jquery/index.d.ts" />
/// <reference path="./../../../../../node_modules/@types/signalr/index.d.ts" />
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import {
  SignalRConnectionStatus, ClashWordServer, ClashWordSignalR,
  ClashWordModel, ClashWordWordModel, ClashWordAnswerFromBody, ClashWordWordAnswerModel
} from '../models/clashword.model';
import { HttpHelper } from './http.helper';
import { Config } from '../core/index';

@Injectable()
export class ClashWordMainService {
  currentState = SignalRConnectionStatus.Disconnected;
  connectionState: Observable<SignalRConnectionStatus>;

  setConnectionId: Observable<string>;
  setLastAnswer: Observable<ClashWordWordAnswerModel>;
  setGameWord: Observable<ClashWordWordModel>;
  setGame: Observable<ClashWordModel>;

  private connectionStateSubject = new Subject<SignalRConnectionStatus>();

  private setConnectionIdSubject = new Subject<string>();
  private setLastAnswerSubject = new Subject<ClashWordWordAnswerModel>();
  private setGameWordSubject = new Subject<ClashWordWordModel>();
  private setGameSubject = new Subject<ClashWordModel>();

  private mainAPI: string = Config.ENVIRONMENT().API + 'ClashWordGame';
  private server: ClashWordServer;

  constructor(private http: HttpHelper) {
    this.connectionState = this.connectionStateSubject.asObservable();

    this.setConnectionId = this.setConnectionIdSubject.asObservable();
    this.setLastAnswer = this.setLastAnswerSubject.asObservable();
    this.setGameWord = this.setGameWordSubject.asObservable();
    this.setGame = this.setGameSubject.asObservable();
  }

  get isDisconnected(): boolean {
    return this.currentState === SignalRConnectionStatus.Disconnected;
  }

  init(): void {
    this.server.init();
  }

  start(): Observable<SignalRConnectionStatus> {
    if (Config.ENVIRONMENT().ENV === 'DEV') {
      $.connection.hub.url = 'http://localhost:5001/signalr';
      $.connection.hub.logging = true;
    }
    $.connection.hub.qs = {'authorization': this.http.authToken};
    console.info('userToken ' + this.http.authToken);

    let connection = <ClashWordSignalR>$.connection;
    // reference signalR hub named 'broadcaster'
    let feedHub = connection.clashWordHub;

    this.server = feedHub.server;

    feedHub.client.setConnectionId = id => this.onSetConnectionId(id);
    feedHub.client.setLastAnswer = answer => this.onLastAnswer(answer);
    feedHub.client.setGameWord = word => this.onNewWord(word);
    feedHub.client.setGame = game => this.onUpdateGame(game);

    // start the connection
    $.connection.hub.start()
      .done(response => this.setConnectionState(SignalRConnectionStatus.Connected))
      .fail(error => this.connectionStateSubject.error(error));

    return this.connectionState;
  }

  stop(): Observable<SignalRConnectionStatus> {
    $.connection.hub.stop();
    this.setConnectionState(SignalRConnectionStatus.Disconnected);
    return this.connectionState;
  }


  // Server side methods
  newGame(): Observable<string> {
    return this.http.get(this.mainAPI + '/new', true);
  }

  nextStep(gameKey: string): Observable<string>  {
    return this.http.post(this.mainAPI + '/step', gameKey, true);
  }

  answer(gameKey: string, answerKey: string, useCart: boolean): Observable<string> {
    let model: ClashWordAnswerFromBody = {
      GameKey: gameKey,
      AnswerKey: answerKey,
      UseCart: useCart
    };
    return this.http.post(this.mainAPI + '/answer', model, true);
  }

  private setConnectionState(connectionState: SignalRConnectionStatus) {
    console.log('connection state changed to: ' + connectionState);
    this.currentState = connectionState;
    this.connectionStateSubject.next(connectionState);
  }

  private onSetConnectionId(id: string) {
    this.setConnectionIdSubject.next(id);
  }

  private onLastAnswer(model: ClashWordWordAnswerModel) {
    this.setLastAnswerSubject.next(model);
  }

  private onNewWord(model: ClashWordWordModel) {
    this.setGameWordSubject.next(model);
  }

  private onUpdateGame(model: ClashWordModel) {
    this.setGameSubject.next(model);
  }
}
