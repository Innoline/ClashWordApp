import { Action } from '@ngrx/store';
import { type } from '../../core/utils/index';
import { UserLoginModel, AuthTokenModel } from '../index';

/**
 * Each action should be namespaced
 * this allows the interior to have similar typed names as other actions
 * however still allow index exports
 */
export namespace Account {
  // Category to uniquely identify the actions
  export const CATEGORY: string = 'Account';

  /**
   * For each action type in an action group, make a simple
   * enum object for all of this group's action types.
   *
   * The 'type' utility function coerces strings into string
   * literal types and runs a simple check to guarantee all
   * action types in the application are unique.
   */
  export interface IAccountActions {
    U_LOGIN: string;
    SET_TOKEN: string;
    SET_ERRORS: string;

    LOGIN: string;
    LOGIN_IN_PROGRESS: string;
    LOGIN_FAILURE: string;

    USER_IS_AUTHENTICATED_INIT: string;
    USER_IS_AUTHENTICATED_RECEIVED: string;
    USER_IS_AUTHENTICATED_FAILURE: string;

    SIGNUP_FAILURE: string;
    SIGNUP: string;
    SIGNUP_IN_PROGRESS: string;

    AUTH_TOKEN_EXPIRED: string;

    LOGOUT_IN_PROGRESS: string;
    LOGOUT_RECEIVED: string;
    LOGOUT_FAILURE: string;
    INIT: string;
  }

  export const ActionTypes: IAccountActions = {
    U_LOGIN: type(`${CATEGORY} uLogin`),
    SET_TOKEN: type(`${CATEGORY} set token`),
    SET_ERRORS: type(`${CATEGORY} set errors`),

    LOGIN: type(`${CATEGORY} login`),
    LOGIN_IN_PROGRESS: type(`${CATEGORY} login In Progress`),
    LOGIN_FAILURE: type(`${CATEGORY} login Failure`),

    USER_IS_AUTHENTICATED_INIT: type(`${CATEGORY} user auth Init`),
    USER_IS_AUTHENTICATED_RECEIVED: type(`${CATEGORY} user auth Received`),
    USER_IS_AUTHENTICATED_FAILURE: type(`${CATEGORY} user auth Failure`),

    SIGNUP: type(`${CATEGORY} signup`),
    SIGNUP_IN_PROGRESS: type(`${CATEGORY} signup In Progress`),
    SIGNUP_FAILURE: type(`${CATEGORY} signup Failure`),

    AUTH_TOKEN_EXPIRED: type(`${CATEGORY} token exp`),

    LOGOUT_IN_PROGRESS: type(`${CATEGORY} logout  In Progress`),
    LOGOUT_RECEIVED: type(`${CATEGORY} logout Received`),
    LOGOUT_FAILURE: type(`${CATEGORY} logout Failure`),

    INIT: type(`${CATEGORY} Init`),
};

  /**
   * Every action is comprised of at least a type and an optional
   * payload. Expressing actions as classes enables powerful
   * type checking in reducer functions.
   *
   * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
   */

  export class SetErrorsAction implements  Action {
    type = ActionTypes.SET_ERRORS;

    constructor(public payload: string) { }
  }

  export class ULoginAction implements Action {
    type = ActionTypes.U_LOGIN;

    constructor(public payload: string) { }
  }

  export class SetTokenAction implements Action {
    type = ActionTypes.SET_TOKEN;

    constructor(public payload: AuthTokenModel) { }
  }

  export class LoginAction implements Action {
    type = ActionTypes.LOGIN;

    constructor(public payload: UserLoginModel) { }
  }

  export class LoginInProgressAction implements Action {
    type = ActionTypes.LOGIN_IN_PROGRESS;

    payload: undefined;
  }

  export class LoginInFailureAction implements Action {
    type = ActionTypes.LOGIN_FAILURE;

    constructor(public payload: string) { }
  }

  export class UserIsAuthenticatedInitAction implements Action {
    type = ActionTypes.USER_IS_AUTHENTICATED_INIT;

    payload: undefined;
  }

  export class UserIsAuthenticatedReceivedAction implements Action {
    type = ActionTypes.USER_IS_AUTHENTICATED_RECEIVED;

    constructor(public payload: boolean) { }
  }

  export class UserIsAuthenticatedFailureAction implements Action {
    type = ActionTypes.USER_IS_AUTHENTICATED_FAILURE;

    constructor(public payload: string) { }
  }

  export class SignupAction implements Action {
    type = ActionTypes.SIGNUP;

    constructor(public payload: string) { }
  }

  export class SignupInProgressAction implements Action {
    type = ActionTypes.SIGNUP_IN_PROGRESS;

    payload: undefined;
  }

  export class SignupInFailureAction implements Action {
    type = ActionTypes.SIGNUP_FAILURE;

    constructor(public payload: string) { }
  }

  export class AuthTokenExpiredAction implements Action {
    type = ActionTypes.AUTH_TOKEN_EXPIRED;

    constructor(public payload: string) { }
  }

  export class LogoutInProgressAction implements Action {
    type = ActionTypes.LOGOUT_IN_PROGRESS;

    payload: undefined;
  }

  export class LogoutReceivedAction implements Action {
    type = ActionTypes.LOGOUT_RECEIVED;

    constructor(public payload: boolean) { }
  }

  export class LogoutInFailureAction implements Action {
    type = ActionTypes.LOGIN_FAILURE;

    constructor(public payload: string) { }
  }

  export class InitAction implements Action {
    type = ActionTypes.INIT;

    payload: undefined;
  }

  /**
   * Export a type alias of all actions in this action group
   * so that reducers can easily compose action types
   */
  export type Actions
    = SetErrorsAction
    | ULoginAction
    | SetTokenAction
    | LoginAction
    | LoginInProgressAction
    | LoginInFailureAction
    | UserIsAuthenticatedInitAction
    | UserIsAuthenticatedReceivedAction
    | UserIsAuthenticatedFailureAction
    | SignupAction
    | SignupInProgressAction
    | SignupInFailureAction
    | AuthTokenExpiredAction
    | LogoutInProgressAction
    | LogoutReceivedAction
    | LogoutInFailureAction
    | InitAction
    ;
}
