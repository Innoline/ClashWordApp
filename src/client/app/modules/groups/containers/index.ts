import { GroupsListPageComponent } from './groups_list.page';
import { GroupMessagePageComponent } from './group_messages.page';
import { GroupCreatePageComponent } from './group_create.page';

export const GROUPS_PAGES: any[] = [
  GroupsListPageComponent,
  GroupMessagePageComponent,
  GroupCreatePageComponent
];

export * from './groups_list.page';
export * from './group_messages.page';
export * from './group_create.page';
