/* SignalR related interfaces  */
import { CountDownStateEnum } from '../shared/components/countdown/countdown.model';
export interface ClashWordSignalR extends SignalR {
  clashWordHub: ClashWordProxy;
}

export interface ClashWordProxy {
  client: ClashWordClient;
  server: ClashWordServer;
}

export interface ClashWordClient {
  setConnectionId: (id: string) => void;
  setLastAnswer: (answer: ClashWordWordAnswerModel) => void;
  setGameWord: (word: ClashWordWordModel) => void;
  setOpponent: (game: ClashWordModel) => void;
  setGame: (game: ClashWordModel) => void;
}

export interface ClashWordServer {
  init(): void;
  start(id: string): void;
}

export enum SignalRConnectionStatus {
  Connected = 1,
  Disconnected = 2,
  Error = 3
}

export interface ClashWordWordModel {
  GameId: string;
  Word: string;
  Translation: string;
  Image: string;
  Character: string;
  Answers: ClashWordWordAnswerModel[];
}

export interface ClashWordWordAnswerModel {
  Key: string;
  Translation: string;
  Word: string;
  Image: string;
  IsRight: boolean;
}

export interface ClashWordAnswerFromBody {
  GameKey: string;
  AnswerKey: string;
  UseCart: boolean;
}

export interface ClashWordUserCartsFromBody {
  TypeKey: ClashWordCartTypeEnum;
  Character: string;
}

export interface ClashWordModel {
  GameKey: string;
  CurrentUserId: string;
  IsPlay: boolean;
  User: ClashWordUserModel;
  Owner: ClashWordUserModel;
  WinnerUserId: string;
  Gold: number;
  Chest: ClashWordUserChest;
  Duration: number;
}

export interface ClashWordUserModel {
  UserId: string;
  UserName: string;
  Message: string;
  MessageModel: ClashWordMessageModel;
  Level: number;
  TowerCurrent: number;
  TowerPoints: number;
  ShieldCurrent: number;
  ShieldPoints: number;
  Trophies: number;
  Carts: ClashWordGameCart[];
}

export interface ClashWordGameCart {
  TypeId: ClashWordCartTypeEnum;
  Count: number;
}

export interface ClashWordUserChest {
  ChestId: string;
  StatusId: ClashWordChestStatusEnum;
  Duration: number;
  TypeId: ClashWordChestTypeEnum;
  State: string;
}

export interface ClashWordUserChestModelApi {
  chestId: string;
  statusId: ClashWordChestStatusEnum;
  typeId: ClashWordChestTypeEnum;
  duration: number;
  arena: number;
  state: CountDownStateEnum;
}


export interface ClashWordUserCartsApi {
  carts: ClashWordUserCartModelApi[];
  typeList: ClashWordCartTypeEnum[];
  cefrList: string[];
  letters: string[];
}

export interface ClashWordUserCartModelApi {
  key: string;
  typeId: ClashWordCartTypeEnum;
  level: number;
  countOfUse: number;
  word: string;
  letter: string;
  count: number;
  cost: number;
  cefrKey: string;
  additional: ClashWordUserCartAdditionalModelApi;
  update: ClashWordUserCartModelUpdateApi;
}

export interface ClashWordUserCartAdditionalModelApi {
  attackTower: number;
  attackShield: number;
  repairTower: number;
  repairShield: number;
  specialId: ClashWordCartAdditionalSpecialEnum;
  specialValue: number;
}

export interface ClashWordUserCartModelUpdateApi {
  count: number;
  level: number;
  attackTower: number;
  attackShield: number;
  repairTower: number;
  repairShield: number;
  specialValue: number;
}

export interface ClashWordUserCartFilter {
  typeId: ClashWordCartTypeEnum;
  cefr: string;
  letter: string;
}


export interface ClashWordMessageModel {
  key: string;
  isImage: boolean;
  text: string;
}

export interface ClashWordUserChestSpecialModelApi {
  chests: ClashWordUserChestModelApi[];
  userCnt: number;
  totalCnt: number;
}


export interface ClashWordUserOpenedChestModel {
  chest: ClashWordUserChestModelApi;
  carts: ClashWordUserCartModelApi[];
}

export enum ClashWordCartTypeEnum {
  Common = 1,
  Rare = 2,
  Epic = 3,
  Legendary = 4
}

export enum ClashWordChestTypeEnum {
  Free = 0,
  Silver = 1,
  Golden = 2,
  Crown = 3,
  Giant = 4,
  Magical = 5,
  SuperMagical = 6
}

export enum ClashWordChestStatusEnum {
  Locked = 1,
  Timer = 2,
  UnLocked = 3,
  Opened = 4
}

export enum ClashWordCartAdditionalSpecialEnum {
  None = 0,
  Wait = 1,
  Speed = 2,
  Poison = 3,
  Wrong = 4,
  NoTips = 5,
  LoseTips = 6,
  Tips = 7,
  MagicShield = 8,
  Mirror = 9
}

