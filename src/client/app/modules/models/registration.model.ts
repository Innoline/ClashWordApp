export class RegistrationModel {
  email: string;
  password: string;
  firstName: string;
  secondName: string;
  middleName: string;
  mobile: string;
  country: string;
  region: string;
  city: string;
  countryId: number;
  regionId?: number;
  cityId: number;
  street: string;
  home: string;
  flat: string;
}
