/// <reference path="./../../../../../../node_modules/@types/jquery/index.d.ts" />
/// <reference path="./../../../../../../node_modules/@types/signalr/index.d.ts" />
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { Config } from '../../core/index';
import { Analytics, AnalyticsService } from '../../analytics/index';

import { SignalRConnectionStatus, GroupsSignalR, GroupsServer, GroupMessageListItemHub } from '../index';
import { HttpHelper } from '../../services/http.helper';

@Injectable()
export class GroupsHub {
  currentState = SignalRConnectionStatus.Disconnected;
  connectionState: Observable<SignalRConnectionStatus>;

  setConnectionId: Observable<string>;
  setMessage: Observable<GroupMessageListItemHub>;

  private connectionStateSubject = new Subject<SignalRConnectionStatus>();

  private setConnectionIdSubject = new Subject<string>();
  private setMessageSubject = new Subject<GroupMessageListItemHub>();

  private server: GroupsServer;

  constructor(private http: HttpHelper) {
    this.connectionState = this.connectionStateSubject.asObservable();

    this.setConnectionId = this.setConnectionIdSubject.asObservable();
    this.setMessage = this.setMessageSubject.asObservable();
  }

  get isDisconnected(): boolean {
    return this.currentState === SignalRConnectionStatus.Disconnected;
  }

  init(): void {
    this.server.init();
  }

  start(): Observable<SignalRConnectionStatus> {
    if (Config.ENVIRONMENT().ENV === 'DEV') {
      $.connection.hub.url = 'http://localhost:5001/signalr';
      $.connection.hub.logging = true;
    }
    $.connection.hub.qs = {'authorization': this.http.authToken};
    console.info('userToken ' + this.http.authToken);

    let connection = <GroupsSignalR>$.connection;
    // reference signalR hub named 'broadcaster'
    let feedHub = connection.groupHub;

    this.server = feedHub.server;

    feedHub.client.setConnectionId = obj => this.onSetConnectionId(obj);
    feedHub.client.setMessage = obj => this.onSetMessage(obj);

    // start the connection
    $.connection.hub.start()
      .done(response => this.setConnectionState(SignalRConnectionStatus.Connected))
      .fail(error => this.connectionStateSubject.error(error));

    return this.connectionState;
  }

  stop(): Observable<SignalRConnectionStatus> {
    $.connection.hub.stop();
    this.setConnectionState(SignalRConnectionStatus.Disconnected);
    return this.connectionState;
  }

  private setConnectionState(connectionState: SignalRConnectionStatus) {
    console.log('connection state changed to: ' + connectionState);
    this.currentState = connectionState;
    this.connectionStateSubject.next(connectionState);
  }

  private onSetConnectionId(id: string) {
    this.setConnectionIdSubject.next(id);
  }

  private onSetMessage(model: GroupMessageListItemHub) {
    this.setMessageSubject.next(model);
  }
}
