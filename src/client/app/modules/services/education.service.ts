import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Config } from '../core/index';
import { HttpHelper } from './http.helper';
import { EducationFilter, EducationWordTask } from '../models/education.model';
import { SelectListItem } from '../shared/models/index';

@Injectable()
export class EducationService {
  private mainAPI: string = Config.ENVIRONMENT().API + 'education';
  constructor(private http: HttpHelper) {}

  start(filter: EducationFilter): Observable<EducationWordTask> {
    return this.http.post(this.mainAPI + '/start', filter, true);
  }

  next(filter: EducationFilter): Observable<EducationWordTask> {
    return this.http.post(this.mainAPI + '/next', filter, true);
  }

  answer(answerId: string, wordId: string, lang: string) {
    let answer = {
      answerId: answerId,
      wordId: wordId,
      lang: lang
    };
    return this.http.post(this.mainAPI + '/answer', answer, true);
  }

  stop(filter: EducationFilter): Observable<EducationWordTask> {
    return this.http.post(this.mainAPI + '/stop', filter, true);
  }

  resources(resource: string): Observable<SelectListItem[]> {
    return this.http.get(this.mainAPI + 'resources/' + resource, true);
  }

  topics(): Observable<SelectListItem[]> {
    return this.http.get(this.mainAPI + 'topics/list', true);
  }

  match(fromId: string, destId: string): Observable<boolean> {
    let answer = {
      fromId: fromId,
      destId: destId
    };
    return this.http.post(this.mainAPI + '/match', answer, true);
  }

  check(wordId: string, word: string, localization: string) {
    let answer = {
      wordId: wordId,
      word: word,
      localization: localization
    };
    return this.http.post(this.mainAPI + '/check', answer, true);
  }

  help(wordId: string, index: number, localization: string) {
    let answer = {
      wordId: wordId,
      index: index,
      localization: localization
    };
    return this.http.post(this.mainAPI + '/help', answer, true);
  }
}
