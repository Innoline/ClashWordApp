import { GroupUserModel, GroupInfoModel, GroupListItem, GroupMessageListItem } from '../models/index';
import { Observable } from 'rxjs/Observable';

export interface IGroupsState {
  group_user: GroupUserModel;
  group_current: GroupInfoModel;
  groups: Array<GroupListItem>;
  messages: Array<GroupMessageListItem>;
  errors: string;
}

export const groupsInitialState: IGroupsState = {
  group_user: undefined,
  group_current: undefined,
  groups: [],
  messages: [],
  errors: undefined
};

export function getGroupsErrors(state$: Observable<IGroupsState>) {
  return state$.select(s => s.errors);
}

export function getGroupUser(state$: Observable<IGroupsState>) {
  return state$.select(s => s.group_user);
}

export function getGroupCurrent(state$: Observable<IGroupsState>) {
  return state$.select(s => s.group_current);
}

// selects specific slice from sample state
export function getGroupsList(state$: Observable<IGroupsState>) {
  return state$.select(s => s.groups);
}

export function getGroupMessagesList(state$: Observable<IGroupsState>) {
  return state$.select(s => s.messages);
}
