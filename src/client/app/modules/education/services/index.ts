import { EducationService } from './education.service';

export const EDUCATION_SERVICES: any[] = [
  EducationService
];

export * from './education.service';
