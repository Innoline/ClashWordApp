// app
import { AuthGuard } from '../modules/auth/authentication.guard';

import { IndexComponent } from './index/index.component';
import { LoginComponent } from '../modules/account/components/login.component';
import { LogoutComponent } from '../modules/account/components/logout.component';
import { RegistrationComponent } from '../modules/account/components/registration.component';
import { PasswordRestoreComponent } from '../modules/account/components/password__restore.component';
import { PasswordChangeComponent } from '../modules/account/components/password__change.component';
import { ClashWordDashboardPageComponent } from '../modules/clashword/containers/clashword_index.page';
import { ClashWordBattlePageComponent } from '../modules/clashword/containers/clashword_battle.page';
import { ClashWordCartsPageComponent } from '../modules/clashword/containers/clashword_carts.page';
import { DictionaryMainPageComponent } from '../modules/dictionary/containers/dictionary_main.page';
import { EducationMainPageComponent } from '../modules/education/containers/education_main.page';
import { UserInfoPageComponent } from '../modules/user/containers/user_info.page';
import { UserSettingsPageComponent } from '../modules/user/containers/user_settings.page';
import {
  GroupsListPageComponent, GroupMessagePageComponent, GroupCreatePageComponent,
  GroupCreateGuard, GroupMessagesGuard
} from '../modules/groups/index';

export const routes: Array<any> = [
  { path: '', component: IndexComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'password_restore', component: PasswordRestoreComponent },
  { path: 'password_change', component: PasswordChangeComponent },
  { path: 'clans', component: GroupsListPageComponent, canActivate: [AuthGuard] },
  { path: 'clan/chat', component: GroupMessagePageComponent, canActivate: [GroupMessagesGuard] },
  { path: 'clan/create', component: GroupCreatePageComponent, canActivate: [GroupCreateGuard] },
  { path: 'cw/index', component: ClashWordDashboardPageComponent, canActivate: [AuthGuard] },
  { path: 'cw/battle', component: ClashWordBattlePageComponent, canActivate: [AuthGuard] },
  { path: 'cw/carts', component: ClashWordCartsPageComponent, canActivate: [AuthGuard] },
  { path: 'dictionary', component: DictionaryMainPageComponent, canActivate: [AuthGuard] },
  { path: 'game/education', component: EducationMainPageComponent, canActivate: [AuthGuard] },
  { path: 'user/info', component: UserInfoPageComponent, canActivate: [AuthGuard] },
  { path: 'user/settings', component: UserSettingsPageComponent, canActivate: [AuthGuard] }
];
