export class DictionaryList {
  word: string;
  pos: string;
  options: DictionaryWord[];
}
export class DictionaryWord {
  key: number;
  word: string;
  sound: string;
  order: number;
  example: string;
  phrase: string;
  explanation: string;
  examples: string[];
}
export class DictionaryWordModel {
  from: string;
  dest: string;
  word: string;
}
export class DictionaryWordCompleterModel {
  word: string;
  localization: string;
  pos: string;
}
