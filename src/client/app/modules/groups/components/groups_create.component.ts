import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { Subject } from 'rxjs/Subject';
import { IAppState, getGroupsErrors } from '../../ngrx/index';
import { Groups, GroupListItem } from '../index';
import { UserCacheModel } from '../../user/index';

@Component({
  moduleId: module.id,
  selector: 'cw-groups-create',
  templateUrl: 'groups_create.component.html'
})

export class GroupsCreateComponent implements OnInit, OnDestroy {
  @Input() settings: UserCacheModel;
  errors: string;
  public group: GroupListItem;
  private ngUnSubscribeErrors: Subject<void> = new Subject<void>();

  constructor(private store: Store<IAppState>,
              private router: Router
  ) {
    const group_init = {
      id: undefined,
      title: undefined,
      description: undefined,
      visibilityId: undefined,
      accessId: undefined,
      typeId: undefined,
      trophies: undefined
    };
    this.group = <GroupListItem>(group_init);
  }

  ngOnInit() {
    this.store.let(getGroupsErrors)
      .takeUntil(this.ngUnSubscribeErrors)
      .subscribe((result: string) => {
        if (result) {
          this.errors = result;
        }
      });
  }

  create() {
    if (this.group) {
      if (this.group.title && this.group.title.length > 0
        && this.group.accessId
        && this.group.trophies && this.group.trophies > 0) {
        let model = {
          title: this.group.title,
          description: this.group.description,
          accessId: this.group.accessId,
          trophies: this.group.trophies,
        };
        this.store.dispatch(new Groups.AddInProgressAction(<GroupListItem>model));
      } else {
        this.errors = 'Заполните все поля';
      }
    }
  }

  ngOnDestroy(): void {
    this.ngUnSubscribeErrors.next();
    this.ngUnSubscribeErrors.complete();
  }
}
