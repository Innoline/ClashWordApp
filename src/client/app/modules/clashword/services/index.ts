import { ClashWordMainService } from './clashword__main.service';
import { ClashWordCartsService } from './clashword__carts.service';
import { ClashWordChestsService } from './clashword__chests.service';
import { ClashWordMessageService } from './clashword__message.service';

export const CLASH_WORD_SERVICES: any[] = [
  ClashWordMainService,
  ClashWordCartsService,
  ClashWordChestsService,
  ClashWordMessageService,
];

export * from './clashword__main.service';
export * from './clashword__chests.service';
export * from './clashword__carts.service';
export * from './clashword__message.service';
