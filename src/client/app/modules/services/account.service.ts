import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/publishReplay';

import { CookieService } from 'ngx-cookie';

import { Config } from '../core/index';
import { UserToken, UserInfo, RegistrationModel } from '../models/index';
import { AuthToken } from '../shared/models/index';

@Injectable()
export class AccountService {
  userToken: UserToken;
  isAuth: Observable<boolean>;

  private isAuthSubject = new Subject<boolean>();
  private mainAPI: string = Config.ENVIRONMENT().API + 'users';
  private observableUser: Observable<UserInfo> = null;
  private currentUser: UserInfo;

  constructor(private http: Http,
              private cookieService: CookieService) {
    // set token if saved in local storage
    this.userToken = <UserToken>this.cookieService.getObject(AuthToken);
    this.isAuth = this.isAuthSubject.asObservable();
  }

  /**
   *
   * @param username
   * @param password
   * @returns {Observable<boolean>}
   */
  login(username: string, password: string): Observable<boolean> {
    return this.http.post(Config.ENVIRONMENT().baseAPI + 'token', JSON.stringify({
      email: username,
      password: password
    }), this.getRequestOptions())
      .map((response: Response) => this.handle(response));
  }

  /**
   *
   * @param utoken
   * @returns {Observable<boolean>}
   */
  social(utoken: string): Observable<boolean> {
    return this.http.get(Config.ENVIRONMENT().API + 'auth/ulogin/' + utoken)
      .map((resonse: Response) => this.handle(resonse));
  }

  /**
   *
   * @param model
   * @returns {Observable<boolean>}
   */
  registration(model: RegistrationModel): Observable<boolean> {
    return this.http.post(this.mainAPI + '/registration', JSON.stringify(model), this.getRequestOptions())
      .map((resonse: Response) => this.handle(resonse));
  }

  /**
   *
   * @param model
   * @returns {Observable<string>}
   */
  passwordRestore(model: string): Observable<string> {
    return this.http.post(this.mainAPI + '/restore', JSON.stringify({email: model}), this.getRequestOptions())
      .map((response: Response) => {
        return response.json();
      });
  }

  /**
   *
   * @param key
   * @param password
   * @returns {Observable<string>}
   */
  passwordChange(key: string, password: string): Observable<string> {
    return this.http.post(this.mainAPI + '/change', JSON.stringify({
      key: key,
      password: password
    }), this.getRequestOptions())
      .map((response: Response) => {
        return response.json();
      });
  }

  /**
   *
   * @returns {Observable<UserInfo>}
   */
  getUserInfo(): Observable<UserInfo> {
    if (!this.observableUser) {
      // add authorization header with jwt token
      const headers = new Headers({
        'Authorization': 'Bearer ' + this.userToken.token,
        'Content-Type': 'application/json'
      });
      const options = new RequestOptions({headers: headers});

      this.observableUser = this.http.get(this.mainAPI, options)
        .map((response: Response) => {
          console.log(response);
          return response.json();
        })
        .do(user => {
          this.currentUser = <UserInfo>user;
          console.log('fetched userToken');
        })
        .publishReplay(1)
        .refCount();
    }
    return this.observableUser;
  }

  getUserInfoByAlias(userAlias: string): Observable<UserInfo> {
    const headers = new Headers({
      'Authorization': 'Bearer ' + this.userToken.token,
      'Content-Type': 'application/json'
    });
    const options = new RequestOptions({headers: headers});

    return this.http.get(this.mainAPI + '/alias/' + userAlias, options)
      .map((response: Response) => response.json());
  }

  reset() {
    this.observableUser = null;
  }

  /**
   * Logs out the current user by removing it from the this.cookieService.
   */
  logout(): void {
    this.setAuth(false);
    this.userToken = null;
    this.observableUser = null;
    this.cookieService.remove(AuthToken);
  }

  /**
   * Returns if there is a user currently logged in.
   * @return {boolean} `true` if a user is currently logged in, otherwise `false`.
   */
  isUserLoggedIn(): boolean {
    return this.cookieService.get(AuthToken) !== null;
  }

  /**
   * Returns the RequestOptions for the HTTP POST and HTTP PATCH Requests
   * @return {RequestOptions} The request options.
   */
  private getRequestOptions(): RequestOptions {
    const headers: Headers = new Headers({'Content-Type': 'application/json'});
    return new RequestOptions({headers: headers});
  }

  private handle(response: Response): boolean {
    let data = this.extractData(response);
    // login successful if there's a jwt token in the response
    let token = data && data.token;
    let expires = data && data.expires;
    if (token && expires) {
      this.userToken = {
        token: token,
        expires: expires
      };
      // store username and jwt token in local storage to keep user logged in between page refreshes

      this.cookieService.putObject(AuthToken, this.userToken);
      // return true to indicate successful login
      this.setAuth(true);
      return true;
    } else {
      // return false to indicate failed login
      this.setAuth(false);
      return false;
    }
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || body;
  }

  private setAuth(isAuth: boolean) {
    console.log('Auth state changed to: ' + isAuth);
    this.isAuthSubject.next(isAuth);
  }
}
