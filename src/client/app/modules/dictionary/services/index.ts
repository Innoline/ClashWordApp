import { DictionaryService } from './dictionary.service';

export const DICTIONARY_SERVICES: any[] = [
  DictionaryService
];

export * from './dictionary.service';
