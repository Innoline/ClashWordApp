import { Http, Response, RequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { CompleterData, CompleterItem } from 'ng2-completer';
import { GeoCityModel } from '../models/geo.model';

export class GeoCityCompleter extends Subject<CompleterItem[]> implements CompleterData {
  private remoteSearch: Subscription;

  constructor(private http: Http,
              private geoCityModel: GeoCityModel,
              private requestOptions: RequestOptions,
              private Url: string) {
    super();
  }

  public search(term: string): void {
    this.cancel();
    this.geoCityModel.query = term;

    this.remoteSearch = this.http.post(this.Url, JSON.stringify(this.geoCityModel), this.requestOptions)
      .map((res: Response) => {
        // Convert the result to CompleterItem[]
        let data = res.json();
        let matches: CompleterItem[] = data.map((item: any) => {
          return {
            title: item.title,
            //image: item.image,
            originalObject: item
          };
        });
        this.next(matches);
      })
      .subscribe();
  }

  public cancel() {
    if (this.remoteSearch) {
      this.remoteSearch.unsubscribe();
    }
  }
}
