import { EducationComponent } from './education.component';

export const EDUCATION_COMPONENTS: any[] = [
  EducationComponent,
];

export * from './education.component';
