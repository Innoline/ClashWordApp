import { Observable } from 'rxjs/Observable';
import { UserInfoModel, UserCacheModel } from '../models/index';

export interface IUserState {
  loading: boolean;
  profile: UserInfoModel;
  cache: UserCacheModel;
  error: string;
}

export const userInitialState: IUserState = {
  loading: false,
  profile: undefined,
  cache: undefined,
  error: undefined,
};

export function getProfile(state$: Observable<IUserState>) {
  return state$.select(state => state.profile);
}

export function getCache(state$: Observable<IUserState>) {
  return state$.select(state => state.cache);
}

export function getUserErrors(state$: Observable<IUserState>) {
  return state$.select(state => state.error);
}
