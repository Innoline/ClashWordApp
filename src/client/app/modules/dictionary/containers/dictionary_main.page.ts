import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IAppState, getUserCache } from '../../ngrx/index';
import { UserCacheModel } from '../../user/index';


@Component({
  selector: 'cw-dictionary-main-page',
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'dictionary_main.page.html'
})

export class DictionaryMainPageComponent {
  settings$: Observable<UserCacheModel>;

  constructor(private store: Store<IAppState>) {
    this.settings$ = store.let(getUserCache);
  }
}
