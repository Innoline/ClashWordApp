// angular
import { NgModule, Optional, SkipSelf, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

// app
import { SharedModule } from '../shared/index';
import { MultilingualModule } from '../i18n/multilingual.module';

import { CLASH_WORD_SERVICES } from './services/index';
//import { EDUCATION_GUARDS } from './guards/index';
import { CLASH_WORD_COMPONENTS } from './components/index';
import { CLASH_WORD_PAGES } from './containers/index';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [
    SharedModule,
    MultilingualModule,
  ],
  declarations: [
    ...CLASH_WORD_PAGES,
    ...CLASH_WORD_COMPONENTS
  ],
  providers: [
    ...CLASH_WORD_SERVICES
    //...EDUCATION_GUARDS
  ],
  schemas: [
    NO_ERRORS_SCHEMA,
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports: [
    ...CLASH_WORD_PAGES,
    ...CLASH_WORD_COMPONENTS
  ]
})
export class ClashWordModule {

  constructor(@Optional() @SkipSelf() parentModule: ClashWordModule) {
    if (parentModule) {
      throw new Error('SampleModule already loaded; Import in root module only.');
    }
  }
}
