import { AccountService } from './account.service';

export const ACCOUNT_PROVIDERS: any[] = [
  AccountService,
];

export * from './account.service';
