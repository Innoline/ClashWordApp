import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IAppState, getUserCache } from '../../ngrx/index';
import { UserCacheModel } from '../models/index';


@Component({
  selector: 'cw-user-profile-page',
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'user_info.page.html'
})

export class UserInfoPageComponent {
  info$: Observable<UserCacheModel>;

  constructor(private store: Store<IAppState>) {
    this.info$ = store.let(getUserCache);
  }
}
