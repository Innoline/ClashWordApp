import { Observable } from 'rxjs/Observable';
import { SelectListItem, ClashWordUserCartModelApi, ClashWordCartTypeEnum, ClashWordUserChestModelApi } from '../index';

export interface IClashWordState {
  errors: string;
  letterList: Array<string>;
  cefrList: Array<string>;
  carts: Array<ClashWordUserCartModelApi>;
  typeList: Array<ClashWordCartTypeEnum>;
  normalChests: Array<ClashWordUserChestModelApi>;
  freeChest: ClashWordUserChestModelApi;
  freeChests: Array<ClashWordUserChestModelApi>;
  freeChestsCount: number;
  crownChest: ClashWordUserChestModelApi;
  crownChestUserCtn: number;
  crownChestTotalCtn: number;
  cartsFromChest: Array<ClashWordUserCartModelApi>;
}

export const clashWordInitialState: IClashWordState = {
  errors: undefined,
  letterList: undefined,
  cefrList: undefined,
  carts: undefined,
  typeList: undefined,
  normalChests: undefined,
  freeChest: undefined,
  freeChests: undefined,
  freeChestsCount: 0,
  crownChest: undefined,
  crownChestUserCtn: 0,
  crownChestTotalCtn: 0,
  cartsFromChest: undefined,
};

export function getClashWordLetterList(state$: Observable<IClashWordState>) {
  return state$.select(state => state.letterList);
}

export function getClashWordCefrList(state$: Observable<IClashWordState>) {
  return state$.select(state => state.cefrList);
}

export function getClashWordTypeList(state$: Observable<IClashWordState>) {
  return state$.select(state => state.typeList);
}

export function getClashWordCarts(state$: Observable<IClashWordState>) {
  return state$.select(state => state.carts);
}

export function getClashWordChestNormal(state$: Observable<IClashWordState>) {
  return state$.select(state => state.normalChests);
}

export function getClashWordFreeChest(state$: Observable<IClashWordState>) {
  return state$.select(state => state.freeChest);
}


export function getClashWordFreeChests(state$: Observable<IClashWordState>) {
  return state$.select(state => state.freeChests);
}

export function getClashWordFreeChestsCount(state$: Observable<IClashWordState>) {
  return state$.select(state => state.freeChestsCount);
}

export function getClashWordCrownChest(state$: Observable<IClashWordState>) {
  return state$.select(state => state.crownChest);
}

export function getClashWordCrownChestUserCtn(state$: Observable<IClashWordState>) {
  return state$.select(state => state.crownChestUserCtn);
}

export function getClashWordCrownChestTotalCtn(state$: Observable<IClashWordState>) {
  return state$.select(state => state.crownChestTotalCtn);
}

export function getClashWordCartsFromChest(state$: Observable<IClashWordState>) {
  return state$.select(state => state.cartsFromChest);
}
