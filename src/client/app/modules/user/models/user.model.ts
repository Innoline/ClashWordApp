export class UserInfoModel {
  lang: string;
  roleId: string;
  userId: string;
  name: string;
  email: string;
  avatar?: string;
  cover: string;
  alias: string;
  money: string;
  gold: string;
  timestamp: string;
}

export interface UserCacheModel {
  isViewFriendGroup: boolean;
  languageInterface: string;
  languageNative: string;
  languageLearning: string;
  clanId: string;
  balanceMoney: string;
  balanceGold: string;
  clashWordLevel: number;
  clashWordPoints: number;
  clashWordTrophies: number;
  clashWordZone: number;
}
