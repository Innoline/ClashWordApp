// angular
import {
  NgModule, Optional, SkipSelf, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA,
  ModuleWithProviders
} from '@angular/core';

// libs

// module
import { SharedModule } from '../shared/index';
import { GROUPS_SERVICES } from './services/index';
import { GROUPS_GUARDS } from './guards/index';
import { GROUPS_COMPONENTS } from './components/index';
import { GROUPS_PAGES } from './containers/index';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    ...GROUPS_PAGES,
    ...GROUPS_COMPONENTS
  ],
  providers: [
    ...GROUPS_SERVICES,
    ...GROUPS_GUARDS
  ],
  exports: [
    ...GROUPS_PAGES,
    ...GROUPS_COMPONENTS
  ],
  schemas: [
    NO_ERRORS_SCHEMA,
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class GroupsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: GroupsModule,
      providers: [
        ...GROUPS_SERVICES
      ]
    };
  }

  constructor(@Optional() @SkipSelf() parentModule: GroupsModule) {
    if (parentModule) {
      throw new Error('AccountModule already loaded; Import in root module only.');
    }
  }
}
