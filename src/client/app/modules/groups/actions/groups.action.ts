import { Action } from '@ngrx/store';
import { type } from '../../core/utils/index';
import { GroupUserModel, GroupInfoModel, GroupListItem, GroupsFilter, GroupMessageListItem } from '../models/index';

/**
 * Each action should be namespaced
 * this allows the interior to have similar typed names as other actions
 * however still allow index exports
 */
export namespace Groups {
  // Category to uniquely identify the actions
  export const CATEGORY: string = 'Groups';

  /**
   * For each action type in an action group, make a simple
   * enum object for all of this group's action types.
   *
   * The 'type' utility function coerces strings into string
   * literal types and runs a simple check to guarantee all
   * action types in the application are unique.
   */
  export interface IGroupActions {
    SEARCH_INIT: string;
    SEARCH_IN_PROGRESS: string;
    SEARCH_RECEIVED: string;
    SEARCH_FAILURE: string;

    GROUP_USER_INIT: string;
    GROUP_USER_IN_PROGRESS: string;
    GROUP_USER_RECEIVED: string;
    GROUP_USER_FAILURE: string;

    GROUP_CURRENT_INIT: string;
    GROUP_CURRENT_IN_PROGRESS: string;
    GROUP_CURRENT_RECEIVED: string;
    GROUP_CURRENT_FAILURE: string;

    GROUP_JOIN_IN_PROGRESS: string;
    GROUP_JOIN_RECEIVED: string;
    GROUP_JOIN_FAILURE: string;

    GROUP_LEAVE_IN_PROGRESS: string;
    GROUP_LEAVE_RECEIVED: string;
    GROUP_LEAVE_FAILURE: string;

    MESSAGES_INIT: string;
    MESSAGES_IN_PROGRESS: string;
    MESSAGES_RECEIVED: string;
    MESSAGES_FAILURE: string;

    MESSAGES_ADD_INIT: string;
    MESSAGES_ADD_IN_PROGRESS: string;
    MESSAGES_ADD_RECEIVED: string;
    MESSAGES_ADD_FAILURE: string;

    ADD_INIT: string;
    ADD_IN_PROGRESS: string;
    ADD_RECEIVED: string;
    ADD_FAILURE: string;

    SAVE_INIT: string;
    SAVE_IN_PROGRESS: string;
    SAVE_RECEIVED: string;
    SAVE_FAILURE: string;
  }

  export const ActionTypes: IGroupActions = {
    SEARCH_INIT: type(`${CATEGORY} Search Init`),
    SEARCH_IN_PROGRESS: type(`${CATEGORY} Search In Progress`),
    SEARCH_RECEIVED: type(`${CATEGORY} Search Received`),
    SEARCH_FAILURE: type(`${CATEGORY} Search Failure`),

    GROUP_USER_INIT: type(`${CATEGORY} Group User Init`),
    GROUP_USER_IN_PROGRESS: type(`${CATEGORY} Group User In Progress`),
    GROUP_USER_RECEIVED: type(`${CATEGORY} Group User Received`),
    GROUP_USER_FAILURE: type(`${CATEGORY} Group User Failure`),

    GROUP_CURRENT_INIT: type(`${CATEGORY} Group Current Init`),
    GROUP_CURRENT_IN_PROGRESS: type(`${CATEGORY} Group Current In Progress`),
    GROUP_CURRENT_RECEIVED: type(`${CATEGORY} Group Current Received`),
    GROUP_CURRENT_FAILURE: type(`${CATEGORY} Group Current Failure`),

    GROUP_JOIN_IN_PROGRESS: type(`${CATEGORY} Group Join In Progress`),
    GROUP_JOIN_RECEIVED: type(`${CATEGORY} Group Join Received`),
    GROUP_JOIN_FAILURE: type(`${CATEGORY} Group Join Failure`),

    GROUP_LEAVE_IN_PROGRESS: type(`${CATEGORY} Group Leave In Progress`),
    GROUP_LEAVE_RECEIVED: type(`${CATEGORY} Group Leave Received`),
    GROUP_LEAVE_FAILURE: type(`${CATEGORY} Group Leave Failure`),

    ADD_INIT: type(`${CATEGORY} Add Init`),
    ADD_IN_PROGRESS: type(`${CATEGORY} Add In Progress`),
    ADD_RECEIVED: type(`${CATEGORY} Add Received`),
    ADD_FAILURE: type(`${CATEGORY} Add Failure`),

    MESSAGES_INIT: type(`${CATEGORY} Messages Init`),
    MESSAGES_IN_PROGRESS: type(`${CATEGORY} Messages In Progress`),
    MESSAGES_RECEIVED: type(`${CATEGORY} Messages Received`),
    MESSAGES_FAILURE: type(`${CATEGORY} Messages Failure`),

    MESSAGES_ADD_INIT: type(`${CATEGORY} Messages Add Init`),
    MESSAGES_ADD_IN_PROGRESS: type(`${CATEGORY} Messages Add In Progress`),
    MESSAGES_ADD_RECEIVED: type(`${CATEGORY} Messages Add Received`),
    MESSAGES_ADD_FAILURE: type(`${CATEGORY} Messages Add Failure`),

    SAVE_INIT: type(`${CATEGORY} Save Init`),
    SAVE_IN_PROGRESS: type(`${CATEGORY} Save In Progress`),
    SAVE_RECEIVED: type(`${CATEGORY} Save Received`),
    SAVE_FAILURE: type(`${CATEGORY} Save Failure`),
  };

  export class SearchInitAction implements Action {
    type = ActionTypes.SEARCH_INIT;

    payload: undefined;
  }

  export class SearchInProgressAction implements Action {
    type = ActionTypes.SEARCH_IN_PROGRESS;

    constructor(public payload: GroupsFilter) { }
  }

  export class SearchReceivedAction implements Action {
    type = ActionTypes.SEARCH_RECEIVED;

    constructor(public payload: GroupListItem[]) { }
  }

  export class SearchFailureAction implements Action {
    type = ActionTypes.SEARCH_FAILURE;

    constructor(public payload: string) { }
  }

  export class GroupUserInitAction implements Action {
    type = ActionTypes.GROUP_USER_INIT;

    payload: undefined;
  }

  export class GroupUserInProgressAction implements Action {
    type = ActionTypes.GROUP_USER_IN_PROGRESS;

    payload: undefined;
  }

  export class GroupUserReceivedAction implements Action {
    type = ActionTypes.GROUP_USER_RECEIVED;

    constructor(public payload: GroupUserModel) { }
  }

  export class GroupUserFailureAction implements Action {
    type = ActionTypes.GROUP_USER_FAILURE;

    constructor(public payload: string) { }
  }

  export class GroupJoinInProgressAction implements Action {
    type = ActionTypes.GROUP_JOIN_IN_PROGRESS;

    constructor(public payload: string) { }
  }

  export class GroupJoinReceivedAction implements Action {
    type = ActionTypes.GROUP_JOIN_RECEIVED;

    payload: undefined;
  }

  export class GroupJoinFailureAction implements Action {
    type = ActionTypes.GROUP_JOIN_FAILURE;

    constructor(public payload: string) { }
  }

  export class GroupLeaveInProgressAction implements Action {
    type = ActionTypes.GROUP_LEAVE_IN_PROGRESS;

    constructor(public payload: string) { }
  }

  export class GroupLeaveReceivedAction implements Action {
    type = ActionTypes.GROUP_LEAVE_RECEIVED;

    payload: undefined;
  }

  export class GroupLeaveFailureAction implements Action {
    type = ActionTypes.GROUP_LEAVE_FAILURE;

    constructor(public payload: string) { }
  }

  export class GroupCurrentInitAction implements Action {
    type = ActionTypes.GROUP_CURRENT_INIT;

    payload: undefined;
  }

  export class GroupCurrentInProgressAction implements Action {
    type = ActionTypes.GROUP_CURRENT_IN_PROGRESS;

    constructor(public payload: string) { }
  }

  export class GroupCurrentReceivedAction implements Action {
    type = ActionTypes.GROUP_CURRENT_RECEIVED;

    constructor(public payload: GroupInfoModel) { }
  }

  export class GroupCurrentFailureAction implements Action {
    type = ActionTypes.GROUP_CURRENT_FAILURE;

    constructor(public payload: string) { }
  }

  export class AddInitAction implements Action {
    type = ActionTypes.ADD_INIT;

    payload: undefined;
  }

  export class AddInProgressAction implements Action {
    type = ActionTypes.ADD_IN_PROGRESS;

    constructor(public payload: GroupListItem) { }
  }

  export class AddReceivedAction implements Action {
    type = ActionTypes.ADD_RECEIVED;

    constructor(public payload: GroupListItem) { }
  }

  export class AddFailureAction implements Action {
    type = ActionTypes.ADD_FAILURE;

    constructor(public payload: string) { }
  }

  export class MessagesInitAction implements Action {
    type = ActionTypes.MESSAGES_INIT;

    payload: undefined;
  }

  export class MessagesInProgressAction implements Action {
    type = ActionTypes.MESSAGES_IN_PROGRESS;

    payload: undefined;
  }

  export class MessagesReceivedAction implements Action {
    type = ActionTypes.MESSAGES_RECEIVED;

    constructor(public payload: GroupMessageListItem[]) { }
  }

  export class MessagesFailureAction implements Action {
    type = ActionTypes.MESSAGES_FAILURE;
    constructor(public payload: string) { }
  }

  export class MessagesAddInitAction implements Action {
    type = ActionTypes.MESSAGES_ADD_INIT;

    payload: undefined;
  }

  export class MessagesAddInProgressAction implements Action {
    type = ActionTypes.MESSAGES_ADD_IN_PROGRESS;

    constructor(public payload: GroupMessageListItem) { }
  }

  export class MessagesAddReceivedAction implements Action {
    type = ActionTypes.MESSAGES_ADD_RECEIVED;

    constructor(public payload: GroupMessageListItem) { }
  }

  export class MessagesAddFailureAction implements Action {
    type = ActionTypes.MESSAGES_ADD_FAILURE;

    constructor(public payload: string) { }
  }


  /**
   * Export a type alias of all actions in this action group
   * so that reducers can easily compose action types
   */
  export type Actions
    = SearchInitAction
    | SearchInProgressAction
    | SearchReceivedAction
    | SearchFailureAction
    | GroupUserInitAction
    | GroupUserInProgressAction
    | GroupUserReceivedAction
    | GroupUserFailureAction
    | GroupCurrentInitAction
    | GroupCurrentInProgressAction
    | GroupCurrentReceivedAction
    | GroupCurrentFailureAction
    | GroupJoinInProgressAction
    | GroupJoinReceivedAction
    | GroupJoinFailureAction
    | GroupLeaveInProgressAction
    | GroupLeaveReceivedAction
    | GroupLeaveFailureAction
    | AddInitAction
    | AddInProgressAction
    | AddReceivedAction
    | AddFailureAction
    | MessagesInitAction
    | MessagesInProgressAction
    | MessagesReceivedAction
    | MessagesFailureAction
    | MessagesAddInitAction
    | MessagesAddInProgressAction
    | MessagesAddReceivedAction
    | MessagesAddFailureAction;
}
