import { UserSettingsComponent } from './user__settings.component';
import { UserInfoComponent } from './user__info.component';

export const USER_COMPONENTS: any[] = [
  UserSettingsComponent,
  UserInfoComponent,
];

export * from './user__settings.component';
export * from './user__info.component';
