import { Component, Output, Input, OnInit, EventEmitter, SimpleChange, OnChanges } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/timer';
import 'rxjs/add/observable/never';
import { CountDownStateEnum } from './countdown.model';

@Component({
  selector: 'count-down',
  template: `<h1>{{displayString}}</h1>
  <ng-content></ng-content>
  <ul>
      <li *ngFor="let change of changeLog">{{change}}</li>
  </ul>
  `
})



export class CountDownComponent implements OnInit, OnChanges {
  @Input() state: CountDownStateEnum = CountDownStateEnum.Stop;
  @Input() duration: number;
  @Input() interval: number = 1000;
  @Input() displayString: string = '';
  @Output() onFinish: EventEmitter<string> = new EventEmitter<string>();

  changeLog: string[] = [];
  countdown: Observable<number>;

  ngOnInit() {
    console.log('cdStart init: ' + this.state);
    if (this.state === CountDownStateEnum.Work) {
      this.start();
    } else {
      this.displayString = '';
    }
  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    console.log('cdStart change: ' + this.state);
    if (this.state === CountDownStateEnum.Work) {
      this.start();
    }

    let log: string[] = [];
    for (let propName in changes) {
      if (propName === 'state') {
        let changedProp = changes[propName];
        let to = JSON.stringify(changedProp.currentValue);
        if (changedProp.isFirstChange()) {
          log.push(`Initial value of ${propName} set to ${to}`);
        } else {
          let from = JSON.stringify(changedProp.previousValue);
          log.push(`${propName} changed from ${from} to ${to}`);
        }
      }
    }
    this.changeLog.push(log.join(', '));
  }

  get durationFromSec() {
    return this.duration * 1000;
  }

  private renderCountdown(number: number) {
    this.displayString = this.getSecondsAsDigitalClock(number);
  }

  private start() {
    let self = this;
    const source = Observable.timer(0, this.interval).takeUntil(Observable.timer(this.durationFromSec + this.interval));
    this.countdown = source.map((value: any) => this.durationFromSec - value * this.interval);
    this.countdown
      .do((count: any) => {
        self.renderCountdown(count / 1000);
      })
      .takeWhile((val) => val > 0 && self.state === CountDownStateEnum.Work)
      .subscribe(obj => {
        if(obj === 0) {
          self.launch();
        }
    });
  }

  private launch() {
    this.displayString = '';
    this.onFinish.emit('launch');
  }

  private getSecondsAsDigitalClock(inputSeconds: number) {
    let sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
    let hours = Math.floor(sec_num / 3600);
    let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds = sec_num - (hours * 3600) - (minutes * 60);
    let hoursString = (hours < 10) ? '0' + hours : hours.toString();
    let minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
    let secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
    return hoursString + ':' + minutesString + ':' + secondsString;
  }
}
