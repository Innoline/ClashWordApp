export interface GroupMessageListItem {
  id: string;
  message: string;
  groupId: string;
  statusId: GroupMessageStatusEnum;
  typeId: GroupMessageTypeEnum;
}

export enum GroupMessageTypeEnum {
    /// <summary>
    /// Сообщение в чате
    /// </summary>
    Message = 0,
    /// <summary>
    /// Запрос вступления в клан
    /// </summary>
    ClanJoiningRequest = 1,
    /// <summary>
    /// Запрос карты
    /// </summary>
    ClanCartRequest = 2
}

export enum GroupMessageStatusEnum {
    /// <summary>
    /// Отправлено
    /// </summary>
    Send = 0,
    /// <summary>
    /// Выполнено
    /// </summary>
    Done = 10
}

export const GroupMessageListItemInit = {
  id: undefined,
  message: undefined,
  groupId: undefined,
  statusId: 0,
  typeId: 0
};

