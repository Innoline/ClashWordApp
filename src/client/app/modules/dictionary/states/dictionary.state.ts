import { Observable } from 'rxjs/Observable';

export interface IDictionaryState {
  names: Array<string>;
}

export const dictionaryInitialState: IDictionaryState = {
  names: <Array<string>>[]
};

// selects specific slice from sample state
export function getNames(state$: Observable<IDictionaryState>) {
  return state$.select(state => state.names);
}
