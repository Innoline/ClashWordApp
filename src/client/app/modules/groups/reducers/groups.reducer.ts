import { IGroupsState, groupsInitialState } from '../states/index';
import { Groups } from '../actions/index';

export function reducer(
  state: IGroupsState = groupsInitialState,
  // could support multiple state actions via union type here
  // ie: NameList.Actions | Other.Actions
  // the seed's example just has one set of actions: NameList.Actions
  action: Groups.Actions
): IGroupsState {
  switch (action.type) {
    case Groups.ActionTypes.SEARCH_INIT:
      return groupsInitialState;

    case Groups.ActionTypes.SEARCH_RECEIVED:
      return (<any>Object).assign({}, state, {
        groups: action.payload,
        errors: undefined
      });

    case Groups.ActionTypes.MESSAGES_RECEIVED:
      return (<any>Object).assign({}, state, {
        messages: action.payload,
        errors: undefined
      });

    case Groups.ActionTypes.MESSAGES_ADD_RECEIVED:
      return (<any>Object).assign({}, state, {
        messages: [ ...state.messages, action.payload ],
        errors: undefined
      });

    case Groups.ActionTypes.ADD_RECEIVED:
    case Groups.ActionTypes.SAVE_RECEIVED:
      return (<any>Object).assign({}, state, {
        errors: undefined
      });

    case Groups.ActionTypes.GROUP_USER_RECEIVED:
      return (<any>Object).assign({}, state, {
        group_user: action.payload,
        errors: undefined
      });

    case Groups.ActionTypes.GROUP_LEAVE_RECEIVED:
      return (<any>Object).assign({}, state, {
        group_user: undefined,
        messages: undefined,
        errors: undefined
      });

    case Groups.ActionTypes.SEARCH_FAILURE:
    case Groups.ActionTypes.ADD_FAILURE:
    case Groups.ActionTypes.SAVE_FAILURE:
    case Groups.ActionTypes.MESSAGES_FAILURE:
    case Groups.ActionTypes.MESSAGES_ADD_FAILURE:
    case Groups.ActionTypes.GROUP_USER_FAILURE:
    case Groups.ActionTypes.GROUP_CURRENT_FAILURE:
    case Groups.ActionTypes.GROUP_LEAVE_FAILURE:
      return (<any>Object).assign({}, state, {
        errors: action.payload
      });

    default:
      return state;
  }
}
