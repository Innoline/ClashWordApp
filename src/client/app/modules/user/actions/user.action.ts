import { Action } from '@ngrx/store';
import { type } from '../../core/utils/index';
import { UserInfoModel, UserCacheModel } from '../models/index';

/**
 * Each action should be namespaced
 * this allows the interior to have similar typed names as other actions
 * however still allow index exports
 */
export namespace User {
  // Category to uniquely identify the actions
  export const CATEGORY: string = 'User';

  /**
   * For each action type in an action group, make a simple
   * enum object for all of this group's action types.
   *
   * The 'type' utility function coerces strings into string
   * literal types and runs a simple check to guarantee all
   * action types in the application are unique.
   */
  export interface IUserActions {
    PROFILE_INIT: string;
    PROFILE_IN_PROGRESS: string;
    PROFILE_RECEIVED: string;
    PROFILE_FAILURE: string;
    PROFILE_SAVE_IN_PROGRESS: string;
    PROFILE_SAVE_RECEIVED: string;
    PROFILE_SAVE_FAILURE: string;

    SETTINGS_INIT: string;
    SETTINGS_RESET: string;
    SETTINGS_IN_PROGRESS: string;
    SETTINGS_RECEIVED: string;
    SETTINGS_FAILURE: string;
    SETTINGS_SAVE_IN_PROGRESS: string;
    SETTINGS_SAVE_RECEIVED: string;
    SETTINGS_SAVE_FAILURE: string;

    USER_CLAN_SET_KEY: string;
  }

  export const ActionTypes: IUserActions = {
    PROFILE_INIT: type(`${CATEGORY} profile Init`),
    PROFILE_IN_PROGRESS: type(`${CATEGORY} profile In Progress`),
    PROFILE_RECEIVED: type(`${CATEGORY} profile Received`),
    PROFILE_FAILURE: type(`${CATEGORY} profile Failure`),
    PROFILE_SAVE_IN_PROGRESS: type(`${CATEGORY} profile save In Progress`),
    PROFILE_SAVE_RECEIVED: type(`${CATEGORY} profile save Received`),
    PROFILE_SAVE_FAILURE: type(`${CATEGORY} profile save Failure`),

    SETTINGS_INIT: type(`${CATEGORY} settings Init`),
    SETTINGS_RESET: type(`${CATEGORY} settings reset Init`),
    SETTINGS_IN_PROGRESS: type(`${CATEGORY} settings In Progress`),
    SETTINGS_RECEIVED: type(`${CATEGORY} settings Received`),
    SETTINGS_FAILURE: type(`${CATEGORY} settings Failure`),
    SETTINGS_SAVE_IN_PROGRESS: type(`${CATEGORY} settings save In Progress`),
    SETTINGS_SAVE_RECEIVED: type(`${CATEGORY} settings save Received`),
    SETTINGS_SAVE_FAILURE: type(`${CATEGORY} settings save Failure`),

    USER_CLAN_SET_KEY: type(`${CATEGORY} clanId set for user`),
  };

  /**
   * Every action is comprised of at least a type and an optional
   * payload. Expressing actions as classes enables powerful
   * type checking in reducer functions.
   *
   * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
   */

  export class ProfileInitAction implements Action {
    type = ActionTypes.PROFILE_INIT;

    payload: string = null;
  }

  export class ProfileInProgressAction implements Action {
    type = ActionTypes.PROFILE_IN_PROGRESS;

    payload: string = null;
  }

  export class ProfileReceivedAction implements Action {
    type = ActionTypes.PROFILE_RECEIVED;

    constructor(public payload: UserInfoModel) {
    }
  }

  export class ProfileFailureAction implements Action {
    type = ActionTypes.PROFILE_FAILURE;

    constructor(public payload: string) {
    }
  }


  export class ProfileSaveInProgressAction implements Action {
    type = ActionTypes.PROFILE_SAVE_IN_PROGRESS;

    constructor(public payload: UserInfoModel) {
    }
  }

  export class ProfileSaveReceivedAction implements Action {
    type = ActionTypes.PROFILE_SAVE_RECEIVED;

    constructor(public payload: UserInfoModel) {
    }
  }

  export class ProfileSaveFailureAction implements Action {
    type = ActionTypes.PROFILE_SAVE_FAILURE;

    constructor(public payload: string) {
    }
  }

  export class SettingsInitAction implements Action {
    type = ActionTypes.SETTINGS_INIT;

    payload: string = null;
  }

  export class SettingsResetAction implements Action {
    type = ActionTypes.SETTINGS_RESET;

    payload: string = null;
  }

  export class SettingsInProgressAction implements Action {
    type = ActionTypes.SETTINGS_IN_PROGRESS;

    payload: string = null;
  }

  export class SettingsReceivedAction implements Action {
    type = ActionTypes.SETTINGS_RECEIVED;

    constructor(public payload: UserCacheModel) {
    }
  }

  export class SettingsFailureAction implements Action {
    type = ActionTypes.SETTINGS_FAILURE;

    constructor(public payload: string) {
    }
  }


  export class SettingsSaveInProgressAction implements Action {
    type = ActionTypes.SETTINGS_SAVE_IN_PROGRESS;

    constructor(public payload: UserCacheModel) {
    }
  }

  export class SettingsSaveReceivedAction implements Action {
    type = ActionTypes.SETTINGS_SAVE_RECEIVED;

    constructor(public payload: UserCacheModel) {
    }
  }

  export class SettingsSaveFailureAction implements Action {
    type = ActionTypes.SETTINGS_SAVE_FAILURE;

    constructor(public payload: string) {
    }
  }

  export class UserClanSetKeyAction implements Action {
    type = ActionTypes.USER_CLAN_SET_KEY;

    constructor(public payload: string) {
    }
  }

  /**
   * Export a type alias of all actions in this action group
   * so that reducers can easily compose action types
   */
  export type Actions
    = ProfileInitAction
    | ProfileInProgressAction
    | ProfileReceivedAction
    | ProfileFailureAction
    | ProfileSaveInProgressAction
    | ProfileSaveReceivedAction
    | ProfileSaveFailureAction
    | SettingsInitAction
    | SettingsInProgressAction
    | SettingsReceivedAction
    | SettingsFailureAction
    | SettingsSaveInProgressAction
    | SettingsSaveReceivedAction
    | SettingsSaveFailureAction
    | UserClanSetKeyAction
    ;
}
