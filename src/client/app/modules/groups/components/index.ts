import { GroupsListComponent } from './groups_list.component';
import { GroupsListItemComponent } from './groups_list_item.component';
import { GroupsCreateComponent } from './groups_create.component';
import { GroupsSearchComponent } from './groups_search.component';
import { GroupMessagesListComponent } from './group_messages_list.component';
import { GroupMessagesListItemComponent } from './group_messages_list_item.component';
import { GroupMessagesCreateComponent } from './group_messages_create.component';
import { GroupMessagesHeaderComponent } from './group_messages_header.component';

export const GROUPS_COMPONENTS: any[] = [
  GroupsListComponent,
  GroupsListItemComponent,
  GroupsCreateComponent,
  GroupsSearchComponent,
  GroupMessagesListComponent,
  GroupMessagesListItemComponent,
  GroupMessagesCreateComponent,
  GroupMessagesHeaderComponent
];

export * from './groups_list.component';
export * from './groups_list_item.component';
export * from './groups_create.component';
export * from './groups_search.component';
export * from './group_messages_list.component';
export * from './group_messages_list_item.component';
export * from './group_messages_create.component';
export * from './group_messages_header.component';
