// angular
import { Injectable } from '@angular/core';

// libs
import { Store, Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

// module
import { UserService } from '../services/index';
import { User } from '../actions/index';

@Injectable()
export class UserEffects {
  /**
   * This effect makes use of the `startWith` operator to trigger
   * the effect immediately on startup.
   */

  @Effect() profile_save$: Observable<Action> = this.actions$
    .ofType(User.ActionTypes.PROFILE_IN_PROGRESS)
    .switchMap(action  => this.userService.saveProfile(action.payload))
    .map(payload => {
      console.log(payload);
      let obj = payload;
      if (obj.data) {
        // analytics
        this.userService.track(User.ActionTypes.PROFILE_SAVE_RECEIVED, { label: obj.data.userId });
        return new User.ProfileReceivedAction(obj.data);
      } else {
        // analytics
        this.userService.track(User.ActionTypes.PROFILE_SAVE_FAILURE, { label: obj.errors });
        return new User.ProfileSaveFailureAction(obj.errors);
      }
    })
    .catch((error) => Observable.of(new User.ProfileSaveFailureAction(error)));

  @Effect() settings_init$: Observable<Action> = this.actions$
    .ofType(User.ActionTypes.SETTINGS_IN_PROGRESS)
    .switchMap(action  => this.userService.getCache())
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        // analytics
        this.userService.track(User.ActionTypes.SETTINGS_RECEIVED, { label: obj.data.clanId });
        return new User.SettingsReceivedAction(obj.data);
      } else {
        // analytics
        this.userService.track(User.ActionTypes.SETTINGS_FAILURE, { label: obj.errors });
        return new User.SettingsFailureAction(obj.errors);
      }
    })
    .catch((error) => Observable.of(new User.SettingsFailureAction(error)));

  @Effect() settings_reset$: Observable<Action> = this.actions$
    .ofType(User.ActionTypes.SETTINGS_RESET)
    .switchMap(action  => this.userService.resetCache())
    .map(payload => {
      let obj = payload;
      if (obj.data) {
        // analytics
        this.userService.track(User.ActionTypes.SETTINGS_RECEIVED, { label: obj.data.clanId });
        return new User.SettingsReceivedAction(obj.data);
      } else {
        // analytics
        this.userService.track(User.ActionTypes.SETTINGS_FAILURE, { label: obj.errors });
        return new User.SettingsFailureAction(obj.errors);
      }
    })
    .catch((error) => Observable.of(new User.SettingsFailureAction(error)));

  @Effect() settings_save$: Observable<Action> = this.actions$
    .ofType(User.ActionTypes.SETTINGS_SAVE_IN_PROGRESS)
    .switchMap(action  => this.userService.saveCache(action.payload))
    .map(payload => {
      console.log(payload);
      let token = payload;
      if (token.data) {
        // analytics
        this.userService.track(User.ActionTypes.SETTINGS_SAVE_RECEIVED, { label: token.data.clanId });
        return new User.SettingsSaveReceivedAction(token.data);
      } else {
        // analytics
        this.userService.track(User.ActionTypes.SETTINGS_SAVE_FAILURE, { label: token.errors });
        return new User.SettingsSaveFailureAction(token.errors);
      }
    })
    .catch((error) => Observable.of(new User.SettingsSaveFailureAction(error)));

  constructor(
    private store: Store<any>,
    private actions$: Actions,
    private userService: UserService
  ) { }
}
