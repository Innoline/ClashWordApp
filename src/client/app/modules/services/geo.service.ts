import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Config } from '../core/index';
import { GeoCountryCompleter, GeoRegionCompleter, GeoCityCompleter } from '../completer/index';

@Injectable()
export class GeoService {
  private mainAPI: string = Config.ENVIRONMENT().baseAPI + 'geo';
  constructor(private http: Http) {}

  completerCountry() {
    return new GeoCountryCompleter(this.http, {
      query: null
    }, this.getRequestOptions(), this.mainAPI + '/countries');
  }

  completerRegion(countryId: number) {
    return new GeoRegionCompleter(this.http, {
      query: null,
      countryId: countryId
    }, this.getRequestOptions(), this.mainAPI + '/regions');
  }

  completerCity(regionId: number, countryId:number) {
    return new GeoCityCompleter(this.http, {
      query: null,
      countryId: countryId,
      regionId: regionId
    }, this.getRequestOptions(), this.mainAPI + '/cities');
  }

  /**
   * Returns the RequestOptions for the HTTP POST and HTTP PATCH Requests
   * @return {RequestOptions} The request options.
   */
  private getRequestOptions(): RequestOptions {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    return new RequestOptions({ headers: headers });
  }
}
