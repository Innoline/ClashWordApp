import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabContentDirective } from './tab-content.directive';
import { TabLabelDirective } from './tab-label.directive';
import { TabDirective } from './tab.directive';
import { TabsComponent } from './tabs.component';

@NgModule({
  imports: [CommonModule],
  exports: [
    TabContentDirective,
    TabLabelDirective,
    TabDirective,
    TabsComponent
  ],
  declarations: [
    TabContentDirective,
    TabLabelDirective,
    TabDirective,
    TabsComponent
  ],
})
export class TabsModule { }
