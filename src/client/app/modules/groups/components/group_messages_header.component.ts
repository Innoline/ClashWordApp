import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Groups, GroupUserModel } from '../index';
import { Store } from '@ngrx/store';
import { IAppState } from '../../ngrx/index';

@Component({
  moduleId: module.id,
  selector: 'cw-group-messages-header',
  templateUrl: 'group_messages_header.component.html'
})

export class GroupMessagesHeaderComponent implements OnInit {
  @Input() group: GroupUserModel;
  @Output() leaveGroup: EventEmitter<string> = new EventEmitter<string>();

  constructor(private store: Store<IAppState>) {
  }

  ngOnInit() {
    if (!this.group) {
      this.store.dispatch(new Groups.GroupUserInProgressAction());
    }
  }

  get title() {
    return this.group.title;
  }

  get description() {
    return this.group.description;
  }

  leave() {
    this.leaveGroup.emit(this.group.id);
  }
}
