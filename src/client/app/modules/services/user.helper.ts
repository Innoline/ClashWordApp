import { Injectable } from '@angular/core';
import { UrlSegment, Router } from '@angular/router';
import { UserInfo } from '../models/user.model';
import { AccountService } from './account.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserHelper {
  public currentUser: UserInfo;
  public userAlias: string;
  public isView: boolean = false;
  public urls: UrlSegment[];
  private observableUser: Observable<UserInfo> = null;

  constructor(private router: Router,
              private accountService: AccountService) {
  }

  getUser(alias: string): Observable<UserInfo> {
    if (this.observableUser === null) {
      if (alias === undefined) {
        this.observableUser = this.getCurrentUser();
      } else {
        this.observableUser = this.accountService
          .getUserInfoByAlias(alias)
          .publishReplay(1)
          .refCount();
      }
    }
    return this.observableUser;
  }

  getCurrentUser(): Observable<UserInfo> {
    return this.accountService.getUserInfo();
  }

  getUserToken() {
    return this.accountService.userToken.token;
  }

  reset() {
    this.observableUser = null;
    this.userAlias = undefined;
  }

  compareAlias(alias: string, url: string) {
    if (alias === this.currentUser.alias) {
      this.reset();
      this.router.navigate([url]);
    }
  }
}
