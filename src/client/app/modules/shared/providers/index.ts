import { AuthGuardProvider } from './guard.provider';

export const SHARED_PROVIDERS: any[] = [
  AuthGuardProvider
];

export * from './guard.provider';
