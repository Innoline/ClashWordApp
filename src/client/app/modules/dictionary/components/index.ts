import { DictionaryComponent } from './dictionary.component';

export const DICTIONARY_COMPONENTS: any[] = [
  DictionaryComponent,
];

export * from './dictionary.component';
