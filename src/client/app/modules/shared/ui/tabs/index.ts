export * from './tab.directive';
export * from './tab-label.directive';
export * from './tab-content.directive';
export * from './tabs.component';
